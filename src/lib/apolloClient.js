import fetch from 'isomorphic-unfetch'
import cookies from 'next-cookies'
import { HttpLink } from 'apollo-link-http'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { URL_API } from 'utils/urls'

export default function createApolloClient (initialState, ctx) {
  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  const token = ctx
    ? cookies(ctx).jwt
    : typeof window !== 'undefined'
    ? localStorage.getItem('jwt') && localStorage.getItem('jwt') != 'undefined'
      ? JSON.parse(localStorage.getItem('jwt'))
      : ''
    : ''

  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: new HttpLink({
      uri: `${URL_API}/gql`, // Server URL (must be absolute)
      credentials: 'same-origin', // Additional fetch() options like `credentials` or `headers`
      fetch,
      headers: {
        authorization: `Bear ${token}`,
        'x-access-jwt-token': token,
        origin: 'https://doramasflix.co',
        referer: 'https://doramasflix.co/'
      }
    }),
    cache: new InMemoryCache().restore(initialState)
  })
}
