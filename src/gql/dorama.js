import gql from 'graphql-tag'

export const LIST_DORAMAS = gql`
  query listDoramas(
    $limit: Int
    $skip: Int
    $sort: SortFindManyDoramaInput
    $filter: FilterFindManyDoramaInput
  ) {
    listDoramas(limit: $limit, skip: $skip, sort: $sort, filter: $filter) {
      _id
      name
      slug
      cast
      names
      overview
      languages
      created_by
      popularity
      poster_path
      vote_average
      backdrop_path
      first_air_date
      episode_run_time
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_DORAMAS_BY_IDS = gql`
  query listDoramas($_ids: [MongoID]!) {
    listDoramasByIds(_ids: $_ids, limit: 200) {
      _id
      name
      slug
      cast
      names
      overview
      languages
      created_by
      popularity
      poster_path
      vote_average
      backdrop_path
      first_air_date
      episode_run_time
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_DORAMAS_SLUG = gql`
  {
    listDoramas(sort: UPDATEDAT_ASC) {
      slug
      cast
      updatedAt
    }
  }
`

export const PAGINATION_DORAMAS = gql`
  query listDoramas(
    $page: Int
    $perPage: Int
    $sort: SortFindManyDoramaInput
    $filter: FilterFindManyDoramaInput
  ) {
    paginationDorama(
      page: $page
      perPage: $perPage
      sort: $sort
      filter: $filter
    ) {
      count
      pageInfo {
        currentPage
        hasNextPage
        hasPreviousPage
      }
      items {
        _id
        name
        slug
        cast
        names
        overview
        languages
        created_by
        popularity
        poster_path
        vote_average
        backdrop_path
        first_air_date
        episode_run_time
        genres {
          name
          slug
        }
        networks {
          name
          slug
        }
      }
    }
  }
`

export const SEARCH_DORAMA = gql`
  query searchDorama($input: String!) {
    searchDorama(input: $input, limit: 10) {
      _id
      slug
      name
      names
    }
  }
`

export const DETAIL_DORAMA = gql`
  query detailDorama($slug: String!) {
    detailDorama(filter: { slug: $slug }) {
      _id
      name
      slug
      cast
      names
      name_es
      overview
      languages
      created_by
      popularity
      poster_path
      vote_average
      backdrop_path
      first_air_date
      episode_run_time
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const DETAIL_DORAMA_EXTRA = gql`
  query detailDoramaExtra($slug: String!, $season_number: Float!) {
    listSeasons(sort: NUMBER_ASC, filter: { serie_slug: $slug }) {
      slug
      season_number
      poster_path
      air_date
      serie_name
    }
    listEpisodes(
      sort: NUMBER_ASC
      filter: {
        type_serie: "dorama"
        serie_slug: $slug
        season_number: $season_number
      }
    ) {
      name
      slug
      serie_name
      serie_id
      still_path
      air_date
      season_number
      episode_number
      languages
    }
  }
`
