import gql from 'graphql-tag'

export const NETWORK_LIST = gql`
  query listNetworks($limit: Int, $skip: Int, $sort: SortFindManyNetworkInput) {
    listNetworks(
      limit: $limit
      skip: $skip
      sort: $sort
      filter: { type_series: ["dorama", "movie"] }
    ) {
      _id
      name
      logo
      slug
      number_of_doramas
    }
  }
`

export const LIST_NETWORKS_SLUG = gql`
  {
    listNetworks(
      sort: UPDATEDAT_ASC
      filter: { type_series: ["dorama", "movie"] }
    ) {
      slug
      updatedAt
    }
  }
`
