import gql from 'graphql-tag'

export const LAST_EPISODES = gql`
  query premiereEpisodes($limit: Float!) {
    premiereEpisodes(limit: $limit) {
      name
      slug
      serie_name
      serie_id
      still_path
      air_date
      serie_poster
      season_number
      episode_number
    }
  }
`

export const LIST_EPISODES = gql`
  query listEpisodes($season_number: Float!, $serie_id: MongoID!) {
    listEpisodes(
      sort: NUMBER_ASC
      filter: {
        type_serie: "dorama"
        serie_id: $serie_id
        season_number: $season_number
      }
    ) {
      _id
      name
      slug
      serie_name
      serie_id
      still_path
      air_date
      season_number
      episode_number
      languages
    }
  }
`

export const LIST_EPISODES_SLUG = gql`
  {
    listEpisodes(
      limit: 10000
      filter: { type_serie: "dorama" }
      sort: UPDATEDAT_ASC
    ) {
      slug
      updatedAt
    }
  }
`

export const EPISODE_DETAIL = gql`
  query detailEpisode($slug: String!) {
    detailEpisode(filter: { slug: $slug }) {
      _id
      name
      slug
      serie_name
      serie_slug
      serie_id
      serie_poster
      still_path
      air_date
      season_number
      episode_number
      languages
      links_online
      overview
      serie_backdrop_path
    }
  }
`

export const EPISODE_DETAIL_EXTRA = gql`
  query detailEpisodeExtra($episode_id: MongoID!) {
    nextEpisode(episode_id: $episode_id) {
      slug
    }
    prevEpisode(episode_id: $episode_id) {
      slug
    }
  }
`

export const TOGGLE_SEEN_MUTATION = gql`
  mutation changeSeen($episode_id: MongoID!) {
    changeSeen(episode_id: $episode_id) {
      _id
    }
  }
`

export const SEEN_EPISODES = gql`
  query getSeenEpisodes(
    $serie_id: MongoID!
    $season_number: Float!
    $user_id: MongoID!
  ) {
    listSeen(
      filter: {
        serie_id: $serie_id
        season_number: $season_number
        user_id: $user_id
      }
    ) {
      episode_id
    }
  }
`
