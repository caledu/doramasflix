import gql from 'graphql-tag'

export const CAST_DETAIL = gql`
  query detailPeople($slug: String!) {
    detailPeople(filter: { slug: $slug }) {
      name
      gender
      birthday
      place_of_birth
      biography
      doramas {
        dorama_id
        poster_path
        name
        slug
      }
      movies {
        movie_id
        poster_path
        name
        slug
      }
    }
  }
`
