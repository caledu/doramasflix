import gql from 'graphql-tag'

export const SEARCH = gql`
  query searchAll($input: String!) {
    searchDorama(input: $input, limit: 10) {
      _id
      slug
      name
      names
    }
    searchMovie(input: $input, limit: 10) {
      _id
      name
      names
      slug
    }
  }
`

export const LIST = gql`
  query list($limit: Int) {
    listDoramas(limit: $limit, sort: POPULARITY_DESC) {
      _id
      name
      slug
      cast
      names
      overview
      created_by
      popularity
      poster_path
      backdrop_path
      first_air_date
      episode_run_time
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
    listMovies(limit: $limit, sort: POPULARITY_DESC) {
      _id
      name
      slug
      cast
      names
      overview
      popularity
      poster_path
      backdrop_path
      release_date
      runtime
      genres {
        name
        slug
      }
      networks {
        name
        slug
      }
    }
  }
`

export const LIST_POPULARS = gql`
  query list($limit: Int, $skip: Int) {
    listDoramas(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      slug
      poster_path
    }
    listMovies(limit: $limit, skip: $skip, sort: POPULARITY_DESC) {
      name
      slug
      poster_path
    }
  }
`

export const LIST_VIEWS = gql`
  {
    listDoramas(limit: 6, sort: CREATEDAT_ASC) {
      name
      slug
      poster_path
    }
    listMovies(limit: 6, sort: CREATEDAT_ASC) {
      name
      slug
      poster_path
    }
  }
`
