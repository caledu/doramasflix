import gql from 'graphql-tag'

export const REGISTER = gql`
  mutation createUser($record: CreateOneUserInput!) {
    signup(record: $record) {
      token
      isFirst
    }
  }
`

export const LOGIN = gql`
  mutation login($email: String!, $password: String!, $platform: String) {
    login(email: $email, password: $password, platform: $platform) {
      token
    }
  }
`

export const REQUEST_PASS = gql`
  mutation request($email: String!) {
    requestPassword(email: $email)
  }
`
