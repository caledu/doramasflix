import gql from 'graphql-tag'

export const GET_LOGGED_USER = gql`
  {
    loggedUser {
      _id
      names
      email
      createdAt
      tokens
      username
    }
  }
`

export const GET_USER_LIST = gql`
  query detailUser($_id: MongoID!) {
    detailUserById(_id: $_id) {
      list_doramas {
        dorama_id
        movie_id
        name
        poster_path
        status
        slug
      }
    }
  }
`

export const GET_USER_FAVS = gql`
  query detailUser($_id: MongoID!) {
    detailUserById(_id: $_id) {
      favs_doramas {
        dorama_id
        movie_id
        name
        poster_path
        slug
      }
    }
  }
`

export const GET_USER_COLLECTION = gql`
  query detailUser($username: String!) {
    detailUser(filter: { username: $username }) {
      _id
      names
      email
      username
      favs_doramas {
        dorama_id
        movie_id
        name
        poster_path
        slug
      }
      list_doramas {
        dorama_id
        movie_id
        name
        poster_path
        status
        slug
      }
    }
  }
`

export const UPDATE_USER = gql`
  mutation editUser($record: UpdateByIdUserInput!) {
    updateUser(record: $record) {
      record {
        _id
        names
        email
        createdAt
        tokens
        username
      }
    }
  }
`

export const ADD_SEEN_MUTATION = gql`
  mutation changeSeen($episode_id: MongoID!) {
    changeSeen(episode_id: $episode_id, onlyAdd: true) {
      _id
    }
  }
`

export const SEEN_SERIES = gql`
  query doramasSeen($limit: Float) {
    listDoramasSeen(limit: $limit) {
      episode_id
      serie_id
      slug
      serie_name
      serie_poster
      season_number
      episode_number
      still_path
    }
  }
`

export const TREND_SERIES = gql`
  {
    trendsDoramas(limit: 15) {
      _id
      name
      slug
      poster_path
    }
  }
`
