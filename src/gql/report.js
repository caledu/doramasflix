import gql from 'graphql-tag'

export const CREATE_PROBLEM = gql`
  mutation reportProblem($record: CreateOneProblemInput!) {
    createProblem(record: $record) {
      record {
        _id
      }
    }
  }
`
