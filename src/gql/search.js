import gql from 'graphql-tag'

export const SEARCH = gql`
  query searchAll($input: String!) {
    searchDorama(input: $input, limit: 5) {
      _id
      slug
      name
      names
    }
    searchMovie(input: $input, limit: 5) {
      _id
      name
      names
      slug
    }
  }
`
