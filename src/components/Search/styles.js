import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

/* SEARCH */
export const SearchButton = styled.a`
  padding: 0.6rem 0.5rem 0.5rem;
  float: right;
  margin-top: 20px;

  svg {
    color: ${({ theme, visible }) =>
      visible ? theme.colors.primary : theme.colors.white};
  }
`

export const Form = styled.div`
  display: block;
  margin-top: 0em;
`

export const Input = styled.input`
  display: block;
  width: calc(100% - 100px);
  height: 2.5rem;
  opacity: ${({ visible }) => (visible ? 0.8 : 0)};
  color: ${({ theme }) => theme.colors.white};
  padding: 0.5rem 1rem;
  pointer-events: ${({ visible }) => (visible ? 'auto' : 'none')};
  background-color: ${({ theme }) =>
    addOpacityToColor(theme.colors.white, 0.3)};
  box-shadow: 0 0 5px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)},
    inset 0 0 0 1px ${({ theme }) => addOpacityToColor(theme.colors.white, 0.2)};
  margin-bottom: 0;
  border-radius: 3px;
  position: absolute;
  top: 20px;
  left: 0;
  &:focus {
    outline: 0;
  }
`

export const Result = styled.div`
  position: absolute;
  left: 0px;
  top: 80px;
  width: 100%;
  opacity: 1;
  cursor: inherit;
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};

  display: block;
`

export const ResultList = styled.ul`
  margin: 0;
  margin-bottom: 1rem;
  padding: 0px;
  cursor: pointer;
`

export const ResultItem = styled.li`
  font-size: 0.75rem;
  padding-right: 1rem;
  text-align: left;
  line-height: 2.5rem;
  position: relative;
  min-height: 2.5rem;
  display: list-item;
  margin: 0;

  &:hover {
    background-color: ${({ theme }) => theme.colors.tinyBlack};
    border-left: 5px solid ${({ theme }) => theme.colors.primary};
    svg {
      color: ${({ theme }) => theme.colors.primary} !important;
    }
    padding-left: 1px;
  }
`

export const ResultMessage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  line-height: 2.5rem;
  min-height: 2.5rem;
  margin: 0;
  color: ${({ theme }) => theme.colors.white};

  div {
    margin-right: 0.5rem;
  }
`

export const ResultType = styled.span`
  vertical-align: middle;
  margin-top: 3px;
  margin-left: 3px;
  background-color: ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.white};
  border-radius: 10px;
  line-height: 1rem;
  height: 1rem;
  font-size: 0.625rem !important;
  display: inline-block;
  padding: 0 0.6rem;
  font-weight: 700;
  margin-top: -2px;
  text-transform: uppercase;
  margin-left: 0.5rem;
`

export const ResultItemLink = styled.a`
  position: absolute;
  display: block;
  left: 1rem;
  right: 0rem;
  top: 0px;
  padding-left: 1.2rem;
  text-align: left;
  font-size: 0.875rem;
  position: relative;
  padding-right: 2rem;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  color: ${({ theme }) => theme.colors.white};
`

export const ResultItemIcon = styled.div`
  position: absolute;
  left: 1rem;
  top: 3px;
  z-index: 1;
`
