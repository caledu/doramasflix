import Link from 'next/link'
import { useLazyQuery } from '@apollo/react-hooks'
import { useState, useRef, useEffect } from 'react'
import { MdSearch, MdClose, MdPlayCircleOutline } from 'react-icons/md'

import { colors } from 'styles/theme'
import { Spinner } from 'styles/base'
import { SEARCH } from 'gql/search'
import { mixArray } from 'utils/functions'
import {
  Form,
  Input,
  Result,
  ResultItem,
  ResultType,
  ResultList,
  SearchButton,
  ResultMessage,
  ResultItemLink,
  ResultItemIcon
} from './styles'

export function Search ({ visible, toggle }) {
  const inputRef = useRef()
  const [text, changeText] = useState('')
  const [
    search,
    { data: { searchDorama = [], searchMovie = [] } = {}, loading }
  ] = useLazyQuery(SEARCH)

  const handleSearch = e => {
    const input = e.target.value
    changeText(input)
    search({ variables: { input } })
  }

  const handleClose = () => {
    toggle()
    changeText('')
    if (!visible) {
      inputRef.current.focus()
    }
  }

  const detectKey = e => {
    if (e.keyCode === 27) {
      handleClose()
    }
    if (e.keyCode === 13) {
      handleClose()
    }
  }

  useEffect(() => {
    document.addEventListener('keydown', detectKey, false)

    return () => {
      document.removeEventListener('keydown', detectKey, false)
    }
  }, [])

  const results = mixArray(searchDorama, searchMovie, 10)

  return (
    <>
      <Form>
        <Input
          ref={inputRef}
          type='text'
          autoComplete='off'
          visible={visible}
          placeholder='Busca dorama...'
          value={text}
          onChange={handleSearch}
        />
      </Form>
      <SearchButton visible={visible} onClick={handleClose}>
        {visible ? <MdClose size={27} /> : <MdSearch size={27} />}
      </SearchButton>
      {text && (
        <Result>
          <ResultList>
            {loading ? (
              <ResultMessage>
                <Spinner size={20} /> Cargando...
              </ResultMessage>
            ) : results.length === 0 ? (
              <ResultMessage>
                No se encontraron resultados para esta busqueda.
              </ResultMessage>
            ) : (
              <>
                {results.map(({ name, slug, __typename }, idx) => (
                  <ResultItem key={idx}>
                    <ResultItemIcon>
                      <MdPlayCircleOutline size={15} color={colors.white} />
                    </ResultItemIcon>
                    <Link
                      href={
                        __typename === 'Dorama'
                          ? '/doramas/[slug]'
                          : '/peliculas/[slug]'
                      }
                      as={`/${
                        __typename === 'Dorama' ? 'doramas' : 'peliculas'
                      }/${slug}`}
                    >
                      <ResultItemLink>
                        {name}
                        <ResultType>
                          {__typename === 'Dorama' ? 'dorama' : 'pelicula'}
                        </ResultType>
                      </ResultItemLink>
                    </Link>
                  </ResultItem>
                ))}
              </>
            )}
          </ResultList>
        </Result>
      )}
    </>
  )
}
