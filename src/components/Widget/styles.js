import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const WidgetContainer = styled.div`
  color: ${({ theme }) => theme.colors.text};
  background-color: ${({ theme }) => theme.colors.bg2};
  margin-bottom: 1.875rem;
  border-radius: 6px;
  padding: 1rem;
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
`

export const WidgetTitle = styled.div`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.tinyBg};
  text-align: center;
  line-height: 1.8rem;
  padding: 1rem;
  font-size: 1.125rem;
  margin: -1rem -1rem 1rem;
  border-bottom: 1px solid transparent;
  border-radius: 6px 6px 0 0;
`

export const WidgetContent = styled.div`
  margin-bottom: 0.937rem;
  height: auto;
`
