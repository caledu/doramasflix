import { WidgetContainer, WidgetTitle, WidgetContent } from './styles'

export function Widget ({ title, children }) {
  return (
    <WidgetContainer>
      <WidgetTitle>{title}</WidgetTitle>
      <WidgetContent>{children}</WidgetContent>
    </WidgetContainer>
  )
}
