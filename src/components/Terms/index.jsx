import { TermsContainer } from './styles'

export function Terms () {
  return (
    <TermsContainer>
      <header>
        <h1>Términos y condiciones</h1>
      </header>
      <div>
        <p>
          El uso de Doramasflix.co, están sujetos a los siguientes términos y al
          momento de acceder o utilizar{' '}
          <strong>
            <a href='https://doramasflix.co/'>Doramasflix</a>
          </strong>
          , usted acepta estos términos y todas las leyes de reglamentos
          aplicables. Si usted no está de acuerdo con cualquiera de estos
          términos, está prohibido el uso o acceso a Doramasflix. Los materiales
          contenidos en Doramasflix pueden están protegidos por derechos de
          autor, derecho de marcas de logotipos, marcas, nombres comerciales y
          las imágenes utilizadas en este sitio son propiedad de sus respectivos
          titulares de derechos de autor.
        </p>
        <p>
          Mediante el uso de Doramasflix usted acepta que:
          <br />
        </p>
        <ul>
          <li>
            Para utilizar Doramasflix debe tener 15 años de edad o más. Si usted
            es menor de 15 años no se le permite tener acceso o utilizarla sin
            supervisión de sus padres o representantes.
          </li>
          <li>
            Doramasflix no aloja ningún vídeo presentado y no se hace
            responsable de posibles daños en su equipo por la visualización o
            descarga de estos archivos.
          </li>
          <li>
            Doramasflix se reserva el derecho de suspender el acceso a la página
            web o cualquier contenido o información publicada o que se haya
            generado en esta, así como cancelar, prohibir la entrada o el uso de
            la página web, con o sin previo aviso ni razón necesaria a su sola
            elección.
          </li>
          <li>
            El uso no autorizado de Doramasflix como tratar de acceder a paneles
            administrativos, tratar de vulnerar la página web, etc. será
            penalizado con un bloqueo de IP.
          </li>
        </ul>
        <p>
          Estos términos de uso, están sujetos a posibles cambios sin previo
          aviso.
        </p>
      </div>
    </TermsContainer>
  )
}
