import { TermsContainer } from './styles'

export function FAQs () {
  return (
    <TermsContainer>
      <header>
        <h1>Preguntas frecuentes</h1>
      </header>
      <div>
        <ul>
          <li>
            <strong>¿Qué es Doramasflix?</strong>
          </li>
        </ul>
        <p>
          <strong>Doramasflix </strong>es un sitio web donde puedes{' '}
          <strong>
            <a href='https://doramasflix.co/'>ver doramas online gratis</a>
          </strong>{' '}
          desde la mayor comodidad de tu hogar, oficina o donde te encuentres
        </p>
        <ul>
          <li>
            {' '}
            <strong>¿Subirán el capítulo de tal dorama en vivo?</strong>{' '}
          </li>
        </ul>
        <p>
          Para estar al tanto de eventos y transmisiones en vivo, te invitamos a
          que te suscribas a las notificaciones o puedes seguirnos a través de
          Facebook como Doramasflix.
        </p>
        <ul>
          <li>
            <strong>No encuentro mi dorama favorita, ¿Cómo hago?</strong>
          </li>
        </ul>
        <p>
          Constantemente estamos actualizando nuestra lista de doramas, te
          invitamos a que nos hagas el pedido a través de Facebook.
        </p>
        <ul>
          <li>
            <strong>Quiero ver una dorama y no sé como. ¿Me apoyan</strong>?
          </li>
        </ul>
        <p>
          ¡Por supuesto! Te dejamos los siguientes pasos para que puedas ver y
          disfrutar de tu película favorita.
        </p>
        <ol>
          <li>
            Encuentra la dorama que quieras ver. Esto lo puedes conseguir en la
            categoría de géneros o puedes usar el buscador en la parte superior
            derecha.
          </li>
          <li>
            Si aún así no encuentras lo que buscabas, te pedimos nos busques en
            Facebook y pidas tu dorama.
          </li>
          <li>
            Si encontraste tu dorama favorita ve a la sección de opciones de
            reproducción. Selecciona el servidor externo que mejor te guste y
            listo. ¡Disfruta de tu dorama!
          </li>
        </ol>
        <p>
          4. Puedes leer un poco más de nosotros, en el siguiente{' '}
          <a href='https://diariodevalladolid.elmundo.es/articulo/gente/cuales-son-mejores-series-momento/20210113104919393334.html'>
            link
          </a>
        </p>
      </div>
    </TermsContainer>
  )
}
