import { NoteContainer, Title, Text, TextStrong, TextLink } from './styles'
import Link from 'next/link'

export function Notes () {
  return (
    <NoteContainer>
      <Title>Doramasflix</Title>
      <Text>
        <TextStrong>doramasflix.co</TextStrong> es un sitio ideal para{' '}
        <TextStrong>
          <Link href='/'>
            <TextLink>ver doramas online</TextLink>
          </Link>
        </TextStrong>
        . Nuestro sistema para ver doramas online gratis, se preocupa por tener
        lo último en doramas de Netlix, TVN, JTBC, MBC, etc, en calidad full HD.
        Para <TextStrong>ver doramas online gratis completas</TextStrong> puedes
        usar el buscador en la parte superior o seguir uno de los enlaces de
        genero o año de estreno en la parte derecha del sitio.
      </Text>
      <Text>
        Tratamos de adaptarnos a todos los gustos, por eso en nuestra web
        tendrás doramas gratis en español, latino y subtituladas. Si buscabas un
        sitio de <TextStrong>Doramas Online</TextStrong>, este es el mejor
        lugar.
      </Text>
    </NoteContainer>
  )
}
