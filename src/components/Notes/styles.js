import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const NoteContainer = styled.div`
  background: ${({ theme }) => theme.colors.tinyBg};
  padding: 20px;
  text-align: center;
  font-size: 15px;
  border-left: 10px solid
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  border-right: 10px solid
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  border-radius: 20px;
  margin-bottom: 2rem;
`
export const Title = styled.h1`
  margin-bottom: 0.937rem;
`

export const Text = styled.p`
  margin-bottom: 0.937rem;
`

export const TextStrong = styled.strong`
  font-weight: 800;
`

export const TextLink = styled.a`
  color: ${({ theme }) => theme.colors.white};
`
