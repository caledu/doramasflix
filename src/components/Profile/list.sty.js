import styled from 'styled-components'

export const ListContainer = styled.div`
  margin: 10px -10px;
  padding: 10px 10px;
  background-color: ${({ theme }) => theme.colors.bg2};
  border-radius: 20px;
  width: 100%;
`

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const NameList = styled.div`
  font-size: 1rem;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.white};
  margin-bottom: 10px;
  margin-left: 10px;
`

export const MoreOption = styled.a`
  font-size: 0.8rem;
  margin-bottom: 3px;
  color: ${({ theme }) => theme.colors.primary};
`

export const ListContent = styled.ul`
  display: flex;
  flex-direction: row;
  margin: 0;
  padding: 0;
  list-style-type: none;
  margin-bottom: 0.2rem;
  margin-left: -0.4rem;
  margin-right: -0.4rem;
  overflow: auto;
  padding-bottom: 5px;
  padding-left: 10px;
`

export const ListWrap = styled.ul`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0;
  padding: 0;
  list-style-type: none;
  margin-bottom: 0.2rem;
  margin-left: -0.4rem;
  margin-right: -0.4rem;
  padding-bottom: 5px;
  padding-left: 10px;
`

export const ListItem = styled.li`
  max-width: 100px;
  margin-right: 8px;
  margin-bottom: 10px;
`

export const EmptyList = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100px;
`

export const MessageEmpty = styled.div`
  font-size: 0.9rem;
  font-weight: bold;
`

export const More = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  margin-right: 10px;
  svg {
    color: ${({ theme }) => theme.colors.primary};
  }
`
