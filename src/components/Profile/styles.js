import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: start;
  flex-wrap: wrap;
`

export const ContainerLeft = styled.div`
  width: 100%;
  margin-top: 10px;
  @media ${({ theme }) => theme.device.laptop} {
    width: 20%;
  }
`

export const ContainerRigth = styled.div`
  width: 100%;
  @media ${({ theme }) => theme.device.laptop} {
    width: 75%;
  }
`

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: 2px solid ${({ theme }) => theme.colors.gray};
  border-radius: 20px;
  padding: 15px;
  margin-bottom: 15px;
`

export const Names = styled.a`
  text-align: center;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.white};
  margin-bottom: 0px;
  font-weight: bold;

  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const Email = styled.a`
  text-align: center;
  font-size: 0.9rem;
  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const Image = styled.img`
  border-radius: 10px;
  width: 50px;
`

export const Record = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  border: 2px solid ${({ theme }) => theme.colors.gray};
  border-radius: 20px;
`

export const RecordItem = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px 15px;
`

export const RecordItemNumber = styled.span`
  font-size: 1rem;
  line-height: 0.9rem;
  font-weight: bold;
  padding-bottom: 0;
`

export const RecordItemStatus = styled.span`
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};
`

export const SocialShare = styled.div`
  width: 100%;
  padding: 5px 10px;
  margin: 10px 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const SocialName = styled.span`
  width: 100%;
  font-weight: bold;
  font-size: 0.7rem;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  line-break: 0;
`

export const SocialButtons = styled.div`
  display: flex;
  flex-direction: 'row';
  justify-content: center;
  flex-wrap: wrap;

  button {
    width: auto;
    margin: 5px 5px;
  }
`
