import React from 'react'
import {
  Email,
  Names,
  Image,
  Info,
  SocialShare,
  SocialName,
  SocialButtons
} from './styles'
import Link from 'next/link'

import {
  FacebookIcon,
  FacebookShareButton,
  TelegramIcon,
  TelegramShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton
} from 'react-share'

export function UserInfo ({ user, own }) {
  const shareUrl = `https://doramasflix.co/perfil/${user.username}`
  const title = 'Perfil de @' + user.username + ' en DoramasFlix!'

  return (
    <Info>
      <Link href='/perfil/[user]' as={`/perfil/${user?.username}`}>
        <Names> {user?.names}</Names>
      </Link>
      <Link href='/perfil/[user]' as={`/perfil/${user?.username}`}>
        <Email>@{user?.username}</Email>
      </Link>
      <Image src='/img/heart.jpg' />
      {own && (
        <SocialShare>
          <SocialName>Comparte tu perfil en:</SocialName>
          <SocialButtons>
            <FacebookShareButton url={shareUrl} quote={title}>
              <FacebookIcon size={32} round />
            </FacebookShareButton>
            <TwitterShareButton url={shareUrl} title={title}>
              <TwitterIcon size={32} round />
            </TwitterShareButton>
            <TelegramShareButton url={shareUrl} title={title}>
              <TelegramIcon size={32} round />
            </TelegramShareButton>
            <WhatsappShareButton url={shareUrl} title={title} separator=':: '>
              <WhatsappIcon size={32} round />
            </WhatsappShareButton>
          </SocialButtons>
        </SocialShare>
      )}
    </Info>
  )
}
