import React from 'react'
import {
  ContainerLeft,
  Container,
  Record,
  RecordItem,
  RecordItemNumber,
  RecordItemStatus,
  ContainerRigth
} from './styles'
import { collection } from '../../utils/constans'
import { ListStatus } from './list'
import { UserInfo } from './user'

export function ProfileInfo ({ user, own }) {
  const { favs_doramas = [], list_doramas = [] } = user

  return (
    <Container>
      <ContainerLeft>
        <UserInfo user={user} own={own} />
        <Record>
          <RecordItem>
            <RecordItemNumber>{favs_doramas.length}</RecordItemNumber>
            <RecordItemStatus>Favoritos</RecordItemStatus>
          </RecordItem>
          {collection.map(name => {
            const list = list_doramas.filter(it => it.status === name)

            return (
              <RecordItem key={name}>
                <RecordItemNumber>{list.length}</RecordItemNumber>
                <RecordItemStatus>{name}</RecordItemStatus>
              </RecordItem>
            )
          })}
        </Record>
      </ContainerLeft>
      <ContainerRigth>
        <ListStatus
          list={favs_doramas.slice(0, 20)}
          name='Favoritos'
          username={user.username}
        />
        {collection.map(name => {
          const list = list_doramas
            .filter((it, idx) => it.status === name)
            .slice(0, 20)

          return <ListStatus name={name} list={list} username={user.username} />
        })}
      </ContainerRigth>
    </Container>
  )
}
