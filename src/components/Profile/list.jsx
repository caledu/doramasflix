import { Card } from 'components/Card'
import React from 'react'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { MdKeyboardArrowRight } from 'react-icons/md'
import slug from 'slug'
import {
  ListContent,
  ListItem,
  ListContainer,
  NameList,
  Header,
  MoreOption,
  EmptyList,
  MessageEmpty,
  More
} from './list.sty'
import Link from 'next/link'

export function ListStatus ({ name, username, list }) {
  return (
    <ListContainer>
      <Header>
        <NameList>{name}</NameList>
        {list.length > 0 && (
          <More>
            <Link
              href='/coleccion/[user]/[status]'
              as={`/coleccion/${username}/${slug(name, { lower: true })}`}
            >
              <MoreOption>Ver todos </MoreOption>
            </Link>
            <MdKeyboardArrowRight size={18} />
          </More>
        )}
      </Header>
      {list.length === 0 && (
        <EmptyList>
          <MessageEmpty>No tienes doramas "{name}"</MessageEmpty>
        </EmptyList>
      )}
      <ListContent>
        {list.map((it, key) => {
          const type = it.dorama_id ? 'doramas' : 'peliculas'
          const label = type === 'doramas' ? 'Dorama' : 'Pelicula'
          return (
            <ListItem key={key}>
              <Card
                width='100px'
                href={`/${type}/[slug]`}
                as={`/${type}/${it.slug}`}
                image={URL_IMAGE_POSTER + it.poster_path}
                title={it.name}
                label={label}
              />
            </ListItem>
          )
        })}
      </ListContent>
    </ListContainer>
  )
}
