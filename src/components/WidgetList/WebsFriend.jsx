import { WidgetList, WidgetItem, WidgetLink } from './styles'
import { Widget } from '../Widget'
import { webs } from 'utils/constans'

export function WebsFriend () {
  return (
    <Widget title='Webs Amigas'>
      <WidgetList>
        {webs.map((web, i) => (
          <WidgetItem key={i}>
            <WidgetLink href={web.url}>{web.name}</WidgetLink>
          </WidgetItem>
        ))}
      </WidgetList>
    </Widget>
  )
}
