import styled from 'styled-components'

export const WidgetList = styled.ul`
  max-height: 240px;
  overflow: auto;
  margin: 0 -1rem;
  padding: 0;
  list-style-type: none;
`

export const WidgetItem = styled.li`
  font-size: 0.75rem;
  padding-right: 1rem;
  text-align: right;
  line-height: 2.5rem;
  position: relative;
  min-height: 2.5rem;
  display: list-item;

  &:hover {
    border-left: 5px solid ${({ theme }) => theme.colors.primary};
    a {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`

export const WidgetLink = styled.a`
  position: absolute;
  left: 1rem;
  right: 1rem;
  top: 0;
  padding-left: 1.2rem;
  text-align: left;
  color: ${({ theme }) => theme.colors.white};
  font-size: 0.875rem;
  padding-right: 2rem;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  transition: 0.2s;
`

export const WidgetImg = styled.img`
  width: 20px;
`
