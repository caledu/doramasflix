import { WidgetList, WidgetItem, WidgetLink } from './styles'
import { useQuery } from '@apollo/react-hooks'
import { GENRE_LIST } from 'gql/genre'
import { Widget } from '../Widget'
import Link from 'next/link'

export default function GenreList () {
  const { data: { listGenres = [] } = {} } = useQuery(GENRE_LIST, {
    variables: {
      limit: 0,
      skip: 0,
      sort: 'NAME_ASC'
    },
    ssr: false
  })

  return (
    <Widget title='Géneros'>
      <WidgetList>
        {listGenres.map(genre => (
          <WidgetItem key={genre._id}>
            <Link href='/generos/[slug]' as={`/generos/${genre.slug}`}>
              <WidgetLink>{genre.name}</WidgetLink>
            </Link>
            {genre.number_of_doramas}
          </WidgetItem>
        ))}
      </WidgetList>
    </Widget>
  )
}
