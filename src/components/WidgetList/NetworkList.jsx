import { WidgetList, WidgetItem, WidgetLink } from './styles'
import { useQuery } from '@apollo/react-hooks'
import { NETWORK_LIST } from 'gql/network'
import { Widget } from '../Widget'
import Link from 'next/link'

export default function NetworkList () {
  const { data: { listNetworks = [] } = {} } = useQuery(NETWORK_LIST, {
    variables: {
      limit: 0,
      skip: 0,
      sort: 'NUMBER_OF_DORAMAS_DESC'
    },
    ssr: false
  })

  return (
    <Widget title='Productoras'>
      <WidgetList>
        {listNetworks.map(network => (
          <WidgetItem key={network._id}>
            <Link
              href='/productoras/[slug]'
              as={`/productoras/${network.slug}`}
            >
              <WidgetLink>{network.name}</WidgetLink>
            </Link>
            {network.number_of_doramas}
          </WidgetItem>
        ))}
      </WidgetList>
    </Widget>
  )
}
