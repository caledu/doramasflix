import { List } from '../List'
import { HeaderList } from './HeaderList'

export function HomeList ({
  list = [],
  sort,
  changeSort,
  title,
  type,
  url,
  loading
}) {
  return (
    <>
      <HeaderList
        active={sort}
        changeActive={changeSort}
        title={title}
        url={url}
      />
      <List items={list} loading={loading} type={type} />
    </>
  )
}
