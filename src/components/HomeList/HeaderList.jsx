import { MdMovie } from 'react-icons/md'
import { TitleSection } from '../TitleSection'
import {
  SortBy,
  SortBySpan,
  SortByList,
  SortByListItem,
  SortByListLink
} from './styles'
import { useState } from 'react'

export function HeaderList ({ active, changeActive, title, url }) {
  const [open, changeOpen] = useState(false)
  const toggle = () => changeOpen(!open)

  return (
    <TitleSection name={title} Icon={MdMovie} url={url}>
      <SortBy>
        <SortBySpan>Ordenar por: </SortBySpan>
        <SortByList open={open} onClick={toggle}>
          <SortByListItem open={open} active={active === 'CREATEDAT_DESC'}>
            <SortByListLink
              onClick={() => {
                changeActive('CREATEDAT_DESC')
                toggle()
              }}
            >
              Últimas
            </SortByListLink>
          </SortByListItem>
          <SortByListItem open={open} active={active === 'VOTE_DESC'}>
            <SortByListLink
              onClick={() => {
                changeActive('VOTE_DESC')
                toggle()
              }}
            >
              Vistas
            </SortByListLink>
          </SortByListItem>
        </SortByList>
      </SortBy>
    </TitleSection>
  )
}
