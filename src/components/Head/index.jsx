import HeadNext from 'next/head'

export function Head ({ title, description, image, url, extra }) {
  return (
    <HeadNext>
      <title>
        {title ||
          'Doramasflix 🥇 Ver Doramas Online Sub Español Gratis HD Completas'}
      </title>
      <link rel='canonical' href={url || 'https://doramasflix.co/'} />
      <meta property='og:url' content={url || 'https://doramasflix.co/'} />

      <meta
        name='description'
        content={
          description ||
          'En ✅ Doramasflix ✅ puedes ver doramas online gratis en español subtitulado, latino, doramas coreanos, chinos, tailandia y japones completas en HD.'
        }
      />
      <meta
        property='og:title'
        content={
          title ||
          'Doramasflix 🥇 Ver Doramas Online Sub Español Gratis HD Completas'
        }
      />
      <meta
        property='og:description'
        content={
          description ||
          'En ✅ Doramasflix ✅ puedes ver doramas online gratis en español subtitulado, latino, doramas coreanos, chinos, tailandia y japones completas en HD.'
        }
      />
      <meta property='og:image' content={image || '/img/meta.jpg'} />
      <meta name='twitter:card' content={image || '/img/meta.jpg'} />
      <meta
        name='twitter:title'
        content={
          title ||
          'Doramasflix 🥇 Ver Doramas Online Sub Español Gratis HD Completas'
        }
      />
      <meta
        name='twitter:description'
        content={
          description ||
          'En ✅ Doramasflix ✅ puedes ver doramas online gratis en español subtitulado, latino, doramas coreanos, chinos, tailandia y japones completas en HD.'
        }
      />
      <meta
        name='viewport'
        content='width=device-width,minimum-scale=1,initial-scale=1'
      />
      <script async src='https://arc.io/widget.min.js#qpPM9V5z'></script>
      {extra}
    </HeadNext>
  )
}
