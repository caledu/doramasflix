import styled, { css } from 'styled-components'
import Img from 'react-cool-img'

/* CAROUSEL */
export const CarouselContainer = styled.div`
  height: 550px;
  overflow: hidden;
  min-height: 100px;
  position: relative;

  .react-multi-carousel-dot-list {
    bottom: 30px;
  }

  @media ${({ theme }) => theme.device.mobileL} {
    height: 540px;
    .react-multi-carousel-dot-list {
      bottom: 40px;
    }
  }

  @media ${({ theme }) => theme.device.tablet} {
    height: 500px;
    .react-multi-carousel-dot-list {
      bottom: 50px;
    }
  }

  @media ${({ theme }) => theme.device.laptop} {
    height: 570px;
    .react-multi-carousel-dot-list {
      right: 11%;
      left: auto;
      bottom: 40px;
    }
  }
  @media ${({ theme }) => theme.device.laptopS} {
    .react-multi-carousel-dot-list {
      right: 9%;
      left: auto;
      bottom: 40px;
    }
  }
  @media ${({ theme }) => theme.device.laptopM} {
    .react-multi-carousel-dot-list {
      right: 8%;
      left: auto;
      bottom: 40px;
    }
  }
  @media ${({ theme }) => theme.device.laptopL} {
    .react-multi-carousel-dot-list {
      right: 9%;
      bottom: 70px;
    }
  }
  @media ${({ theme }) => theme.device.desktop} {
    .react-multi-carousel-dot-list {
      right: 10%;
      bottom: 70px;
    }
  }
  @media ${({ theme }) => theme.device.desktopM} {
    .react-multi-carousel-dot-list {
      right: 17%;
      bottom: 70px;
    }
  }
`

export const Dot = styled.a`
  width: 12px;
  height: 12px;
  opacity: 0.5;
  margin-right: 5px;
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.white};

  img {
    display: none;
  }

  @media ${({ theme }) => theme.device.laptop} {
    width: 60px;
    height: auto;
    img {
      display: block;
    }
  }

  @media ${({ theme }) => theme.device.laptopM} {
    width: 70px;
    margin-right: 6px;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    width: 80px;
    margin-right: 7px;
  }

  @media ${({ theme }) => theme.device.desktop} {
    width: 90px;
    margin-right: 8px;
  }

  ${({ active }) =>
    active &&
    css`
      opacity: 1;
      z-index: 1;
      transform: scale(1.1);
      background-color: ${({ theme }) => theme.colors.primary};
    `}
`

export const Poster = styled(Img)`
  margin-right: 0.5rem;
  cursor: pointer;

  @media ${({ theme }) => theme.device.laptop} {
    width: 100%;
  }

  ${({ imgactive }) =>
    imgactive &&
    css`
      opacity: 1;
      transform: scale(1.1);
    `}

  &:hover {
    ${({ imgactive }) =>
      !imgactive &&
      css`
        opacity: 1;
        z-index: 13;
        transform: scale(1.1);
      `}
  }
`

/* CAROUSEL ITEM */
export const Article = styled.article`
  z-index: 1;
  position: relative;
  padding: 6rem 0 4rem;
  margin-bottom: 1.875rem;
  color: ${({ theme }) => theme.colors.tinyWhite};

  @media ${({ theme }) => theme.device.laptop} {
    padding: 6rem 0 4rem;
  }
`
