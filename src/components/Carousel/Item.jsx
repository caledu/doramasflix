import { Info } from '../Info'
import { Article } from './styles'
import { Container } from 'styles/base'
import { ImageBackground } from '../ImageBackgound'

export function Item ({ notList, backdrop_path = '', ...props }) {
  return (
    <Article>
      <Container>
        <Info {...props} notList={notList} />
        <ImageBackground backdrop_path={backdrop_path} />
      </Container>
    </Article>
  )
}
