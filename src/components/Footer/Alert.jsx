import { FooterAlert, FooterAlertP, FooterAlertContainer } from './styles'

export function Alert () {
  return (
    <FooterAlertContainer>
      <FooterAlert>
        <FooterAlertP>
          Reportes, Pedidos, Sugerencias y mas al DM de Instagram{' '}
          <a href='https://instagram.com/doramasflix.co' target='_blank'>
            Doramasflix
          </a>
        </FooterAlertP>
      </FooterAlert>
    </FooterAlertContainer>
  )
}
