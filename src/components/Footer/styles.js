import styled from 'styled-components'

export const FooterContainer = styled.footer`
  display: block;
`

export const FooterTop = styled.div`
  color: ${({ theme }) => theme.colors.text};
  background-color: ${({ theme }) => theme.colors.tinyBg};
  padding: 1.5rem 0;

  @media ${({ theme }) => theme.device.laptop} {
    padding: 3.75rem 0;
  }
`

export const FooterRows = styled.div`
  margin: 0 1rem;
`

export const FooterNav = styled.div`
  padding: 0 1rem;
  margin-bottom: 0;
`

export const FooterTitle = styled.div`
  margin-bottom: 1rem;
  line-height: 2.5rem;
  font-size: 1.125rem;
  margin: 0 -1rem;
`

export const FooterSidebar = styled.div`
  margin: 0 -1rem;
`

export const FooterSidebarList = styled.ul`
  list-style-type: none;
  padding-left: 0;
`

export const FooterSidebarItem = styled.li`
  position: relative;
  font-size: 0.8rem;
  margin-bottom: 0.3rem;

  @media ${({ theme }) => theme.device.laptop} {
    font-size: 1rem;
  }

  svg {
    opacity: 0.7;
  }

  &:hover {
    svg {
      opacity: 1;
    }
    span {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`

export const FooterSidebarIcon = styled.div`
  position: absolute;
  left: -12px;
  svg {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const FooterSidebarSpan = styled.span`
  color: ${({ theme }) => theme.colors.white};
  padding: 0;
  text-transform: uppercase;
  display: block;
  font-weight: 600;
  padding-left: 1rem;
`

export const FooterSidebarLink = styled.a`
  line-height: 1.5rem;
`

export const FooterBottom = styled.div`
  color: ${({ theme }) => theme.colors.text};
  background-color: ${({ theme }) => theme.colors.bg};
  text-align: center;
  padding: 1rem 0;
`

export const FooterBottomText = styled.p`
  margin-bottom: 0;
  margin-top: 0;
  text-align: center;
`

export const FooterAlertContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  bottom: 5px;
  left: 0;
  right: 0;
  z-index: 9;
`

export const FooterAlert = styled.div`
  width: 100%;
  border-radius: 10px;
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.bg2};
  color: ${({ theme }) => theme.colors.text};

  @media ${({ theme }) => theme.device.mobileL} {
    width: 90%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    width: 80%;
  }

  @media ${({ theme }) => theme.device.laptop} {
    width: 70%;
  }
`

export const FooterAlertP = styled.p`
  font-size: 0.8rem;
  margin: 0;
  text-align: center;
  color: ${({ theme }) => theme.colors.text};

  a {
    color: ${({ theme }) => theme.colors.primary};
  }

  @media ${({ theme }) => theme.device.laptop} {
    font-size: 1rem;
  }
`
