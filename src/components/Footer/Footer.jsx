import { MdInsertDriveFile } from 'react-icons/md'
import {
  FooterContainer,
  FooterNav,
  FooterRows,
  FooterSidebar,
  FooterSidebarItem,
  FooterSidebarSpan,
  FooterSidebarLink,
  FooterSidebarList,
  FooterSidebarIcon,
  FooterTitle,
  FooterTop,
  FooterBottom,
  FooterBottomText
} from './styles'
import { Container } from 'styles/base'
import Link from 'next/link'

export function Footer () {
  return (
    <FooterContainer>
      <FooterTop>
        <Container>
          <FooterRows>
            <FooterNav>
              <FooterSidebar>
                <FooterTitle>Páginas de apoyo</FooterTitle>
                <FooterSidebarList>
                  <FooterSidebarItem>
                    <Link href='/dcma'>
                      <FooterSidebarLink>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>DCMA</FooterSidebarSpan>
                      </FooterSidebarLink>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/politicas'>
                      <FooterSidebarLink>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Políticas de Cookies
                        </FooterSidebarSpan>
                      </FooterSidebarLink>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/faq'>
                      <FooterSidebarLink>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Preguntas Frecuentes
                        </FooterSidebarSpan>
                      </FooterSidebarLink>
                    </Link>
                  </FooterSidebarItem>
                  <FooterSidebarItem>
                    <Link href='/terminos'>
                      <FooterSidebarLink>
                        <FooterSidebarIcon>
                          <MdInsertDriveFile size={20} />
                        </FooterSidebarIcon>
                        <FooterSidebarSpan>
                          Términos y condiciones
                        </FooterSidebarSpan>
                      </FooterSidebarLink>
                    </Link>
                  </FooterSidebarItem>
                </FooterSidebarList>
              </FooterSidebar>
            </FooterNav>
          </FooterRows>
        </Container>
      </FooterTop>
      <FooterBottom>
        <Container>
          <FooterBottomText>
            Este sitio web no almacena ningún archivo en su servidor. Todo el
            contenido es proporcionado por terceras partes las cuales no están
            afiliadas a esta web.
          </FooterBottomText>
        </Container>
      </FooterBottom>
    </FooterContainer>
  )
}
