import { FaCircle } from 'react-icons/fa'

import { List, Item } from './styles'
import { URL_IMAGE_EPISODE_1X, URL_IMAGE_EPISODE_2X } from 'utils/urls'
import { createArray } from 'utils/functions'
import { CardLoader } from 'components/Loader/Card'
import { CardEpisode } from 'components/Card/Episode'

export function LastEpisodes ({ episodes = [], loading }) {
  const arrayLoading = createArray(12)
  return (
    <List>
      {loading
        ? arrayLoading.map(it => (
            <Item key={it}>
              <CardLoader width={'100%'} height={100} />
            </Item>
          ))
        : episodes.map((ep, i) => (
            <Item key={i}>
              <CardEpisode
                href={'/capitulos/[slug]'}
                as={`/capitulos/${ep.slug}`}
                title={`${ep.serie_name} ${
                  ep.season_number > 1 ? `Temporada ${ep.season_number}` : ''
                } Episodio ${ep.episode_number}`}
                image={URL_IMAGE_EPISODE_1X + ep.still_path}
                srcSet={`${URL_IMAGE_EPISODE_1X}${ep.still_path} 1x, ${URL_IMAGE_EPISODE_2X}${ep.still_path} 2x`}
                label={ep.air_date && ep.air_date.split('T')[0]}
                state={
                  <>
                    <FaCircle color='green' size={8} /> Emisión
                  </>
                }
                coverImage
              />
            </Item>
          ))}
    </List>
  )
}
