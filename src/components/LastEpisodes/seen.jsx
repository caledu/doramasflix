import { FaCircle } from 'react-icons/fa'

import { ListHorizontal, ItemSeen } from './styles'
import { URL_IMAGE_EPISODE_1X, URL_IMAGE_EPISODE_2X } from 'utils/urls'
import { createArray } from 'utils/functions'
import { CardLoader } from 'components/Loader/Card'
import { CardEpisode } from 'components/Card/Episode'

export function LastSeenEpisodes ({ episodes = [], loading }) {
  const arrayLoading = createArray(12)
  return (
    <ListHorizontal>
      {loading
        ? arrayLoading.map(it => (
            <ItemSeen key={it}>
              <CardLoader width={'100%'} height={100} />
            </ItemSeen>
          ))
        : episodes.map((ep, i) => (
            <ItemSeen key={i}>
              <CardEpisode
                href={'/capitulos/[slug]'}
                as={`/capitulos/${ep.slug}`}
                title={`${ep.serie_name} ${
                  ep.season_number > 1 ? `Temporada ${ep.season_number}` : ''
                } Episodio ${ep.episode_number}`}
                image={URL_IMAGE_EPISODE_1X + ep.still_path}
                srcSet={`${URL_IMAGE_EPISODE_1X}${ep.still_path} 1x, ${URL_IMAGE_EPISODE_2X}${ep.still_path} 2x`}
                coverImage
              />
            </ItemSeen>
          ))}
    </ListHorizontal>
  )
}
