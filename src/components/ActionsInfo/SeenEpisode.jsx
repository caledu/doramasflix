import { useMutation } from '@apollo/react-hooks'
import { MdRemoveRedEye } from 'react-icons/md'
import { TouchableView, View } from './actions.sty'
import React, { useContext, useEffect, useState } from 'react'

import { ModalAuth } from 'components/ModalAuth'
import { UserContext } from 'contexts/UserContext'
import { TOGGLE_SEEN_MUTATION } from 'gql/episode'

export const SeenEpisode = ({ _id, listSeen = [] }) => {
  const { loggedUser } = useContext(UserContext)

  let seen = false
  if (listSeen.length !== 0) {
    seen = listSeen.findIndex(item => item.episode_id === _id) !== -1
  }
  const [isSeen, toggleSeen] = useState(seen)
  const [changeSeen] = useMutation(TOGGLE_SEEN_MUTATION)

  useEffect(() => {
    toggleSeen(seen)
  }, [seen])

  const [visibleModal, changeVisibleModal] = useState(false)
  const [type, changeType] = useState()

  return (
    <>
      <TouchableView
        active={isSeen}
        onClick={() => {
          if (loggedUser) {
            changeSeen({ variables: { episode_id: _id } })
            toggleSeen(!isSeen)
          } else {
            changeType('login')
            changeVisibleModal(true)
          }
        }}
      >
        <View>
          <MdRemoveRedEye size={20} />
        </View>
      </TouchableView>
      <ModalAuth
        changeType={changeType}
        type={type}
        visible={visibleModal}
        changeVisible={changeVisibleModal}
      />
    </>
  )
}
