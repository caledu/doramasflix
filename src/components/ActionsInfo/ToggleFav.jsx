import { FaHeart } from 'react-icons/fa'
import React, { useContext, useState } from 'react'
import { Touchable, View, Text } from './actions.sty'

import { FavsContext } from 'contexts'
import { UserContext } from 'contexts/UserContext'
import { ModalAuth } from 'components/ModalAuth'

export const ToggleFav = ({ _id, name, poster_path, __typename, slug }) => {
  const { favs, updatefavs } = useContext(FavsContext)
  const { loggedUser } = useContext(UserContext)

  const [visibleModal, changeVisibleModal] = useState(false)
  const [type, changeType] = useState()

  const liked =
    favs.findIndex(item => item.dorama_id === _id || item.movie_id === _id) !==
    -1

  return (
    <>
      <Touchable
        liked={liked}
        onClick={() => {
          if (loggedUser) {
            updatefavs({ _id, name, poster_path, __typename, slug })
          } else {
            changeType('login')
            changeVisibleModal(true)
          }
        }}
      >
        <View>
          <FaHeart size={18} />
          <Text>Favoritos</Text>
        </View>
      </Touchable>
      <ModalAuth
        changeType={changeType}
        type={type}
        visible={visibleModal}
        changeVisible={changeVisibleModal}
      />
    </>
  )
}
