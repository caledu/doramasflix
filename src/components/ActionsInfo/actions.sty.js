import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const ViewActions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const Touchable = styled.a`
  padding: 2px 12px;
  background-color: ${({ theme }) => theme.colors.foreng};
  border-radius: 100px;
  height: 38px;
  margin-right: 10px;
  position: relative;
  overflow: visible;

  @media ${({ theme }) => theme.device.laptop} {
    span {
      display: block;
    }
  }

  :hover {
    @media ${({ theme }) => theme.device.laptop} {
      transform: scale(1.1);

      ul {
        opacity: 1;
        padding-bottom: 0rem;
        overflow: auto;
      }
    }
  }

  ${({ liked }) =>
    liked &&
    css`
      background-color: ${({ theme }) => theme.colors.primary};
    `}
`

export const TouchableView = styled.a`
  padding: 2px 12px;
  margin-left: 10px;

  :hover {
    transform: scale(1.1);

    svg {
      color: ${({ theme }) => theme.colors.primary};
    }
  }

  ${({ active }) =>
    active &&
    css`
      svg  {
        color: ${({ theme }) => theme.colors.primary};
      }
    `}
`

export const View = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 38px;
  padding-bottom: 3px;
  svg {
    color: ${({ theme }) => theme.colors.white};
  }
`

export const Text = styled.span`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.white};
  display: none;
  margin-left: 5px;
`

export const Menu = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  position: absolute;
  top: 45px;
  left: 5px;
  z-index: 99;
  overflow: hidden;
  opacity: 0;
  border-top: 2px solid transparent;
  border-top-color: ${({ theme }) => theme.colors.primary};
  box-shadow: 0 5px 25px
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};
  display: block;
  height: auto;

  ${({ visible }) =>
    visible &&
    css`
      opacity: 1;
      padding-bottom: 0rem;
      overflow: auto;
    `}
`

export const MenuItem = styled.li`
  position: relative;
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  padding: 0 0.5rem;
  margin: 0 0.1rem;
  line-height: 2.5rem;
  font-size: 0.8rem;
  :hover {
    background-color: ${({ theme }) => theme.colors.bg2};
  }
`

export const Option = styled.a`
  font-weight: 700;
  color: ${({ theme }) => theme.colors.white};
  padding: 0 0.7rem;
  display: flex;
  rotate: 90°;

  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}

  ${({ remove }) =>
    remove &&
    css`
      color: ${({ theme }) => theme.colors.white};
      border-top: 1px solid transparent;
      border-top-color: ${({ theme }) => theme.colors.white};
    `}
`
