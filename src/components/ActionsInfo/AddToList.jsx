import { MdPlaylistAdd, MdPlaylistAddCheck } from 'react-icons/md'
import React, { useContext, useState } from 'react'
import { Touchable, View, Text, Menu, MenuItem, Option } from './actions.sty'

import { ListContext } from 'contexts'
import { UserContext } from 'contexts/UserContext'
import { ModalAuth } from 'components/ModalAuth'
import { collection } from 'utils/constans'

export const AddToList = ({ _id, name, poster_path, __typename, slug }) => {
  const { listDoramas, updateList, deleteToList } = useContext(ListContext)
  const { loggedUser } = useContext(UserContext)

  const [visible, changeVisible] = useState(false)
  const [visibleModal, changeVisibleModal] = useState(false)
  const [type, changeType] = useState()

  let inList = null
  let status = ''

  if (listDoramas) {
    inList = listDoramas.find(item =>
      __typename === 'Dorama' ? item.dorama_id === _id : item.movie_id === _id
    )
    status = inList?.status
  }

  const handleEdit = status => {
    setTimeout(() => {
      changeVisible(false)
    }, 200)
    updateList({
      name,
      status,
      __typename,
      slug,
      poster_path,
      movie_id: __typename === 'Movie' ? _id : null,
      dorama_id: __typename === 'Dorama' ? _id : null
    })
  }

  return (
    <>
      <Touchable
        liked={!!inList}
        onClick={() => {
          if (loggedUser) {
            changeVisible(!visible)
          } else {
            changeType('login')
            changeVisibleModal(true)
          }
        }}
      >
        <View>
          {inList ? (
            <MdPlaylistAddCheck size={24} />
          ) : (
            <MdPlaylistAdd size={24} />
          )}
          <Text>{status ? status : 'Colección'}</Text>
        </View>
        <Menu visible={visible}>
          {collection.map(it => (
            <MenuItem key={it}>
              <Option active={status === it} onClick={() => handleEdit(it)}>
                {it}
              </Option>
            </MenuItem>
          ))}

          {inList && (
            <MenuItem>
              <Option
                remove
                onClick={() => {
                  deleteToList(_id)
                  setTimeout(() => {
                    changeVisible(false)
                  }, 200)
                }}
              >
                Eliminar
              </Option>
            </MenuItem>
          )}
        </Menu>
      </Touchable>
      <ModalAuth
        changeType={changeType}
        type={type}
        visible={visibleModal}
        changeVisible={changeVisibleModal}
      />
    </>
  )
}
