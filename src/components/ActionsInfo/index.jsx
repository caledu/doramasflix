import React from 'react'
import { ToggleFav } from './ToggleFav'
import { AddToList } from './AddToList'
import { ViewActions } from './actions.sty'

export function ActionsInfo ({ notList, ...props }) {
  return (
    <ViewActions>
      <ToggleFav {...props} />
      {!notList && <AddToList {...props} />}
    </ViewActions>
  )
}
