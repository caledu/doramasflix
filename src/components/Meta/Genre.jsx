import { MetaText, MetaStrong, MetaContainer } from './styles'

export function MetaGenre ({ name }) {
  return (
    <MetaContainer>
      <MetaText>
        <MetaStrong>{`Ver doramas de ${name} Online`}</MetaStrong> en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>{`Doramas del género ${name}`}</MetaStrong> en Sub Español,
        Latino en HD Gratis y Completas.
      </MetaText>
    </MetaContainer>
  )
}
