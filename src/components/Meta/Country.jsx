import { MetaText, MetaStrong, MetaContainer } from './styles'

export function MetaCountry ({ type, name }) {
  return (
    <MetaContainer>
      <MetaText>
        <MetaStrong>{`Ver doramas ${type} Online`}</MetaStrong> en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>{`Doramas de ${name}`}</MetaStrong> en Sub Español, Latino
        en HD Gratis y Completas.
      </MetaText>
    </MetaContainer>
  )
}
