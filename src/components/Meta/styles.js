import styled from 'styled-components'

export const MetaContainer = styled.div`
  display: block;
  margin-bottom: 2rem;
`

export const MetaTitle = styled.h2`
  margin-bottom: 0.937rem;
  margin-top: 0;
  font-size: 1.25rem;
  color: ${({ theme }) => theme.colors.white};
`

export const MetaText = styled.p`
  margin-bottom: 0.937rem;
`

export const MetaStrong = styled.strong`
  font-weight: 700;
`

export const MetaLink = styled.a`
  font-weight: 700;
  color: ${({ theme }) => theme.colors.white};
`
