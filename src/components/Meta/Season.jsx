import { MetaText, MetaTitle, MetaStrong, MetaContainer } from './styles'

export function MetaSeason ({ name, season }) {
  return (
    <MetaContainer>
      <MetaTitle>{`Ver ${name} temporada ${season}`}</MetaTitle>
      <MetaText>
        <MetaStrong>{`Ver ${name} Temporada ${season} Online `}</MetaStrong>
        {` Gratis en HD con audio Latino y Subtitulado.`}
      </MetaText>
      <MetaText>
        ¡Bienvenido a Doramasflix! Esperamos que la estés pasando genial viendo
        <MetaStrong>{` ${name} Temporada ${season} sub español`}</MetaStrong>.
        Recuerda que ver doramas
        <MetaStrong>{` ${name} ${season} online `}</MetaStrong>
        nunca ha sido tan fácil en nuestra página web. En Doramasflix te
        proveemos los mejores estrenos de doramas online completas en alta
        calidad 720p y 1080p. No olvides de compartir el dorama
        <MetaStrong> {name}</MetaStrong> con tus amigos y dejarnos un
        comentario.
      </MetaText>
    </MetaContainer>
  )
}
