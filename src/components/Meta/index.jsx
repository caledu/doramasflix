import Link from 'next/link'
import {
  MetaText,
  MetaLink,
  MetaTitle,
  MetaStrong,
  MetaContainer
} from './styles'

export function Meta ({ name }) {
  return (
    <MetaContainer>
      <MetaTitle>Ver {name} Online HD Gratis</MetaTitle>
      <MetaText>
        ¡Annyeonghaseyo! 🇰🇷 🙌 Actualmente estás viendo el dorama{' '}
        <MetaStrong>{name}</MetaStrong>. Recuerda que si tienes algún problema
        con el <MetaStrong>{`dorama ${name}`}</MetaStrong>, no dudes en
        informarlo a nuestra página de Facebook. En
        <Link href='/'>
          <MetaLink> Doramasflix.co </MetaLink>
        </Link>
        puedes ver doramas online gratis en español subtitulado, doramas
        coreanos, chinos, Japoneses en emisión y finalizados en HD.
      </MetaText>
    </MetaContainer>
  )
}
