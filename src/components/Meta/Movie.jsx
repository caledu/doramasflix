import Link from 'next/link'
import {
  MetaText,
  MetaLink,
  MetaTitle,
  MetaStrong,
  MetaContainer
} from './styles'

export function MetaMovie ({ name }) {
  return (
    <MetaContainer>
      <MetaTitle>Ver {name} Online HD Gratis</MetaTitle>
      <MetaText>
        ¡Annyeonghaseyo! 🇰🇷 🙌 Actualmente estás viendo la pelicula{' '}
        <MetaStrong>{name}</MetaStrong>. Recuerda que si tienes algún problema
        con la <MetaStrong>{`pelicula ${name}`}</MetaStrong>, no dudes en
        informarlo a nuestra página de Facebook. En{' '}
        <Link href='/'>
          <MetaLink>Doramasflix.co</MetaLink>
        </Link>{' '}
        puedes ver doramas online gratis en español subtitulado, peliculas
        coreanas, chinas, Japonesas en emisión y finalizadas en HD.
      </MetaText>
    </MetaContainer>
  )
}
