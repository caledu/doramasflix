import styled from 'styled-components'

export const NotContainer = styled.section`
  margin-bottom: 2rem;
  max-width: 50rem;
  margin-left: auto;
  margin-right: auto;
`

export const NotIcon = styled.div`
  text-align: center;
  svg {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const NotTop = styled.header`
  text-align: center;
  margin-bottom: 1rem;
  line-height: 1.875rem;
`

export const NotTopTitle = styled.h1`
  font-weight: 700;
  font-size: 1.125rem;
  margin-bottom: 0;
  padding: 5px 0;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.5rem;
  color: ${({ theme }) => theme.colors.white};
`
export const NotTopText = styled.header`
  text-align: center;
  margin-bottom: 0.5rem;
  line-height: 1.875rem;
`
