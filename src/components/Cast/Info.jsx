import {
  InfoContainer,
  InfoName,
  InfoDescription,
  InfoItem,
  InfoItemSpan,
  InfoItemStrong
} from './styles'

export function CastInfo ({ name, biography, birthday, place_of_birth }) {
  return (
    <InfoContainer>
      <InfoName>{name}</InfoName>
      <InfoItem>
        <InfoItemStrong>Cumpleaños:</InfoItemStrong>
        <InfoItemSpan>{birthday}</InfoItemSpan>
      </InfoItem>
      <InfoItem>
        <InfoItemStrong>Lugar de nacimiento:</InfoItemStrong>
        <InfoItemSpan>{place_of_birth}</InfoItemSpan>
      </InfoItem>
      <InfoDescription>{biography}</InfoDescription>
    </InfoContainer>
  )
}
