import { CastList, CastItem } from './styles'
import { Card } from '../Card'
import { URL_PEOPLE } from 'utils/urls'
import toSlug from 'slug'

export default function CastList1 ({ cast }) {
  return (
    <CastList>
      {cast.map(({ name, profile_path, character, slug, id }, idx) => (
        <CastItem key={idx}>
          <Card
            href={`/reparto/[slug]`}
            as={slug ? `/reparto/${slug}` : `${id}-${toSlug(name)}`}
            image={profile_path && URL_PEOPLE + profile_path}
            title={name}
            year={character}
            imageCover={true}
          />
        </CastItem>
      ))}
    </CastList>
  )
}
