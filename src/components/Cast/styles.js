import styled from 'styled-components'

export const CastList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin: 0;
  padding: 0;
  list-style-type: none;
  margin-bottom: 1.6rem;
  margin-left: -0.4rem;
  margin-right: -0.4rem;
`

export const CastItem = styled.li`
  flex: 0 0 33.33333333%;
  padding: 0 0.3rem 0.6rem;
  min-width: 0;
  max-width: 33.33333333%;

  @media ${({ theme }) => theme.device.mobileL} {
    flex: 0 0 25%;
    max-width: 25%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    flex: 0 0 20%;
    max-width: 20%;
  }

  @media ${({ theme }) => theme.device.laptop} {
    flex: 0 0 25%;
    max-width: 25%;
  }

  @media ${({ theme }) => theme.device.laptopM} {
    flex: 0 0 20%;
    max-width: 20%;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    flex: 0 0 16.66666666666667%;
    max-width: 16.66666666666667%;
  }

  @media ${({ theme }) => theme.device.desktop} {
    flex: 0 0 12.5%;
    max-width: 12.5%;
  }
`

export const InfoContainer = styled.article`
  margin-bottom: 1rem;
`

export const InfoName = styled.h1`
  color: ${({ theme }) => theme.colors.white};
  font-size: 2rem;
`

export const InfoDescription = styled.p`
  font-size: 1rem;
`

export const InfoItem = styled.div`
  display: flex;
  align-items: center;
  strong {
    margin-right: 0.5rem;
  }
`

export const InfoItemStrong = styled.strong`
  color: ${({ theme }) => theme.colors.white};
`

export const InfoItemSpan = styled.span`
  color: ${({ theme }) => theme.colors.white};
`
