import { useRouter } from 'next/router'
import { DiscussionEmbed } from 'disqus-react'
import { Comments } from './styles'

export default function Disqus ({ _id, name }) {
  const router = useRouter()

  const url = 'https://doramasflix.co' + router.asPath

  return (
    <Comments>
      <DiscussionEmbed
        shortname='doramasflix'
        config={{
          url,
          identifier: _id,
          title: name
        }}
      />
    </Comments>
  )
}
