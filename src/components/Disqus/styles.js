import styled from 'styled-components'

export const Comments = styled.div`
  a {
    color: ${({ theme }) => theme.colors.primary};
  }
`
