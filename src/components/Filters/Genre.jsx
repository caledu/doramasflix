import { useQuery } from '@apollo/react-hooks'
import { FaFolderOpen } from 'react-icons/fa'
import { MdArrowDropDown } from 'react-icons/md'

import { CheckBox } from '../CheckBox'
import { GENRE_LIST } from 'gql/genre'
import {
  FilterItem,
  FilterSpan,
  FilterIcon,
  FilterChoise,
  FilterIconDown,
  FilterNav,
  FilterNavList,
  FilterNavItem,
  FilterNavText
} from './styles'

export function GenreFilter ({
  filter = {},
  changeFilter,
  menu,
  changeMenu,
  changePage
}) {
  const { data: { listGenres = [] } = {} } = useQuery(GENRE_LIST, {
    variables: { sort: 'NAME_ASC', limit: 0, skip: 0 }
  })

  const { genreIds = [] } = filter

  const visible = menu === 'genre'
  const open = () => changeMenu('genre')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  const handleChange = (checked, id) => {
    if (checked) {
      genreIds.push(id)
    } else {
      const index = genreIds.findIndex(item => item === id)
      genreIds.splice(index, 1)
    }
    if (genreIds.length === 0) {
      changeFilter({ ...filter, genreIds: undefined })
    } else {
      changeFilter({ ...filter, genreIds })
    }
    changePage(1)
  }

  return (
    <FilterItem>
      <FilterIcon>
        <FaFolderOpen size={20} />
      </FilterIcon>
      <FilterSpan>Generos: </FilterSpan>
      <FilterChoise active={genreIds.length > 0} onClick={toggle}>
        Elegir
        <FilterIconDown>
          <MdArrowDropDown size={30} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        <FilterNavList>
          {listGenres.map(({ _id, name }) => (
            <FilterNavItem key={_id}>
              <CheckBox
                value={_id}
                checked={!!genreIds.find(item => item === _id)}
                onChange={handleChange}
              />
              <FilterNavText>{name} </FilterNavText>
            </FilterNavItem>
          ))}
        </FilterNavList>
      </FilterNav>
    </FilterItem>
  )
}
