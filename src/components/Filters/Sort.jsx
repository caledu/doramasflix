import { MdArrowDropDown } from 'react-icons/md'
import { FaSortAmountDownAlt } from 'react-icons/fa'

import {
  FilterNav,
  FilterItem,
  FilterSpan,
  FilterIcon,
  FilterChoise,
  FilterNavList,
  FilterNavItem,
  FilterNavLink,
  FilterNavText,
  FilterIconDown
} from './styles'

export function SortFilter ({ sort, changeSort, menu, changeMenu }) {
  const label =
    sort === 'CREATEDAT_DESC'
      ? 'Últimas'
      : sort === 'POPULARITY_DESC'
      ? 'Populares'
      : 'Vistas'

  const visible = menu === 'sort'
  const open = () => changeMenu('sort')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <FilterItem>
      <FilterIcon>
        <FaSortAmountDownAlt size={20} />
      </FilterIcon>
      <FilterSpan>Ordenar por:</FilterSpan>
      <FilterChoise onClick={toggle}>
        {label}
        <FilterIconDown>
          <MdArrowDropDown size={30} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        <FilterNavList>
          <FilterNavItem all>
            <FilterNavLink
              active={sort === 'CREATEDAT_DESC'}
              onClick={() => changeSort('CREATEDAT_DESC')}
            >
              <FilterNavText>Últimas</FilterNavText>
            </FilterNavLink>
          </FilterNavItem>

          <FilterNavItem all>
            <FilterNavLink
              active={sort === 'POPULARITY_DESC'}
              onClick={() => changeSort('POPULARITY_DESC')}
            >
              <FilterNavText>Populares</FilterNavText>
            </FilterNavLink>
          </FilterNavItem>

          <FilterNavItem all>
            <FilterNavLink
              active={sort === 'VOTE_DESC'}
              onClick={() => changeSort('VOTE_DESC')}
            >
              <FilterNavText>Vistas</FilterNavText>
            </FilterNavLink>
          </FilterNavItem>
        </FilterNavList>
      </FilterNav>
    </FilterItem>
  )
}
