import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const FiltersContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  @media ${({ theme }) => theme.device.tablet} {
    flex-direction: row;
    justify-content: space-between;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    display: inline-block;
    float: right;
  }
`

export const FilterItem = styled.div`
  float: left;
  text-align: left;
  position: relative;
  padding: 0.3rem 1.6rem;
  margin-right: 0.5rem;

  @media ${({ theme }) => theme.device.laptop} {
    &:hover {
      cursor: pointer;
      aside {
        max-height: calc(100vh - 80px);
        height: auto;
        z-index: 10;
        opacity: 1;
        padding-bottom: 1rem;
        overflow: auto;
      }
    }
  }
`

export const FilterIcon = styled.div`
  position: relative;
  display: inline-block;

  svg {
    position: absolute;
    top: -16px;
    left: -24px;
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const FilterSpan = styled.span`
  display: inline-block;
  vertical-align: top;
  position: relative;
  z-index: 1;
  padding-right: 0.3rem;
`

export const FilterChoise = styled.a`
  display: inline-block;
  color: ${({ theme }) => theme.colors.white};

  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`

export const FilterIconDown = styled.div`
  position: relative;
  display: inline-block;

  svg {
    position: absolute;
    top: -20px;
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const FilterNav = styled.aside`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  z-index: -1;
  transition: 0.2s;
  max-height: 0;
  overflow: hidden;
  opacity: 0;
  border-top: 2px solid ${({ theme }) => theme.colors.primary};
  box-shadow: 0 5px 25px
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};
  margin: 0;
  padding: 0;
  display: block;
  list-style-type: none;
  height: 0;

  @media ${({ theme }) => theme.device.tablet} {
    width: 20rem;
    overflow: visible;
  }

  ${({ visible }) =>
    visible &&
    css`
      max-height: calc(100vh - 80px);
      height: auto;
      z-index: 10;
      opacity: 1;
      padding-bottom: 1rem;
      overflow: auto;
    `}
`

export const FilterNavList = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: flex;
  flex-wrap: wrap;
`

export const FilterNavItem = styled.li`
  position: relative;
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  padding: 0.5rem 1.2rem;
  width: 100%;
  display: flex;
  align-items: center;

  ${({ all }) =>
    !all &&
    css`
      @media ${({ theme }) => theme.device.mobileM} {
        flex: 0 0 50%;
        max-width: 50%;
      }

      @media ${({ theme }) => theme.device.mobileXL} {
        flex: 0 0 33.33333%;
        max-width: 33.33333%;
      }

      @media ${({ theme }) => theme.device.tablet} {
        flex: 0 0 50%;
        max-width: 50%;
      }
    `}
`

export const FilterNavLink = styled.a`
  display: block;
  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => theme.colors.primary};
    `}
`

export const FilterNavText = styled.span`
  display: inline-block;
  line-height: 2.5rem;
  font-size: 1rem;
  margin-left: 0.6rem;
  max-width: 7rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-height: 2rem;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`
