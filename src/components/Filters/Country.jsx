import { FaMapMarkerAlt } from 'react-icons/fa'
import { MdArrowDropDown } from 'react-icons/md'

import { CheckBox } from '../CheckBox'
import { countries as listCountries } from 'utils/constans'
import {
  FilterItem,
  FilterSpan,
  FilterIcon,
  FilterChoise,
  FilterIconDown,
  FilterNav,
  FilterNavList,
  FilterNavItem,
  FilterNavText
} from './styles'

export function CountryFilter ({
  menu,
  changeMenu,
  filter = {},
  changeFilter,
  changePage
}) {
  const { countries = [] } = filter

  const handleChange = (checked, id) => {
    if (checked) {
      countries.push(id)
    } else {
      const index = countries.findIndex(item => item === id)
      countries.splice(index, 1)
    }
    if (countries.length === 0) {
      changeFilter({ ...filter, countries: undefined })
    } else {
      changeFilter({ ...filter, countries })
    }
    changePage(1)
  }

  const visible = menu === 'country'
  const open = () => changeMenu('country')
  const close = () => changeMenu('')
  const toggle = visible ? close : open

  return (
    <FilterItem>
      <FilterIcon>
        <FaMapMarkerAlt size={20} />
      </FilterIcon>
      <FilterSpan>País:</FilterSpan>
      <FilterChoise active={countries.length > 0} onClick={toggle}>
        Elegir
        <FilterIconDown>
          <MdArrowDropDown size={30} />
        </FilterIconDown>
      </FilterChoise>
      <FilterNav visible={visible}>
        <FilterNavList>
          {listCountries.map(({ slug, name }) => (
            <FilterNavItem key={slug}>
              <CheckBox
                value={slug}
                checked={!!countries.find(item => item === slug)}
                onChange={handleChange}
              />
              <FilterNavText>{name}</FilterNavText>
            </FilterNavItem>
          ))}
        </FilterNavList>
      </FilterNav>
    </FilterItem>
  )
}
