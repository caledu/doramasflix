import ReactPaginate from 'react-paginate'
import { PaginationContainer } from './styles'
import { TiArrowLeftThick, TiArrowRightThick } from 'react-icons/ti'

export function Pagination ({ count, perPage, onChange, deviceType, page }) {
  const handleChange = ({ selected }) => {
    onChange(selected + 1)
    document.documentElement.scrollTop = 0
  }

  const displayed = deviceType === 'laptop' ? 3 : 2
  const margin = deviceType === 'laptop' ? 3 : deviceType === 'tablet' ? 2 : 1

  return (
    <PaginationContainer>
      <ReactPaginate
        forcePage={page - 1}
        initialPage={page - 1}
        previousLabel={<TiArrowLeftThick size={20} />}
        nextLabel={<TiArrowRightThick size={20} />}
        breakLabel={'...'}
        pageCount={parseInt(count / perPage) + 1}
        marginPagesDisplayed={margin}
        pageRangeDisplayed={displayed}
        onPageChange={handleChange}
      />
    </PaginationContainer>
  )
}
