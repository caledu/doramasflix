import styled from 'styled-components'

export const Share = styled.div`
  width: 100%;
  padding: 5px 10px;
  margin: 10px 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const Title = styled.span`
  width: 100%;
  font-weight: bold;
  font-size: 1.1rem;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  line-break: 0;
`

export const SocialName = styled.span`
  width: 100%;
  font-weight: bold;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  line-break: 0;
`

export const Buttons = styled.div`
  display: flex;
  flex-direction: 'row';
  justify-content: center;
  flex-wrap: wrap;

  button {
    width: auto;
    margin: 5px 5px;
  }
`
