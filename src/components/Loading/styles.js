import styled from 'styled-components'

export const LoadingContainer = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1.2rem;

  div {
    margin-right: 0.5rem;
  }
`
