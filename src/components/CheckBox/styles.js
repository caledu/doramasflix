import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const Checkbox = styled.div`
  display: inline-block;
  position: relative;
  width: 22px;
  height: 22px;

  ${({ checked }) =>
    checked &&
    css`
      a {
        display: block;
      }
    `}
`

export const Input = styled.input`
  background-color: ${({ theme }) => theme.colors.gray};
  display: block;
  width: 100%;
  height: 100%;
  margin-top: 2px;
  height: 1.5rem;
  cursor: pointer;

  border: 1px solid ${({ theme }) => addOpacityToColor(theme.colors.gray, 0.5)};

  &:checked {
    background-color: ${({ theme }) => theme.colors.primary};
    border: 1px solid
      ${({ theme }) => addOpacityToColor(theme.colors.primary, 0.5)};
  }

  &:not(:focus) {
    opacity: 1;
  }
`

export const InputIcon = styled.a`
  display: none;
  position: absolute;
  top: 1px;
  left: 4px;

  svg {
    color: ${({ theme }) => theme.colors.white};
  }
`
