import { FaCheck } from 'react-icons/fa'

import { Input, InputIcon, Checkbox } from './styles'

export function CheckBox ({ checked, value, onChange }) {
  const handleChange = e => {
    const check = e.target.checked
    onChange(check, value)
  }
  return (
    <Checkbox checked={checked}>
      <Input
        type='checkbox'
        name={value}
        checked={checked}
        onChange={handleChange}
      />
      <InputIcon onClick={() => onChange(false, value)}>
        <FaCheck />
      </InputIcon>
    </Checkbox>
  )
}
