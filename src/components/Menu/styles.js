import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'
import IMG from 'react-cool-img'

/* MENU */
export const MenuButton = styled.a`
  text-align: center;
  padding: 0.6rem 0.5rem 0.5rem;
  width: 40px;
  height: 40px;
  float: left;
  margin: 20px 20px 20px 0;
  @media ${({ theme }) => theme.device.laptop} {
    display: none;
  }
  svg {
    color: ${({ theme, visible }) =>
      visible ? theme.colors.primary : theme.colors.white};
  }
`

export const Nav = styled.nav`
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 10;
  transition: 0.2s;
  max-height: 0;
  overflow: hidden;
  opacity: 0;
  border-top: 2px solid transparent;
  border-top-color: ${({ theme }) => theme.colors.primary};
  box-shadow: 0 5px 25px ${({ theme }) =>
    addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};
  margin: 0;
  padding: 0;
  display: block;
  list-style-type: none;
  
  ${({ visible }) =>
    visible &&
    css`
      max-height: calc(100vh - 80px);
      opacity: 1;
      padding-bottom: 1rem;
      overflow: auto;
    `}
  
  @media ${({ theme }) => theme.device.laptop} {
    max-height: calc(100vh - 80px);
    opacity: 1;
    padding-bottom: 1rem;
    overflow: visible;
    top: 25px;
    z-index: 1;
    border-top: 0px;
    box-shadow: none;
    background-color: transparent;
    padding-bottom: 0px;
    margin: 0 100px 0 200px;
  }
  @media ${({ theme }) => theme.device.laptopS} {
    top: 25px;
    margin: 0 100px 0 200px;
  }
  @media ${({ theme }) => theme.device.laptopM} {
    top: 23px;
  }
`

export const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;

  @media ${({ theme }) => theme.device.laptop} {
    display: inline-block;
  }
`

export const Item = styled.li`
  position: relative;
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  padding: 0 0.5rem;
  margin: 0 0.1rem;
  line-height: 2.5rem;
  font-size: 0.7rem;
  margin-left: 14px;

  @media ${({ theme }) => theme.device.laptop} {
    float: left;
    position: relative;
    border-bottom: 0px;
    margin: 0;
    ul {
      opacity: 0;
      max-height: 0;
      height: 0;
      display: none;
    }
    span {
      display: none;
    }
    &:hover {
      ul {
        opacity: 1;
        display: block;
        z-index: 20;
        max-height: 40em;
        height: auto;
      }
      span {
        display: block;
      }
      aside {
        display: block;
      }
    }
    margin-right: 10px;
    margin-left: 0px;
  }
  @media ${({ theme }) => theme.device.laptopS} {
    margin-right: 10px;
  }
  @media ${({ theme }) => theme.device.laptopM} {
    margin-right: 12px;
  }
`

export const ItemMenu = styled.a`
  text-transform: uppercase;
  font-weight: 700;
  color: ${({ theme }) => theme.colors.white};
  padding-left: 0.7rem;
  display: flex;
  rotate: 90°;

  @media ${({ theme }) => theme.device.laptopL} {
    margin-right: 16px;
  }
`

export const ItemIcon = styled.div`
  position: absolute;
  left: -1px;
  top: 3px;
  z-index: 1;

  svg {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const ItemIconDown = styled.div`
  width: 0;
  height: 0;
  position: relative;

  svg {
    position: absolute;
    right: -20px;
    top: 8px;
    transform: rotate(-90deg);
    color: ${({ theme }) => theme.colors.primary};
  }

  @media ${({ theme }) => theme.device.laptop} {
    display: block;
    right: 1rem;
    svg {
      transform: rotate(0deg);
      right: -36px;
      top: 7px;
    }
  }

  @media ${({ theme }) => theme.device.laptopM} {
    right: 2rem;
    top: 4px;
    svg {
      right: -55px;
      top: 4px;
    }
  }
`

export const ItemIconUp = styled.span`
  width: 0;
  height: 0;
  position: absolute;
  left: 2px;
  top: 12px;
  bottom: 0;
  display: none;
  svg {
    color: ${({ theme }) => theme.colors.primary};
  }

  @media ${({ theme }) => theme.device.laptop} {
    display: ${({ visible }) => (visible ? 'block' : 'none')};
  }
`

export const IconPlay = styled.div`
  width: 0;
  height: 0;
  position: relative;

  svg {
    position: absolute;
    top: -10px;
    left: -25px;
  }
`

export const ListDiv = styled.aside`
  box-shadow: none;
  display: block;

  @media ${({ theme }) => theme.device.laptop} {
    display: none;
  }
`

export const SubMenu = styled.ul`
  box-shadow: none;
  max-height: none;
  transition: 0.2s;
  overflow: hidden;
  opacity: 1;
  margin: 0;
  padding: 0;
  padding-bottom: 1rem;
  list-style-type: none;

  @media ${({ theme }) => theme.device.laptop} {
    width: 260px;
    background-color: ${({ theme }) => theme.colors.bg};
    position: absolute;
    top: 100%;
    max-height: 0;
    opacity: 0;
    display: none;
    z-index: 10;
    box-shadow: inset 0 0 70px
        ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
      0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
    border-top: 3px solid ${({ theme }) => theme.colors.primary};
    padding: 5px 0px 1rem;
  }
`

export const SubMenuItem = styled.li`
  float: left;
  padding: 0 0.4rem;
  margin-top: 0.5rem;
  display: list-item;
  line-height: 2rem;
  width: 100%;

  img[class*='styles__SubMenuLogo'] {
    display: none;
  }

  @media ${({ theme }) => theme.device.mobileM} {
    width: 50%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    width: 33.33333%;
    img[class*='styles__SubMenuLogo'] {
      display: initial;
    }
  }

  @media ${({ theme }) => theme.device.laptop} {
    width: 100%;

    ${({ two }) =>
      two &&
      css`
        width: 50%;
      `}

    &:hover {
      background-color: ${({ theme }) => theme.colors.gray};
      a {
        background-color: ${({ theme }) => theme.colors.gray};
      }
    }
  }
`

export const SubMenuItemLink = styled.a`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.gray};
  padding: 0 0.5rem;
  font-size: 0.7rem;
  font-weight: 300;
  border-radius: 4px;
  opacity: 0.7;
  display: block;
  transform: rotate(0);
  backface-visibility: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  text-align: left;
  text-transform: uppercase;
  ${({ center }) =>
    center &&
    css`
      display: flex;
      align-items: center;
      justify-content: center;
    `};

  @media ${({ theme }) => theme.device.laptop} {
    background-color: ${({ theme }) => theme.colors.bg};
    text-align: left;
  }
`

export const SubMenuLogo = styled(IMG)`
  float: right;
  margin-top: 8px;
  max-width: 2.2rem;
`

export const SubMenuImg = styled.img`
  float: right;
  margin-top: 4px;
`
