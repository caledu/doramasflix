import dynamic from 'next/dynamic'
import { useNearScreen } from 'hooks/useNearScreen'
import { MdOndemandVideo, MdArrowDropDown, MdArrowDropUp } from 'react-icons/md'
import { ListDiv, ItemIcon, ItemMenu, ItemIconUp, ItemIconDown } from './styles'

const GenreList = dynamic(() => import('./GenreList'))

export function Genres () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '0px'
  })
  return (
    <>
      <ItemMenu>
        <ItemIcon>
          <MdOndemandVideo size={15} />
        </ItemIcon>
        Generos
        <ItemIconDown>
          <MdArrowDropDown size={25} />
        </ItemIconDown>
      </ItemMenu>
      <ItemIconUp>
        <MdArrowDropUp size={50} />
      </ItemIconUp>
      <ListDiv ref={fromRef}>{isNearScreen && <GenreList />}</ListDiv>
    </>
  )
}
