import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'

import { GENRE_LIST } from 'gql/genre'
import { SubMenu, SubMenuItem, SubMenuItemLink } from './styles'
import { Loading } from 'components/Loading'

export default function GenresList () {
  const { data: { listGenres = [] } = {}, loading } = useQuery(GENRE_LIST, {
    variables: { sort: 'NAME_ASC' },
    ssr: false
  })

  return (
    <SubMenu>
      {loading && <Loading />}
      {listGenres.map(({ name, slug }, idx) => (
        <SubMenuItem key={idx} two>
          <Link href={`/generos/[slug]`} as={`/generos/${slug}`}>
            <SubMenuItemLink>{name}</SubMenuItemLink>
          </Link>
        </SubMenuItem>
      ))}
    </SubMenu>
  )
}
