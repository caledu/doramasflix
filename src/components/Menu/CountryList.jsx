import Link from 'next/link'
import { countries } from 'utils/constans'
import { SubMenu, SubMenuImg, SubMenuItem, SubMenuItemLink } from './styles'

export default function Countries () {
  return (
    <SubMenu>
      {countries.map(({ slug, name }, i) => (
        <SubMenuItem key={i}>
          <Link href='/paises/[slug]' as={`/paises/${slug}`}>
            <SubMenuItemLink>
              {name}
              <SubMenuImg
                src={`/img/lang/${slug}.svg`}
                width='20'
                height='20'
              />
            </SubMenuItemLink>
          </Link>
        </SubMenuItem>
      ))}
    </SubMenu>
  )
}
