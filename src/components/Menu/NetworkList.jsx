import Link from 'next/link'
import { useQuery } from '@apollo/react-hooks'

import { NETWORK_LIST } from 'gql/network'
import { URL_LOGO_NETWORK } from 'utils/urls'
import { Loading } from 'components/Loading'
import { SubMenu, SubMenuLogo, SubMenuItem, SubMenuItemLink } from './styles'

export default function Networks () {
  const { data: { listNetworks = [] } = {}, loading } = useQuery(NETWORK_LIST, {
    variables: { sort: 'NUMBER_OF_DORAMAS_DESC', limit: 7 },
    ssr: false
  })

  return (
    <SubMenu>
      {loading && <Loading />}
      {listNetworks.map(({ name, logo, slug }, idx) => (
        <SubMenuItem key={idx}>
          <Link href={`/productoras/[slug]`} as={`/productoras/${slug}`}>
            <SubMenuItemLink>
              {name}
              <SubMenuLogo
                src={URL_LOGO_NETWORK + logo}
                width='40'
                height='auto'
              />
            </SubMenuItemLink>
          </Link>
        </SubMenuItem>
      ))}
      <SubMenuItem>
        <Link href={`/productoras`}>
          <SubMenuItemLink>Todas las Productoras</SubMenuItemLink>
        </Link>
      </SubMenuItem>
    </SubMenu>
  )
}
