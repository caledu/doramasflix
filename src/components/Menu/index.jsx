import Link from 'next/link'
import { useState } from 'react'
import {
  MdMenu,
  MdHome,
  MdClose,
  MdLocalMovies,
  MdMovieFilter,
  MdVideoLibrary
} from 'react-icons/md'

import { Genres } from './Genres'
import { Networks } from './Networks'
import { Countries } from './Countries'
import { Nav, List, Item, ItemIcon, ItemMenu, MenuButton } from './styles'

export function Menu () {
  const [visibe, changeVisible] = useState(false)

  const toggleVisible = () => changeVisible(!visibe)

  return (
    <>
      <MenuButton visible={visibe} onClick={toggleVisible}>
        {visibe ? <MdClose size={27} /> : <MdMenu size={27} />}
      </MenuButton>
      <Nav visible={visibe}>
        <List>
          <Item>
            <Link href='/doramas'>
              <ItemMenu>
                <ItemIcon>
                  <MdVideoLibrary size={15} />
                </ItemIcon>
                Doramas
              </ItemMenu>
            </Link>
          </Item>
          <Item>
            <Link href='/peliculas'>
              <ItemMenu>
                <ItemIcon>
                  <MdLocalMovies size={15} />
                </ItemIcon>
                Peliculas
              </ItemMenu>
            </Link>
          </Item>
          <Item>
            <Genres />
          </Item>
          <Item>
            <Networks />
          </Item>
          <Item>
            <Countries />
          </Item>
        </List>
      </Nav>
    </>
  )
}
