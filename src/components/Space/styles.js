import styled from 'styled-components'

export const SpaceContainer = styled.div`
  width: ${({ width }) => width || 0};
  height: ${({ height }) => height || 0};
`
