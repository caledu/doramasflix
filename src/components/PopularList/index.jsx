import CarouselLib from 'react-multi-carousel'

import { Card } from '../Card'
import { Section, Item, Dot } from './styles'
import {
  URL_IMAGE_POSTER_1X,
  URL_IMAGE_POSTER_2X,
  URL_IMAGE_POSTER_3X
} from 'utils/urls'

export function PopularList ({ list, deviceType }) {
  const CustomDot = ({ index, onClick, active }) => {
    return (
      <Dot
        key={index}
        active={active}
        onClick={e => {
          onClick()
          e.preventDefault()
        }}
      />
    )
  }

  const x1 =
    deviceType === 'desktop' ? URL_IMAGE_POSTER_2X : URL_IMAGE_POSTER_1X
  const x2 =
    deviceType === 'desktop' ? URL_IMAGE_POSTER_3X : URL_IMAGE_POSTER_2X

  return (
    <Section>
      <CarouselLib
        ssr
        showDots
        draggable
        swipeable
        arrows={false}
        renderDotsOutside
        keyBoardControl={false}
        responsive={responsive}
        deviceType={deviceType}
        customDot={<CustomDot />}
      >
        {list.map((item, idx) => (
          <Item key={idx}>
            <Card
              href={
                item.__typename === 'Movie'
                  ? '/peliculas/[slug]'
                  : '/doramas/[slug]'
              }
              as={
                item.__typename === 'Movie'
                  ? `/peliculas/${item.slug}`
                  : `/doramas/${item.slug}`
              }
              image={x1 + item.poster_path}
              srcSet={`${x1}${item.poster_path} 1x, ${x2}${item.poster_path} 2x`}
              title={item.name}
              label={item.__typename === 'Movie' ? 'Pelicula' : 'Dorama'}
            />
          </Item>
        ))}
      </CarouselLib>
    </Section>
  )
}

const responsive = {
  desktopL: {
    breakpoint: { max: 3000, min: 1950 },
    items: 12,
    slidesToSlide: 12
  },
  desktopM: {
    breakpoint: { max: 1949, min: 1650 },
    items: 11,
    slidesToSlide: 11
  },
  desktop: {
    breakpoint: { max: 1649, min: 1440 },
    items: 10,
    slidesToSlide: 10
  },
  laptopL: {
    breakpoint: { max: 1439, min: 1260 },
    items: 9,
    slidesToSlide: 9
  },
  laptopM: {
    breakpoint: { max: 1259, min: 1150 },
    items: 8,
    slidesToSlide: 8
  },
  laptop: {
    breakpoint: { max: 1149, min: 1024 },
    items: 7,
    slidesToSlide: 7
  },
  tabletL: {
    breakpoint: { max: 1023, min: 900 },
    items: 6,
    slidesToSlide: 6
  },
  tabletM: {
    breakpoint: { max: 899, min: 768 },
    items: 5,
    slidesToSlide: 5
  },
  tablet: {
    breakpoint: { max: 767, min: 425 },
    items: 4,
    slidesToSlide: 4
  },
  mobile: {
    breakpoint: { max: 424, min: 0 },
    items: 3,
    slidesToSlide: 3
  }
}
