import styled, { css } from 'styled-components'

export const Section = styled.section`
  display: block;
  overflow: visible;
  position: relative;
  margin-bottom: 2rem;

  .react-multi-carousel-dot-list {
    top: -40px;
    left: auto;
  }
`

export const Dot = styled.a`
  width: 12px;
  height: 12px;
  opacity: 0.5;
  margin-right: 5px;
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.white};

  ${({ active }) =>
    active &&
    css`
      opacity: 1;
      z-index: 1;
      transform: scale(1.3);
      background-color: ${({ theme }) => theme.colors.primary};
    `}
`

export const Item = styled.div`
  margin-right: 0.4rem;
`
