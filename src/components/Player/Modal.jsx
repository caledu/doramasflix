import Modal from 'react-modal'
import { useMutation } from '@apollo/react-hooks'
import {
  ModalStyle,
  ModalTitle,
  ModalText,
  ModalOption,
  ModalOptionIcon,
  ModalOptionName,
  ModalOptions,
  ModalTextArea,
  ModalClose,
  ModalButton,
  ModalTelegram,
  ModalOptionLink
} from './styles'
import { colors } from 'styles/theme'
import {
  MdRadioButtonChecked,
  MdRadioButtonUnchecked,
  MdClose
} from 'react-icons/md'
import { useState } from 'react'
import { CREATE_PROBLEM } from 'gql/report'
import { Alert } from 'components/Alert'
import { Button } from 'components/Info/styles'
import { FaTelegramPlane } from 'react-icons/fa'

export function PlayerModal ({
  visible,
  changeVisible,
  episode,
  option,
  movie
}) {
  const [select, changeSelect] = useState('')
  const [text, changeText] = useState('')
  const [success, changeSuccess] = useState(false)
  const customStyles = {
    overlay: {
      backgroundColor: 'rgba(0,0,0,.8)',
      zIndex: 99,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    content: {
      zIndex: 100,
      maxHeight: '80%',
      maxWidth: '90%',
      minWidth: '40%',
      position: 'relative',
      background: colors.bg,
      color: colors.white,
      border: 0,
      top: 0,
      left: 0,
      right: 0
    }
  }

  const [createProblem, { loading }] = useMutation(CREATE_PROBLEM)

  const sendReport = () => {
    createProblem({
      variables: {
        record: {
          dorama_id: episode && episode.serie_id,
          movie_id: movie && movie._id,
          serie_name: movie ? movie.name : episode.serie_name,
          season_number: episode && episode.season_number,
          episode_number: episode && episode.episode_number,
          type_serie: movie ? 'movie' : 'dorama',
          type: select,
          server: option,
          text
        }
      }
    }).then(() => {
      changeSuccess(true)
    })
  }

  return (
    <ModalStyle>
      <Modal
        isOpen={visible}
        onRequestClose={() => changeVisible(false)}
        style={customStyles}
        contentLabel='Reportar episodio'
      >
        <ModalClose onClick={() => changeVisible(false)}>
          <MdClose size={25} />
        </ModalClose>
        <ModalTitle>Reportar video</ModalTitle>
        {success ? (
          <Alert
            type='success'
            message='Hemos recibido con éxito tu reporte, gracias.'
          />
        ) : (
          <>
            <ModalText>
              Cuentanos el problema que presentaste con el video:
            </ModalText>
            <ModalOptions>
              <ModalOption>
                <ModalOptionLink
                  onClick={() => changeSelect('No tiene opciones')}
                >
                  <ModalOptionIcon>
                    {select === 'No tiene opciones' ? (
                      <MdRadioButtonChecked size={25} />
                    ) : (
                      <MdRadioButtonUnchecked size={25} />
                    )}
                  </ModalOptionIcon>
                  <ModalOptionName>No tiene opciones</ModalOptionName>
                </ModalOptionLink>
              </ModalOption>
              <ModalOption>
                <ModalOptionLink
                  onClick={() => changeSelect('Video eliminado')}
                >
                  <ModalOptionIcon>
                    {select === 'Video eliminado' ? (
                      <MdRadioButtonChecked size={25} />
                    ) : (
                      <MdRadioButtonUnchecked size={25} />
                    )}
                  </ModalOptionIcon>
                  <ModalOptionName>Video eliminado</ModalOptionName>
                </ModalOptionLink>
              </ModalOption>
              <ModalOption>
                <ModalOptionLink
                  onClick={() => changeSelect('Lentitud en los servidores')}
                >
                  <ModalOptionIcon>
                    {select === 'Lentitud en los servidores' ? (
                      <MdRadioButtonChecked size={25} />
                    ) : (
                      <MdRadioButtonUnchecked size={25} />
                    )}
                  </ModalOptionIcon>
                  <ModalOptionName>Lentitud en los servidores</ModalOptionName>
                </ModalOptionLink>
              </ModalOption>
              <ModalOption>
                <ModalOptionLink onClick={() => changeSelect('No se escucha')}>
                  <ModalOptionIcon>
                    {select === 'No se escucha' ? (
                      <MdRadioButtonChecked size={25} />
                    ) : (
                      <MdRadioButtonUnchecked size={25} />
                    )}
                  </ModalOptionIcon>
                  <ModalOptionName>No se escucha</ModalOptionName>
                </ModalOptionLink>
              </ModalOption>
              <ModalOption>
                <ModalOptionLink onClick={() => changeSelect('Otro')}>
                  <ModalOptionIcon>
                    {select === 'Otro' ? (
                      <MdRadioButtonChecked size={25} />
                    ) : (
                      <MdRadioButtonUnchecked size={25} />
                    )}
                  </ModalOptionIcon>
                  <ModalOptionName>Otro</ModalOptionName>
                </ModalOptionLink>
                {select === 'Otro' && (
                  <ModalTextArea
                    rows='4'
                    placeholder='Detalla el problema que has presentado...'
                    onChange={e => changeText(e.target.value)}
                  ></ModalTextArea>
                )}
              </ModalOption>
            </ModalOptions>
            <ModalButton disabled={!select || loading} onClick={sendReport}>
              Reportar
            </ModalButton>
            <ModalTelegram>
              También puedes escribirnos a
              <Button
                noUpper
                color='#0088cc'
                maxWidth='200px'
                href='https://t.me/doramasflixgo'
                target='_blank'
              >
                <FaTelegramPlane size={16} />
                @doramasflixgo
              </Button>
            </ModalTelegram>
          </>
        )}
      </Modal>
    </ModalStyle>
  )
}
