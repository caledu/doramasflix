import Link from 'next/link'
import { useState, useEffect } from 'react'
import { MdPlaylistPlay } from 'react-icons/md'
import { AiFillAlert } from 'react-icons/ai'
import { FaPlay, FaStepBackward, FaStepForward } from 'react-icons/fa'
import { FaTelegramPlane } from 'react-icons/fa'
import { Container } from 'styles/base'
import { PlayerModal } from './Modal'
import { ImageBackground } from '../ImageBackgound'
import { languages, servers, langsName } from 'utils/constans'
import {
  Video,
  NoOptions,
  VideoFrame,
  PlayerImage,
  VideoTitle,
  PlayerVideo,
  PlayerIFrame,
  PlayerOption,
  VideoControl,
  VideoControls,
  PlayerOptions,
  PlayerContainer,
  PlayerOptionText,
  VideoControlText,
  PlayerOptionsCont,
  PlayerOptionLink,
  PlayerOptionTitle,
  PlayerVideoOptions,
  PlayerOptionReport,
  PlayerOptionSubTitle,
  NoOptionsText
} from './styles'
import { Button } from 'components/Info/styles'
import { Space } from 'components/Space'

export function Player ({
  next,
  prev,
  slug,
  title,
  links,
  backdrop_path,
  episode,
  air_date
}) {
  const [optionActual, changeOption] = useState(links[0])
  const [visible, changeVisible] = useState(false)
  const [options, changeOptions] = useState([])

  const date = new Date()
  date.setDate(date.getDate() - 1)
  date.setHours(0, 0, 0, 0)

  const air = new Date(air_date)
  air.setHours(6, 0, 0, 0)

  const isBeforeDate =
    links.length > 0 ? false : air.getTime() >= date.getTime()

  var optionsDate = { year: 'numeric', month: 'long', day: 'numeric' }
  const date_text = air.toLocaleDateString('es-ES', optionsDate)

  useEffect(() => {
    if (links.length > 0) {
      let options = links.filter(
        it =>
          !(it.server == '14707' || it.server == '8310' || it.server == '1231')
      )

      options
        .sort((a, b) => a.lang - b.lang)
        .forEach((it, idx) => (it.idx = idx))
      changeOptions(options)
      changeOption(options[0])
    }
  }, [links, episode])

  return (
    <PlayerContainer>
      <Container>
        <PlayerVideoOptions>
          <PlayerVideo>
            <VideoTitle>{title}</VideoTitle>
            <Video>
              <VideoFrame>
                {optionActual ? (
                  <PlayerIFrame
                    src={optionActual.link}
                    allowFullScreen
                    frameborder='0'
                  />
                ) : isBeforeDate ? (
                  <>
                    <NoOptions>
                      <NoOptionsText>
                        El episodio estará disponible {date_text}
                      </NoOptionsText>
                      Normalmente los episodios tardan de 5 a 6 horas en ser
                      subtituladas al español, después de su estreno mundial en
                      Asia. Así que normalmente, se suben por la tarde en
                      horario de Mexico. ¡Atentos a nuestras redes sociales!
                      <Space height='10px' />
                      <Button
                        noUpper
                        color='#0088cc'
                        maxWidth='200px'
                        href='https://t.me/doramasflixgo'
                        target='_blank'
                      >
                        <FaTelegramPlane size={16} />
                        @doramasflixgo
                      </Button>
                    </NoOptions>
                  </>
                ) : (
                  <NoOptions>
                    <NoOptionsText>No hay opciones disponibles</NoOptionsText>
                    Reportalo para que podamos solucionarlo lo mas pronto
                    posible
                  </NoOptions>
                )}
              </VideoFrame>
            </Video>
            <VideoControls>
              <Link
                href='/capitulos/[slug]'
                as={`/capitulos/${prev && prev.slug}`}
              >
                <VideoControl disabled={!prev}>
                  <FaStepBackward size={15} />
                  <VideoControlText> Anterior</VideoControlText>
                </VideoControl>
              </Link>
              <Link href='/doramas/[slug]' as={`/doramas/${slug}`}>
                <VideoControl>
                  <MdPlaylistPlay size={25} />
                  <VideoControlText> Lista de episodios</VideoControlText>
                </VideoControl>
              </Link>
              <Link
                href='/capitulos/[slug]'
                as={`/capitulos/${next && next.slug}`}
              >
                <VideoControl disabled={!next}>
                  <VideoControlText>Siguiente </VideoControlText>
                  <FaStepForward size={15} />
                </VideoControl>
              </Link>
            </VideoControls>
          </PlayerVideo>
          <PlayerOptionsCont>
            <PlayerOption>
              <PlayerOptionTitle>
                <FaPlay size={16} />
                <PlayerOptionSubTitle>Opciones</PlayerOptionSubTitle>
              </PlayerOptionTitle>
            </PlayerOption>
            <PlayerOptions>
              {options.map((option, idx) => (
                <PlayerOption key={idx}>
                  <PlayerOptionLink
                    active={option.idx === optionActual?.idx}
                    onClick={() => changeOption(option)}
                  >
                    <PlayerImage
                      src={`/img/lang/${languages[option.lang]}.svg`}
                    />
                    <PlayerOptionSubTitle>
                      opción {idx + 1}
                    </PlayerOptionSubTitle>
                    <PlayerOptionText>
                      {servers[option.server]} | {langsName[option.lang]}
                    </PlayerOptionText>
                  </PlayerOptionLink>
                </PlayerOption>
              ))}
            </PlayerOptions>
            {!isBeforeDate && (
              <PlayerOptionReport onClick={() => changeVisible(true)}>
                <PlayerOptionTitle>
                  <AiFillAlert size={20} />
                  <PlayerOptionSubTitle>Reportar</PlayerOptionSubTitle>
                </PlayerOptionTitle>
              </PlayerOptionReport>
            )}
          </PlayerOptionsCont>
        </PlayerVideoOptions>
        {backdrop_path && <ImageBackground backdrop_path={backdrop_path} />}
        <PlayerModal
          visible={visible}
          changeVisible={changeVisible}
          episode={episode}
          option={optionActual}
        />
      </Container>
    </PlayerContainer>
  )
}
