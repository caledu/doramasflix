import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const PlayerContainer = styled.div`
  padding-bottom: 0;
  position: relative;
  padding: 5rem 0 0.5rem 0;
  margin-bottom: 0.5rem;
  z-index: 1;

  @media ${({ theme }) => theme.device.laptop} {
    padding: 8rem 0 0.5rem;
  }
`

export const PlayerVideoOptions = styled.div`
  display: block;

  @media ${({ theme }) => theme.device.laptop} {
    display: flex;
    flex-wrap: wrap;
  }
`

export const PlayerVideo = styled.div`
  position: relative;
  width: 100%;
  margin-bottom: 1rem;

  @media ${({ theme }) => theme.device.laptop} {
    width: 68%;
    margin-bottom: 0;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    width: 78%;
    margin: 0 0 0 0.5rem;
  }
`

export const Video = styled.div`
  display: block;
  position: relative;
  min-height: 250px;
  max-height: ${({ movie }) => (movie ? '440px' : '340px')};
  overflow: hidden;
  animation: scale 0.7s ease-in-out;

  &::before {
    content: '';
    display: block;
    padding-top: 56.25%;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    max-height: 500px;
  }
`

export const VideoTitle = styled.h1`
  width: 100%;
  height: 50px;
  position: relative;
  font-size: 1rem;
  margin-bottom: 0;
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.black};
  padding: 10px 10px;
  text-align: center;
`

export const VideoControls = styled.div`
  display: flex;
  width: 100%;
  position: relative;
  justify-content: space-between;
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.black};
`

export const VideoControl = styled.a`
  width: 33.333%;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    margin: 5px;
    transform: scale(1.3);
  }

  ${({ disabled }) =>
    disabled &&
    css`
      opacity: 0.5;
      cursor: not-allowed;
      text-decoration: none;
      pointer-events: none;
    `}

  &:hover {
    ${({ disabled }) =>
      !disabled &&
      css`
        background-color: ${({ theme }) => theme.colors.gray};
        color: ${({ theme }) => theme.colors.white};
      `}
  }

  @media ${({ theme }) => theme.device.laptop} {
    svg {
      transform: scale(1);
    }
    span {
      display: block;
    }
  }
`

export const VideoControlText = styled.span`
  font-size: 1rem;
  display: none;
`

export const VideoFrame = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border: 0;
  max-width: 100%;
  display: inline-block;
  vertical-align: middle;
`

export const PlayerIFrame = styled.iframe`
  width: 100%;
  height: 100%;
  margin: 0;
  font-size: 0;
`

export const NoOptions = styled.p`
  width: 100%;
  height: 100%;
  margin: 0;
  background-color: ${({ theme }) => theme.colors.black};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 10px 20px;
  text-align: center;
  font-size: 0.8rem;
  padding-bottom: 0;
  line-height: 1rem;
`

export const NoOptionsText = styled.span`
  font-size: 1.1rem;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  margin-bottom: 20px;
`

export const PlayerOptionsCont = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  max-height: 440px;
  background-color: ${({ theme }) => theme.colors.bg};
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};

  @media ${({ theme }) => theme.device.laptop} {
    width: 30%;
    margin: 0 0 0 0.5rem;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    width: 20%;
    max-height: 640px;
  }
`

export const PlayerOptions = styled.ul`
  width: 100%;
  max-height: 320px;
  padding: 0;
  margin: 0;
  overflow: auto;
  list-style-type: none;
  margin-bottom: 5px;

  @media ${({ theme }) => theme.device.laptop} {
    max-height: 320px;
    margin-bottom: 5px;
  }

  @media ${({ theme }) => theme.device.laptopL} {
    max-height: 480px;
  }
`

export const PlayerOption = styled.li`
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  display: flex;
  align-items: center;
  position: relative;
`

export const PlayerOptionReport = styled.a`
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  display: flex;
  align-items: center;

  @media ${({ theme }) => theme.device.laptop} {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
  }

  background-color: ${({ theme }) => theme.colors.gray};
  svg {
    color: ${({ theme }) => theme.colors.primary};
  }

  &:hover {
    background-color: ${({ theme }) => theme.colors.primary};
    cursor: pointer;
    svg {
      color: ${({ theme }) => theme.colors.white};
    }
  }
`

export const PlayerOptionTitle = styled.span`
  color: ${({ theme }) => theme.colors.white};
  text-transform: uppercase;
  padding: 10px 20px;
  margin-top: 0.5rem;
  height: 50px;
  position: relative;

  svg {
    position: absolute;
    top: 0.7rem;
  }
`

export const PlayerOptionLink = styled.a`
  width: 100%;
  padding: 10px 20px;
  &:hover {
    background-color: ${({ theme }) => theme.colors.gray};
    border-left: 3px solid ${({ theme }) => theme.colors.primary};
  }
  ${({ active }) =>
    active &&
    css`
      background-color: ${({ theme }) => theme.colors.gray};
      border-left: 3px solid ${({ theme }) => theme.colors.primary};
    `}
`

export const PlayerOptionSubTitle = styled.strong`
  color: ${({ theme }) => theme.colors.white};
  text-transform: uppercase;
  margin-left: 2.3rem;
`

export const PlayerOptionText = styled.p`
  margin-bottom: 0;
  margin-left: 2.3rem;
`

export const PlayerImage = styled.img`
  width: 20px;
  position: absolute;
  top: 1.5rem;
`

export const ModalStyle = styled.div`
  div {
    z-index: 100;
  }
`

export const ModalTitle = styled.h1`
  font-size: 1.5rem;
  text-align: center;
  padding: 0.5rem;
  color: ${({ theme }) => theme.colors.white};
`

export const ModalText = styled.p`
  font-size: 1rem;
  text-align: center;
  color: ${({ theme }) => theme.colors.white};
`

export const ModalOptions = styled.ul`
  margin: 10px 0 2rem;
  padding: 0;
`

export const ModalOption = styled.li`
  margin-bottom: 0.5rem;
`

export const ModalOptionLink = styled.a`
  display: block;
`

export const ModalOptionIcon = styled.div`
  svg {
    position: absolute;
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const ModalClose = styled.div`
  position: relative;
  width: 100%;
  cursor: pointer;
  svg {
    position: absolute;
    color: ${({ theme }) => theme.colors.white};
    right: 0px;
    top: 0px;
  }
`

export const ModalOptionName = styled.span`
  padding-left: 2rem;
`

export const ModalTelegram = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 20px 0px 5px;
`

export const ModalTextArea = styled.textarea`
  margin-top: 0.7rem;
  background-color: ${({ theme }) =>
    addOpacityToColor(theme.colors.white, 0.3)};
`

export const ModalButton = styled.button`
  background-color: ${({ theme }) => theme.colors.primary};
  padding: 10px 20px;
  border: 0;
  color: ${({ theme }) => theme.colors.white};
  text-transform: uppercase;
  font-size: 1rem;
  font-weight: 700;
  cursor: pointer;
  border-radius: 10px;

  &:disabled {
    opacity: 0.5;
  }
`
