import { useState, useEffect } from 'react'
import { FaPlay } from 'react-icons/fa'
import { AiFillAlert } from 'react-icons/ai'

import { PlayerModal } from './Modal'
import { languages, servers, langsName } from 'utils/constans'
import {
  Video,
  VideoFrame,
  PlayerImage,
  PlayerVideo,
  PlayerIFrame,
  PlayerOption,
  PlayerOptions,
  PlayerOptionText,
  PlayerOptionLink,
  PlayerOptionsCont,
  PlayerOptionTitle,
  PlayerVideoOptions,
  PlayerOptionReport,
  PlayerOptionSubTitle
} from './styles'

export function PlayerMovie ({ links, movie }) {
  const [optionActual, changeOption] = useState()
  const [visible, changeVisible] = useState(false)
  const [options, changeOptions] = useState([])

  useEffect(() => {
    if (links.length > 0) {
      let options = links.filter(
        it =>
          !(it.server == '14707' || it.server == '8310' || it.server == '1231')
      )

      options
        .sort((a, b) => a.lang - b.lang)
        .forEach((it, idx) => (it.idx = idx))
      changeOptions(options)
      changeOption(options[0])
    }
  }, [links])
  return (
    <PlayerVideoOptions>
      <PlayerVideo>
        <Video movie>
          <VideoFrame>
            <PlayerIFrame
              src={optionActual?.link}
              allowFullScreen
              frameborder='0'
            />
          </VideoFrame>
        </Video>
      </PlayerVideo>
      <PlayerOptionsCont>
        <PlayerOption>
          <PlayerOptionTitle>
            <FaPlay size={16} />
            <PlayerOptionSubTitle>Opciones</PlayerOptionSubTitle>
          </PlayerOptionTitle>
        </PlayerOption>
        <PlayerOptions>
          {options.map((option, idx) => (
            <PlayerOption key={idx}>
              <PlayerOptionLink
                active={
                  `${option.id}-${option.lang}` ===
                  `${optionActual?.id}-${optionActual?.lang}`
                }
                onClick={() => changeOption(option)}
              >
                <PlayerImage src={`/img/lang/${languages[option.lang]}.svg`} />
                <PlayerOptionSubTitle>opción {idx + 1}</PlayerOptionSubTitle>
                <PlayerOptionText>
                  {servers[option.server]} | {langsName[option.lang]}
                </PlayerOptionText>
              </PlayerOptionLink>
            </PlayerOption>
          ))}
        </PlayerOptions>
        <PlayerOptionReport onClick={() => changeVisible(true)}>
          <PlayerOptionTitle>
            <AiFillAlert size={20} />
            <PlayerOptionSubTitle>Reportar</PlayerOptionSubTitle>
          </PlayerOptionTitle>
        </PlayerOptionReport>
      </PlayerOptionsCont>
      <PlayerModal
        visible={visible}
        changeVisible={changeVisible}
        movie={movie}
        option={optionActual}
      />
    </PlayerVideoOptions>
  )
}
