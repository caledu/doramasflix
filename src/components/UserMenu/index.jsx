import { ModalAuth } from 'components/ModalAuth'
import { UserContext } from 'contexts/UserContext'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useContext, useState } from 'react'
import { FaUserCircle } from 'react-icons/fa'
import {
  UserButton,
  UserButtonMenu,
  UserButtonMenuUL,
  UserButtonMenuLI,
  UserButtonMenuA
} from './styles'

export function UserMenu () {
  const [visible, changeVisible] = useState(false)
  const [visibleModal, changeVisibleModal] = useState(false)
  const [type, changeType] = useState()
  const router = useRouter()

  const { loggedUser, logout } = useContext(UserContext)

  const handleAuth = type => {
    changeType(type)
    changeVisibleModal(true)
    changeVisible(false)
  }

  return (
    <>
      <UserButton
        onClick={() => {
          changeVisible(!visible)
        }}
      >
        <FaUserCircle size={24} />
      </UserButton>
      {visible && (
        <UserButtonMenu visible={visible}>
          {loggedUser ? (
            <UserButtonMenuUL visible={visible}>
              <UserButtonMenuLI name> {loggedUser.names} </UserButtonMenuLI>
              <UserButtonMenuLI>
                <Link
                  href='/perfil/[user]'
                  as={`/perfil/${loggedUser.username}`}
                >
                  <UserButtonMenuA onClick={() => changeVisible(false)}>
                    Perfil
                  </UserButtonMenuA>
                </Link>
              </UserButtonMenuLI>
              <UserButtonMenuLI>
                <Link href='/cuenta'>
                  <UserButtonMenuA>Editar cuenta</UserButtonMenuA>
                </Link>
              </UserButtonMenuLI>
              <UserButtonMenuLI>
                <UserButtonMenuA
                  onClick={() => {
                    logout()
                    changeVisible(false)
                    changeVisibleModal(false)
                    router.reload()
                  }}
                >
                  Cerrar Sesión
                </UserButtonMenuA>
              </UserButtonMenuLI>
            </UserButtonMenuUL>
          ) : (
            <UserButtonMenuUL visible={visible}>
              <UserButtonMenuLI>
                <UserButtonMenuA onClick={() => handleAuth('login')}>
                  Iniciar Sesión
                </UserButtonMenuA>
              </UserButtonMenuLI>
              <UserButtonMenuLI>
                <UserButtonMenuA onClick={() => handleAuth('register')}>
                  Registrarse
                </UserButtonMenuA>
              </UserButtonMenuLI>
            </UserButtonMenuUL>
          )}
        </UserButtonMenu>
      )}
      <ModalAuth
        changeType={changeType}
        type={type}
        visible={visibleModal}
        changeVisible={changeVisibleModal}
      />
    </>
  )
}
