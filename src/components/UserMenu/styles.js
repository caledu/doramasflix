import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const UserButton = styled.a`
  padding: 0.6rem 0.5rem 0.5rem;
  float: right;
  margin-top: 20px;

  svg {
    color: ${({ theme, visible }) =>
      visible ? theme.colors.primary : theme.colors.white};
  }
`

export const UserButtonMenu = styled.div`
  position: absolute;
  top: 62px;
  right: 5px;
  z-index: 99;
  overflow: hidden;
  opacity: 0;
  border-top: 2px solid transparent;
  border-top-color: ${({ theme }) => theme.colors.primary};
  box-shadow: 0 5px 25px
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};
  margin: 0;
  padding: 0;
  display: block;
  list-style-type: none;
  height: auto;

  ${({ visible }) =>
    visible &&
    css`
      opacity: 1;
      padding-bottom: 0rem;
      overflow: auto;
    `}
`

export const UserButtonMenuUL = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`

export const UserButtonMenuLI = styled.li`
  position: relative;
  border-bottom: 1px solid ${({ theme }) => theme.colors.gray};
  line-height: 2.5rem;
  font-size: 0.8rem;
  margin: 0 0rem;

  ${({ name }) =>
    name &&
    css`
      padding: 0 0.7rem;
    `}
`

export const UserButtonMenuA = styled.a`
  width: 100%;
  font-weight: 700;

  color: ${({ theme }) => theme.colors.white};
  padding: 0 1rem;
  display: flex;
  rotate: 90°;
  :hover {
    background-color: ${({ theme }) => theme.colors.bg2};
    border-left: 3px solid ${({ theme }) => theme.colors.primary};
  }
`

export const ItemIconDown = styled.div`
  width: 0;
  height: 0;
  position: relative;

  svg {
    position: absolute;
    right: -25px;
    top: 4px;
    transform: rotate(-90deg);
    color: ${({ theme }) => theme.colors.primary};
  }

  @media ${({ theme }) => theme.device.laptop} {
    display: block;
    right: 1rem;
    svg {
      transform: rotate(0deg);
      right: -40px;
      top: 4px;
    }
  }

  @media ${({ theme }) => theme.device.laptopM} {
    right: 2rem;
    top: 4px;
    svg {
      right: -55px;
      top: 0px;
    }
  }
`
