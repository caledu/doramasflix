import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const InfoContainer = styled.div`
  margin-left: 0;
  min-height: 335px;
  max-width: 570px;
  overflow: visible;

  ${({ card }) =>
    card &&
    css`
      max-width: 100%;
      min-height: auto;
      overflow: hidden;
      a,
      h1 {
        font-size: 0.75rem;
        line-height: 1.2rem;
        white-space: nowrap;
        margin-right: 2px;
        margin-bottom: 3px;
        color: ${({ theme }) => theme.colors.tinyWhite};
      }
      span {
        margin-right: 5px;
        font-size: 0.75rem;
        line-height: 0.9375rem;
      }
      span[class*='styles__VoteNumber'] {
        padding-left: 0.75rem;
      }
      div[class*='styles__Vote'] {
        svg {
          left: -2px;
          top: -2px;
          width: 15px;
          height: 15px;
        }
      }
      a[class*='styles__Label'] {
        display: none;
      }
      p {
        font-size: 0.75rem;
        line-height: 0.9375rem;
        margin-bottom: 7px;
        max-lines: 3;
        a {
          font-size: 0.75rem;
          line-height: 0.9375rem;
        }
      }
      div {
        font-size: 0.75rem;
        line-height: 0.9375rem;
        margin-bottom: 0;
        padding: 0;
      }
    `}
`

export const Title = styled.h2`
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
  font-size: 1.875rem;
  line-height: 2.5rem;
  font-weight: 700;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.4rem;
  margin-bottom: 0;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-width: 670px;
  cursor: pointer;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;

  ${({ card }) =>
    card &&
    css`
      padding-top: 10px;
    `}
`

export const TitleH1 = styled.h1`
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
  line-height: 2.5rem;
  font-weight: 700;
  display: inline-block;
  vertical-align: top;
  margin-right: 0.4rem;
  margin-bottom: 0;
  cursor: pointer;
`

export const SubTitle = styled.h2`
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
  font-size: 1rem;
  margin-bottom: 0;
  line-height: 1.5rem;
  margin-bottom: 5px;
  font-weight: 500;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-width: 670px;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;

  ${({ card }) =>
    card &&
    css`
      display: none;
    `}

  ${({ full }) =>
    full &&
    css`
      -webkit-line-clamp: 2;
    `}
`

export const Details = styled.div`
  display: flex;
  position: relative;
  align-items: flex-end;
  font-size: 0.75rem;
  line-height: 1.25rem;
  padding: 0px 0;
  font-weight: 700;
  margin-bottom: 5px;
`

export const DetailItem = styled.span`
  vertical-align: top;
  display: inline-block;
  margin-right: 0.4rem;
  margin-bottom: 5px;
  line-height: 1.25rem;
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
`

export const Vote = styled.div`
  vertical-align: top;
  margin-right: 0.4rem;
  svg {
    margin-right: 0.4em;
    position: absolute;
    top: -2px;
  }
`

export const VoteNumber = styled(DetailItem)`
  font-size: 12px;
  padding-left: 1.4rem;
`

export const Label = styled.a`
  color: currentColor;
  border: 1px solid currentColor;
  height: 1.25rem;
  line-height: 1.1rem;
  padding: 0 0.5rem;
  border-radius: 30px;
  text-transform: uppercase;
  font-size: 0.625rem;
  font-weight: 700;
  vertical-align: top;
  display: inline-block;
  margin-right: 0.4rem;
  margin-bottom: 5px;
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
`

export const Description = styled.div`
  font-size: 1rem;
  line-height: 1.5625rem;
  margin-bottom: 1rem;
`

export const Overview = styled.p`
  max-height: 25rem;
  margin-bottom: 0.937rem;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: ${({ lines }) => lines};
  -webkit-box-orient: vertical;
`

export const OverviewStrong = styled.strong`
  max-height: 25rem;
`

export const DescriptionItem = styled.p`
  margin-bottom: 0.2rem;
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  max-height: 1.5rem;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`

export const DescriptionItemName = styled.span`
  margin-right: 0.4rem;
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
`

export const DescriptionItemLink = styled.a`
  font-weight: 400;
  text-shadow: 1px 1px 1px ${({ theme }) => theme.colors.black};
  &:hover {
    color: ${({ theme }) => theme.colors.white};
  }
`

export const Button = styled.a`
  font-size: 0.9rem;
  margin-right: 0.5rem;
  box-shadow: inset 0 -10px 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)};
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme, color }) => color || theme.colors.primary};
  border: 0;
  max-width: ${({ maxWidth }) => maxWidth || '165px'};
  cursor: pointer;
  padding: 5px 10px;
  display: flex;
  align-items: center;
  line-height: 1.6rem;
  border-radius: 5px;
  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    margin-right: 0.2rem;
  }
  &:hover {
    transform: scale(1.05);
  }

  ${({ card }) =>
    card &&
    css`
      position: absolute;
      bottom: 10px;
      right: 10%;
      left: 10%;
    `}

  ${({ noUpper }) =>
    !noUpper &&
    css`
      text-transform: uppercase;
    `}
`

export const TextLink = styled.span`
  margin-top: 3px;
`

export const Buttons = styled.div`
  display: flex;
  align-items: center;
`
