import Link from 'next/link'
import { MdStar, MdPlayCircleOutline } from 'react-icons/md'
import toSlug from 'slug'
import { ActionsInfo } from '../ActionsInfo'

import { colors } from 'styles/theme'
import {
  Vote,
  Title,
  Label,
  Button,
  Details,
  TitleH1,
  Buttons,
  SubTitle,
  Overview,
  TextLink,
  VoteNumber,
  DetailItem,
  Description,
  InfoContainer,
  OverviewStrong,
  DescriptionItem,
  DescriptionItemName,
  DescriptionItemLink
} from './styles'

export function Info ({
  _id,
  full,
  name,
  card,
  slug,
  title,
  names,
  notList = false,
  poster_path,
  __typename,
  cast = [],
  genres = [],
  runtime = 0,
  networks = [],
  overview = '',
  release_date = '',
  first_air_date = '',
  episode_run_time = []
}) {
  const year = first_air_date
    ? first_air_date.split('-')[0]
    : release_date.split('-')[0]
  const time = runtime ? runtime : episode_run_time.join('-')
  cast = cast.slice(0, 4)
  const type = runtime > 0 ? 'Pelicula' : 'Dorama'
  const url = runtime > 0 ? '/peliculas/' : '/doramas/'
  const lines = full ? 0 : card ? 4 : 3

  genres = card ? genres.slice(0, 2) : genres
  networks = card ? networks.slice(0, 1) : networks

  return (
    <InfoContainer card={card}>
      {title ? <TitleH1>{name}</TitleH1> : <Title card={card}>{name}</Title>}
      <SubTitle card={card} full={full}>
        {names}
      </SubTitle>
      <Details>
        <Vote>
          <MdStar size={20} color={colors.primary} />
          <VoteNumber>4.5</VoteNumber>
        </Vote>
        <DetailItem>{year}</DetailItem>
        <Label>{type}</Label>
        <DetailItem>{time} min</DetailItem>
      </Details>
      <Description>
        <Overview lines={lines}>
          {!card && <OverviewStrong>Ver {name} online: </OverviewStrong>}
          {overview}
        </Overview>
        {genres.length > 0 && (
          <DescriptionItem>
            <DescriptionItemName>{card ? 'G' : 'Generos'}:</DescriptionItemName>
            {genres.map(({ name, slug }, idx) => (
              <Link href='/generos/[slug]' as={`/generos/${slug}`} key={idx}>
                <DescriptionItemLink>
                  {name}
                  {idx !== genres.length - 1 && ', '}
                </DescriptionItemLink>
              </Link>
            ))}
          </DescriptionItem>
        )}
        {networks.length > 0 && (
          <DescriptionItem>
            <DescriptionItemName>
              {card ? 'P' : 'Productoras'}:
            </DescriptionItemName>
            {networks.map(({ name, slug }, idx) => (
              <Link
                href='/productoras/[slug]'
                as={`/productoras/${slug}`}
                key={idx}
              >
                <DescriptionItemLink>
                  {name}
                  {idx !== networks.length - 1 && ', '}
                </DescriptionItemLink>
              </Link>
            ))}
          </DescriptionItem>
        )}
        {!card && cast.length > 0 && (
          <DescriptionItem>
            <DescriptionItemName>Actores:</DescriptionItemName>
            {cast.map(({ name, slug, id }, idx) => (
              <Link
                href='/reparto/[slug]'
                as={slug ? `/reparto/${slug}` : `${id}-${toSlug(name)}`}
                key={idx}
              >
                <DescriptionItemLink>
                  {name}
                  {idx !== cast.length - 1 && ', '}
                </DescriptionItemLink>
              </Link>
            ))}
          </DescriptionItem>
        )}
      </Description>
      {
        <Buttons>
          {!full && (
            <Link href={url + '[slug]'} as={url + slug}>
              <Button card={card}>
                <MdPlayCircleOutline size={20} color={colors.white} />
                <TextLink>Ver ahora</TextLink>
              </Button>
            </Link>
          )}
          {!card && (
            <ActionsInfo
              _id={_id}
              notList={notList}
              __typename={__typename}
              poster_path={poster_path}
              name={name}
              slug={slug}
            />
          )}
        </Buttons>
      }
    </InfoContainer>
  )
}
