import React from 'react'
import ContentLoader from 'react-content-loader'

import { colors } from 'styles/theme'

export function CardsLoader ({ cols = 5, rows = 1, ...props }) {
  const coverHeight = 165
  const coverWidth = 110
  const padding = 5
  const speed = 2

  const coverHeightWithPadding = coverHeight + padding
  const coverWidthWithPadding = coverWidth + padding
  const initial = 0
  const covers = Array(cols * rows).fill(1)

  return (
    <ContentLoader
      speed={speed}
      width={cols * coverWidthWithPadding}
      height={rows * coverHeightWithPadding}
      backgroundColor={colors.backg}
      foregroundColor={colors.foreng}
      {...props}
    >
      {covers.map((g, i) => {
        let vy = Math.floor(i / cols) * coverHeightWithPadding + initial
        let vx = (i * coverWidthWithPadding) % (cols * coverWidthWithPadding)
        return (
          <rect
            key={i}
            x={vx}
            y={vy}
            rx='0'
            ry='0'
            width={coverWidth}
            height={coverHeight}
          />
        )
      })}
    </ContentLoader>
  )
}
