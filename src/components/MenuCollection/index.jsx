import Link from 'next/link'
import React from 'react'
import slug from 'slug'
import { collection } from 'utils/constans'
import { Menu, MenuItemA, MenuItem } from './styles'

export function MenuCollection ({ username, status }) {
  return (
    <Menu>
      <MenuItem>
        <Link
          href='/coleccion/[user]/[status]'
          as={`/coleccion/${username}/favoritos`}
        >
          <MenuItemA active={status === 'favoritos'}>Favoritos</MenuItemA>
        </Link>
      </MenuItem>
      {collection.map(it => (
        <MenuItem key={it}>
          <Link
            href='/coleccion/[user]/[status]'
            as={`/coleccion/${username}/${slug(it, { lower: true })}`}
          >
            <MenuItemA active={status === slug(it, { lower: true })}>
              {it}
            </MenuItemA>
          </Link>
        </MenuItem>
      ))}
    </Menu>
  )
}
