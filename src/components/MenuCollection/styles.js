import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const Menu = styled.ul`
  display: flex;
  padding: 0;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  list-style: none;
  list-style-type: none;
  box-shadow: 0 5px 25px
    ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg2};
  border-radius: 20px;
`

export const MenuItem = styled.li`
  padding: 5px 10px;
  margin: 5px 5px;
`

export const MenuItemA = styled.a`
  width: 100%;
  font-weight: bold;
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};

  :hover {
    border-bottom: 3px solid ${({ theme }) => theme.colors.primary};
    color: ${({ theme }) => theme.colors.primary};
  }

  ${({ active }) =>
    active &&
    css`
      border-bottom: 3px solid ${({ theme }) => theme.colors.primary};
      color: ${({ theme }) => theme.colors.primary};
    `}
`

export const MenuItemBack = styled.a`
  width: 100%;
  font-weight: bold;
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};
  display: flex;
  align-items: center;

  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`
