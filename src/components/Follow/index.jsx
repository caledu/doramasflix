import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Iframe = dynamic(() => import('./Iframe'))

export function Follow () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '0px'
  })

  return (
    <div style={{ minHeight: 120 }} ref={fromRef}>
      {isNearScreen && <Iframe />}
    </div>
  )
}
