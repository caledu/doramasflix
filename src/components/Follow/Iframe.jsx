import { Iframe } from './styles'
import { Widget } from '../Widget'
import { Button } from 'components/Info/styles'
import { Center } from 'components/ModalAuth/styles'
import { FaInstagram, FaTelegramPlane } from 'react-icons/fa'
import { Space } from 'components/Space'

export default function IFrameFollow () {
  return (
    <Widget title='Síguenos'>
      <Center>
        <Iframe
          src={
            'https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fweb.facebook.com%2Fdoramasflix&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=696236261014036'
          }
          scrolling='no'
          frameborder='0'
          allowtransparency='true'
          allow='encrypted-media'
          width='350px'
          height='100px'
        ></Iframe>
        <Button
          noUpper
          maxWidth='200px'
          href='https://instagram.com/doramasflix.co'
          target='_blank'
        >
          <FaInstagram size={16} />
          @doramasflix.co
        </Button>
        <Space height='10px' />
        <Button
          noUpper
          color='#0088cc'
          maxWidth='200px'
          href='https://t.me/doramasflixgo'
          target='_blank'
        >
          <FaTelegramPlane size={16} />
          @doramasflixgo
        </Button>
      </Center>
    </Widget>
  )
}
