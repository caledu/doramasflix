import Modal from 'react-modal'
import { colors } from 'styles/theme'
import { MdClose } from 'react-icons/md'
import FacebookLogin from 'react-facebook-login'
import {
  ModalStyle,
  ModalTitle,
  ModalClose,
  ModalHeader,
  ModalBody,
  FormMore,
  FormMoreLink,
  ModalContainer
} from './styles'
import { RegisterForm } from './RegisterForm'
import { LoginForm } from './LoginForm'
import { useLogin, useRequestPassword, useRegister } from 'hooks'
import { useRouter } from 'next/router'
import { RequestForm } from './RequestForm'

export function ModalAuth ({ visible, changeVisible, changeType, type }) {
  const customStyles = {
    overlay: {
      backgroundColor: 'rgba(0,0,0,.8)',
      zIndex: 99,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    content: {
      zIndex: 100,
      maxHeight: '80%',
      maxWidth: 420,
      minWidth: '20%',
      position: 'relative',
      background: colors.bg,
      color: colors.white,
      border: 0,
      top: 0,
      left: 0,
      right: 0
    }
  }
  const router = useRouter()
  const title =
    type === 'request'
      ? 'Solicitar Contraseña'
      : type === 'login'
      ? 'Inicio de sesión'
      : 'Registro'

  const handleSuccess = () => {
    changeVisible(false)
    router.reload()
  }

  const { handleLogin, ldLogin, errLogin } = useLogin({
    onSuccess: handleSuccess
  })
  const { registerUser, ldRegister, errRegister } = useRegister({
    onSuccess: handleSuccess
  })
  const { handleRequest, ldRequest, errRequest } = useRequestPassword({
    onSuccess: () => changeType('login')
  })

  const responseFacebook = ({ email, id, name }) => {
    registerUser({
      email,
      names: name,
      facebookId: id,
      register_platform: 'doramasflix'
    })
  }

  return (
    <ModalStyle>
      <Modal
        isOpen={visible}
        onRequestClose={() => changeVisible(false)}
        style={customStyles}
        contentLabel={title}
      >
        <ModalContainer>
          <ModalHeader>
            <ModalClose onClick={() => changeVisible(false)}>
              <MdClose size={25} />
            </ModalClose>
            <ModalTitle>{title}</ModalTitle>
          </ModalHeader>
          <ModalBody>
            {type === 'request' ? (
              <RequestForm
                onSubmit={handleRequest}
                loading={ldRequest}
                error={errRequest}
              />
            ) : type === 'login' ? (
              <LoginForm
                onSubmit={handleLogin}
                loading={ldLogin}
                error={errLogin}
              />
            ) : (
              <RegisterForm
                onSubmit={registerUser}
                loading={ldRegister}
                error={errRegister}
              />
            )}
            {type !== 'request' && (
              <FacebookLogin
                appId='1814232468732055'
                autoLoad={false}
                fields='name,email'
                icon='fa-facebook'
                size='medium'
                textButton='Continuar con facebook'
                callback={responseFacebook}
                isDisabled={ldRegister}
              />
            )}
            {type === 'request' ? (
              <FormMore>
                ¿No tienes cuenta?{' '}
                <FormMoreLink onClick={() => changeType('register')}>
                  Registrate
                </FormMoreLink>
              </FormMore>
            ) : type === 'login' ? (
              <>
                <FormMore>
                  ¿Olvidaste tu contraseña?{' '}
                  <FormMoreLink onClick={() => changeType('request')}>
                    Solicitar
                  </FormMoreLink>
                </FormMore>
                <FormMore>
                  ¿No tienes cuenta?{' '}
                  <FormMoreLink onClick={() => changeType('register')}>
                    Registrate
                  </FormMoreLink>
                </FormMore>
                <FormMore>
                  Recuerda que tienes acceso con tu cuenta de SeriesGO! o
                  DoramasGO!
                </FormMore>
              </>
            ) : (
              <>
                <FormMore>
                  ¿Ya tienes cuenta?{' '}
                  <FormMoreLink onClick={() => changeType('login')}>
                    Inicia sesión
                  </FormMoreLink>
                </FormMore>
                <FormMore>
                  Recuerda que tienes acceso con tu cuenta de SeriesGO! o
                  DoramasGO!
                </FormMore>
              </>
            )}
          </ModalBody>
        </ModalContainer>
      </Modal>
    </ModalStyle>
  )
}
