import React, { useState } from 'react'
import { AuthErrors } from './AuthErrors'
import {
  FormContainer,
  FormItem,
  FormLabel,
  FormInput,
  FormButtom,
  FormMore
} from './styles'

export function RequestForm ({ onSubmit, loading, error }) {
  const [data, changeData] = useState({
    email: ''
  })

  const handleChange = e => {
    const { name, value } = e.target
    data[name] = value
    changeData({ ...data })
  }

  const handleSubmit = () => {
    onSubmit && onSubmit(data)
  }

  return (
    <FormContainer>
      <FormItem>
        <FormLabel>Email:</FormLabel>
        <FormInput
          name='email'
          placeholder=''
          type='email'
          value={data.email}
          onChange={handleChange}
        />
      </FormItem>

      {!!error && (
        <FormItem>
          <AuthErrors error={error} />
        </FormItem>
      )}
      <FormItem>
        <FormButtom loading={loading} onClick={handleSubmit}>
          Solicitar Contraseña
        </FormButtom>
      </FormItem>
      <FormItem>
        <FormMore>
          Se te enviara un email con una contraseña temporal que puedes cambiar
          luego desde tu perfil.
        </FormMore>
      </FormItem>
    </FormContainer>
  )
}
