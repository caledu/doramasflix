import React from 'react'
import { FormError, FormErrorItem } from './styles'

export function AuthErrors ({ error, message }) {
  if (message) {
    return (
      <FormError>
        <FormErrorItem>{message}</FormErrorItem>
      </FormError>
    )
  }

  if (!error || !error.graphQLErrors) {
    return null
  }

  return (
    <FormError>
      {error.graphQLErrors.map(({ message }, idx) => (
        <FormErrorItem key={idx}>{message}</FormErrorItem>
      ))}
    </FormError>
  )
}
