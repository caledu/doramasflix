import React, { useState } from 'react'
import { AuthErrors } from './AuthErrors'
import {
  FormContainer,
  FormItem,
  FormLabel,
  FormInput,
  FormButtom,
  ModalTitle,
  Center
} from './styles'

export function EditUserForm ({ user, onSubmit, loading, error }) {
  const [data, changeData] = useState({
    names: user?.names,
    email: user?.email,
    username: user?.username,
    password: ''
  })

  const disable = !data.names || !data.username || !data.email

  const handleChange = e => {
    const { name, value } = e.target
    data[name] = value
    changeData({ ...data })
  }

  const handleSubmit = () => {
    !loading && onSubmit(data)
  }

  return (
    <Center>
      <ModalTitle>Editar Usuario</ModalTitle>
      <FormContainer>
        <FormItem>
          <FormLabel>Nombres:</FormLabel>
          <FormInput
            name='names'
            placeholder=''
            value={data.names}
            onChange={handleChange}
            type='text'
          />
        </FormItem>
        <FormItem>
          <FormLabel>Email:</FormLabel>
          <FormInput
            name='email'
            placeholder=''
            type='email'
            value={data.email}
            onChange={handleChange}
          />
        </FormItem>
        <FormItem>
          <FormLabel>Username:</FormLabel>
          <FormInput
            name='username'
            placeholder=''
            type='text'
            value={data.username}
            onChange={handleChange}
          />
        </FormItem>
        <FormItem>
          <FormLabel>Contraseña:</FormLabel>
          <FormInput
            name='password'
            placeholder=''
            type='password'
            value={data.password}
            onChange={handleChange}
          />
        </FormItem>
        {!!error && <AuthErrors error={error} />}
        <FormItem>
          <FormButtom
            onClick={() => !disable && handleSubmit()}
            loading={loading}
            disabled={disable}
          >
            Editar
          </FormButtom>
        </FormItem>
      </FormContainer>
    </Center>
  )
}
