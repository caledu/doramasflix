import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const ModalStyle = styled.div`
  div {
    z-index: 100;
  }
`

export const ModalContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  max-width: 420px;
  margin-right: 0;
`

export const ModalTitle = styled.h1`
  font-size: 1.2rem;
  text-align: center;
  color: ${({ theme }) => theme.colors.white};
  padding: 0 2.2rem;
`

export const ModalHeader = styled.div`
  width: 100%;
  max-width: 420px;
  margin: 0 0;
  border-bottom: 2px solid transparent;
  border-bottom-color: ${({ theme }) => theme.colors.primary};
`

export const ModalBody = styled.div`
  max-width: 420px;
  padding: 5px 10px;
`

export const ModalClose = styled.div`
  position: relative;
  width: 100%;
  cursor: pointer;
  svg {
    position: absolute;
    color: ${({ theme }) => theme.colors.white};
    right: 0px;
    top: 0px;
  }
`

// Aqui para el login y registrp

export const FormContainer = styled.div`
  padding: 10px 0 0 0;
  width: 100%;
  max-width: 420px;
`

export const FormItem = styled.div`
  padding: 5px 0;
  width: 100%;
  ${({ center }) =>
    center &&
    css`
      display: flex;
      justify-content: center;
      align-items: center;
    `}
`

export const FormLabel = styled.span`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.white};
  font-weight: bold;
`

export const FormMore = styled.p`
  width: 100%;
  font-size: 0.9rem;
  margin-bottom: 0.8rem;
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
  margin-top: 0.8rem;
  line-height: 1rem;
`

export const FormMoreLink = styled.a`
  font-size: 0.9rem;
  color: ${({ theme }) => theme.colors.primary};
  opacity: 1;
`

export const FormInput = styled.input`
  background-color: ${({ theme }) => theme.colors.bg2};
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};
  font-weight: bold;
`

export const FormButtom = styled.a`
  width: 100%;
  text-transform: uppercase;
  font-size: 1rem;
  margin-right: 0.5rem;
  margin-bottom: 1rem;
  box-shadow: inset 0 -10px 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)};
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
  border: 0;
  cursor: pointer;
  padding: 5px 1rem;
  display: flex;
  align-items: center;
  line-height: 1.875rem;
  border-radius: 5px;
  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    transform: scale(1.05);
  }

  ${({ loading }) =>
    loading &&
    css`
      opacity: 0.7;
      cursor: none;
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      opacity: 0.7;
      cursor: not-allowed;
      text-decoration: none;
    `}
`

export const FormError = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const FormErrorItem = styled.p`
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.primary};
  font-weight: bold;
`

export const Center = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
