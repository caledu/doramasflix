import React, { useState } from 'react'
import { AuthErrors } from './AuthErrors'
import {
  FormContainer,
  FormItem,
  FormLabel,
  FormInput,
  FormButtom,
  FormMore
} from './styles'

export function RegisterForm ({ onSubmit, loading, error }) {
  const [data, changeData] = useState({
    names: '',
    email: '',
    password: '',
    register_platform: 'doramasflix'
  })

  const handleChange = e => {
    const { name, value } = e.target
    data[name] = value
    changeData({ ...data })
  }

  const handleSubmit = () => {
    !loading && onSubmit(data)
  }

  const disable = !data.password || !data.email || !data.names

  return (
    <FormContainer>
      <FormItem>
        <FormLabel>Nombres:</FormLabel>
        <FormInput
          name='names'
          placeholder=''
          value={data.names}
          onChange={handleChange}
          type='text'
        />
      </FormItem>
      <FormItem>
        <FormLabel>Email:</FormLabel>
        <FormInput
          name='email'
          placeholder=''
          type='email'
          value={data.email}
          onChange={handleChange}
        />
      </FormItem>
      <FormItem>
        <FormLabel>Contraseña:</FormLabel>
        <FormInput
          name='password'
          placeholder=''
          type='password'
          value={data.password}
          onChange={handleChange}
        />
      </FormItem>
      {!!error && <AuthErrors error={error} />}
      <FormItem>
        <FormButtom
          disabled={disable}
          onClick={() => !disable && handleSubmit()}
          loading={loading}
        >
          Registrarse
        </FormButtom>
      </FormItem>
    </FormContainer>
  )
}
