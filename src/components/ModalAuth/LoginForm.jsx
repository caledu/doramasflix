import React, { useState } from 'react'
import { AuthErrors } from './AuthErrors'
import {
  FormContainer,
  FormItem,
  FormLabel,
  FormInput,
  FormButtom
} from './styles'

export function LoginForm ({ onSubmit, loading, error }) {
  const [data, changeData] = useState({
    email: '',
    password: '',
    platform: 'doramasflix'
  })

  const handleChange = e => {
    const { name, value } = e.target
    data[name] = value
    changeData({ ...data })
  }

  const handleSubmit = () => {
    onSubmit && onSubmit(data)
  }

  const disable = !data.password || !data.email

  return (
    <FormContainer>
      <FormItem>
        <FormLabel>Email:</FormLabel>
        <FormInput
          name='email'
          placeholder=''
          type='email'
          value={data.email}
          onChange={handleChange}
        />
      </FormItem>
      <FormItem>
        <FormLabel>Contraseña:</FormLabel>
        <FormInput
          name='password'
          placeholder=''
          type='password'
          value={data.password}
          onChange={handleChange}
        />
      </FormItem>
      {!!error && (
        <FormItem>
          <AuthErrors error={error} />
        </FormItem>
      )}
      <FormItem>
        <FormButtom
          disabled={disable}
          loading={loading}
          onClick={() => !disable && handleSubmit()}
        >
          Iniciar Sesión
        </FormButtom>
      </FormItem>
    </FormContainer>
  )
}
