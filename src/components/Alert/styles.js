import styled from 'styled-components'

export const View = styled.div`
  margin: 1rem 1.5rem;
  padding: 0.5rem 1rem;
  background-color: ${({ theme }) => theme.colors.greenBg};
`

export const Text = styled.p`
  text-align: center;
  margin-bottom: 0.5rem;
  color: ${({ theme, title }) =>
    title ? theme.colors.green1 : theme.colors.green2};
  font-size: ${({ title }) => (title ? '1.3rem' : '1rem')};
`
