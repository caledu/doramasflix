import React from 'react'
import { View, Text } from './styles'

export function Alert ({ title, message }) {
  return (
    <View>
      <Text title>{title}</Text>
      <Text>{message}</Text>
    </View>
  )
}
