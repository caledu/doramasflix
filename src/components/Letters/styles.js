import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const List = styled.ul`
  overflow: auto;
  white-space: nowrap;
  font-size: 0;
  margin: 0 -5px 1.5rem;
  text-align: center;
  padding: 0;
  list-style-type: none;

  @media ${({ theme }) => theme.device.tablet} {
    white-space: normal;
  }
`

export const Item = styled.li`
  display: inline-block;
  vertical-align: top;
  position: relative;
  padding: 0 5px 10px;

  ${({ active }) =>
    active &&
    css`
      a {
        background-color: ${({ theme }) => theme.colors.primary};
      }
    `}

  @media ${({ theme }) => theme.device.laptop} {
    width: 3.7%;
  }
`

export const ItemLink = styled.a`
  color: ${({ theme }) => theme.colors.white};
  display: block;
  line-height: 30px;
  border-radius: 3px;
  font-size: 0.875rem;
  font-weight: 700;
  text-transform: uppercase;
  min-width: 28px;
  background-color: ${({ theme }) => theme.colors.gray};

  &:hover {
    background-color: ${({ theme }) =>
      addOpacityToColor(theme.colors.primary, 0.8)};
  }
`
