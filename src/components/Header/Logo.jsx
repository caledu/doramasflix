import Link from 'next/link'

import { Figure, Image } from './styles'

export function Logo () {
  return (
    <Figure>
      <Link href='/'>
        <Image width='170' height='40' src='/img/logo.png' />
      </Link>
    </Figure>
  )
}
