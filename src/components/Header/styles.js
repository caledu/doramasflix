import styled from 'styled-components'

/* HEADER */
export const HeaderContainer = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 10;

  &::after {
    background: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 1) 0%,
      rgba(0, 0, 0, 0) 100%
    );
    bottom: auto;
    height: 100px;
    opacity: 1;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    content: '';
    z-index: -1;
  }
`

export const Top = styled.div`
  position: relative;
`

/* LOGO */
export const Figure = styled.figure`
  padding: 28px 60px;
  text-align: center;
  transition: 0.2s;
  @media ${({ theme }) => theme.device.laptop} {
    float: left;
    padding-left: 0;
    padding-right: 0;
  }
`

export const Image = styled.img`
  min-height: 1px;
  vertical-align: top;
  border: 0;
  max-width: 80%;
  height: auto;
  display: inline-block;
  cursor: pointer;
`

export const UserButton = styled.a`
  padding: 0.6rem 0.5rem 0.5rem;
  float: right;
  margin-top: 20px;

  svg {
    color: ${({ theme, visible }) =>
      visible ? theme.colors.primary : theme.colors.white};
  }
`
