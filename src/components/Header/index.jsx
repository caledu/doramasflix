import { useState } from 'react'

import { Logo } from './Logo'
import { Menu } from '../Menu'
import { Search } from '../Search'
import { Container } from 'styles/base'
import { HeaderContainer, Top } from './styles'
import { UserMenu } from '../UserMenu'

export function Header () {
  const [visibeInput, changeVisible] = useState(false)
  const toggleVisible = () => changeVisible(!visibeInput)

  return (
    <HeaderContainer>
      <Container>
        <Top>
          {!visibeInput && <Menu />}
          <div>
            <UserMenu />
            <Search visible={visibeInput} toggle={toggleVisible} />
          </div>
          {!visibeInput && <Logo />}
        </Top>
      </Container>
    </HeaderContainer>
  )
}
