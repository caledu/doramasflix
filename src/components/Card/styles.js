import styled, { css } from 'styled-components'
import { addOpacityToColor } from 'styles/utils'

export const CardLink = styled.a``

export const CardContainer = styled.div`
  position: relative;
  border-radius: 3px;
  margin-bottom: 0;
  overflow: visible;
  width: ${({ width }) => width || 'auto'};
  height: ${({ height }) => height || '100%'};
  min-height: 40px;
  background-color: ${({ theme }) => theme.colors.bg2};

  &:hover {
    cursor: pointer;
    img {
      opacity: 0.7;
    }
    div[class*='styles__IconPlay-'] {
      transform: scale(1);
      transition: 0.2s;
    }

    span[class*='styles__CardLabels'] {
      transition: 0.5s;
      display: none;
    }

    span[class*='styles__CardExtraLabels'] {
      transition: 0.5s;
      display: none;
    }

    h2 {
      font-size: 0.75rem;
      line-height: 1rem;
      text-align: center;
      color: ${({ theme }) => theme.colors.white};
      margin-bottom: 0.5rem;
    }

    @media ${({ theme }) => theme.device.laptop} {
      aside {
        opacity: 1;
        visibility: visible;
        transform: scale(1);
      }
    }
  }
`

export const CardHover = styled.aside`
  visibility: hidden;
  overflow: visible;
  opacity: 0;
  position: absolute;
  left: -1rem;
  top: -1rem;
  right: -1rem;
  transition: 0.2s;
  opacity: 0;
  transform: scale(0);
  border-radius: 6px;
  padding: 1rem;
  min-height: calc(100% + 1rem);
  z-index: 4;
  color: ${({ theme }) => theme.colors.text};
  box-shadow: inset 0 0 70px
      ${({ theme }) => addOpacityToColor(theme.colors.black, 0.3)},
    0 0 20px ${({ theme }) => addOpacityToColor(theme.colors.black, 0.5)};
  background-color: ${({ theme }) => theme.colors.bg};
`

export const CardContainerImage = styled.div`
  position: relative;
  height: 100%;
`

export const CardFigure = styled.figure`
  position: relative;
  border-radius: 3px;
  overflow: hidden;
  height: 100%;

  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0%;
    right: 0;
    height: 100%;
    border-radius: 3px;
    background: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0) 50%,
      rgba(0, 0, 0, 1) 100%
    );
  }
`

export const IconPlay = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  margin: auto;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  z-index: 3;
  display: flex;
  justify-content: center;
  align-items: center;

  transform: scale(0);
  transition: 0.2s;
  background-color: ${({ theme }) =>
    addOpacityToColor(theme.colors.black, 0.7)};
  box-shadow: inset 0 0 0 2px ${({ theme }) => theme.colors.white};
`

export const CardImage = styled.img`
  opacity: 1;
  width: 100%;
  height: 100%;
  ${({ imageCover }) =>
    imageCover &&
    css`
      object-fit: cover;
    `}
`

export const CardLabels = styled.span`
  position: absolute;
  top: 5px;
  left: 5px;
  display: inline-block;
  vertical-align: top;
`

export const CardExtraLabels = styled.span`
  position: absolute;
  top: 5px;
  right: 5px;
  display: inline-block;
  vertical-align: top;
`

export const Label = styled.span`
  position: static;
  margin-bottom: 3px;
  font-weight: 700;
  border: 0;
  float: left;
  clear: both;
  margin-right: 0;
  height: auto;
  padding: 0 0.5rem;
  border-radius: 30px;
  text-transform: uppercase;
  font-size: 0.6rem;
  line-height: 1rem;
`

export const CardLabel = styled(Label)`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.colors.primary};
`

export const CardYear = styled(Label)`
  color: ${({ theme }) => theme.colors.tinyBlack};
  background-color: ${({ theme }) =>
    addOpacityToColor(theme.colors.white, 0.7)};
`

export const CardLangs = styled(Label)`
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) =>
    addOpacityToColor(theme.colors.black, 0.7)};
`

export const CardFlag = styled.img`
  width: 22px;
  height: 22px;
  margin-right: 2px;
`

export const CardTitle = styled.h2`
  max-width: 100%;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0.5rem;
  padding: 0 0.5rem;
  border-radius: 0 0 10px 10px;
  font-size: 0.75rem;
  line-height: 1rem;
  color: ${({ theme }) => theme.colors.white};
  font-weight: 700;
  margin: 0;
  display: inline-block;
  vertical-align: top;
`
