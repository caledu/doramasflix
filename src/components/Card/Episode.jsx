import Img from 'react-cool-img'
import Link from 'next/link'
import { MdPlayArrow } from 'react-icons/md'

import { colors } from 'styles/theme'
import {
  CardLink,
  IconPlay,
  CardYear,
  CardLabel,
  CardHover,
  CardTitle,
  CardLabels,
  CardFigure,
  CardContainer,
  CardExtraLabels,
  CardContainerImage
} from './styles'

export function CardEpisode ({
  as,
  image,
  label,
  title,
  width,
  state,
  height,
  srcSet,
  children,
  href = '/'
}) {
  return (
    <CardContainer width={width} height={height}>
      <Link href={href} as={as || href}>
        <CardLink>
          <CardContainerImage>
            <IconPlay>
              <MdPlayArrow size={25} color={colors.primary} />
            </IconPlay>
            {image && (
              <CardFigure>
                <Img
                  style={{
                    backgroundColor: colors.backg,
                    width: '100%',
                    minHeight: '50px'
                  }}
                  src={image}
                  srcSet={srcSet && srcSet}
                />
              </CardFigure>
            )}
            <CardLabels>{label && <CardLabel>{label}</CardLabel>}</CardLabels>
            <CardExtraLabels>
              {state && <CardYear>{state}</CardYear>}
            </CardExtraLabels>
          </CardContainerImage>
        </CardLink>
      </Link>
      {title && <CardTitle>{title}</CardTitle>}
      {children && <CardHover>{children}</CardHover>}
    </CardContainer>
  )
}
