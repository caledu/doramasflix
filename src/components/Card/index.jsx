import Img from 'react-cool-img'
import Link from 'next/link'
import { MdPlayArrow } from 'react-icons/md'

import { colors } from 'styles/theme'
import { languages } from 'utils/constans'
import {
  CardFlag,
  IconPlay,
  CardYear,
  CardLabel,
  CardHover,
  CardTitle,
  CardLangs,
  CardLabels,
  CardFigure,
  CardContainer,
  CardExtraLabels,
  CardContainerImage
} from './styles'

export function Card ({
  as,
  year,
  image,
  label,
  title,
  width,
  state,
  height,
  srcSet,
  children,
  href = '/',
  langs = []
}) {
  return (
    <CardContainer width={width} height={height}>
      <Link href={href} as={as || href}>
        <a>
          <CardContainerImage>
            <IconPlay>
              <MdPlayArrow size={20} color={colors.primary} />
            </IconPlay>
            {image && (
              <CardFigure>
                <Img
                  style={{
                    backgroundColor: colors.backg,
                    width: '100%',
                    height: 'auto'
                  }}
                  src={image}
                  srcSet={srcSet && srcSet}
                />
              </CardFigure>
            )}
            <CardLabels>
              {label && <CardLabel>{label}</CardLabel>}
              {year && <CardYear>{year}</CardYear>}
              {langs && langs.length > 0 && (
                <CardLangs>
                  {langs.map(
                    l =>
                      languages[l] && (
                        <CardFlag
                          key={l}
                          src={`/img/lang/${languages[l]}.svg`}
                        />
                      )
                  )}
                </CardLangs>
              )}
            </CardLabels>
            <CardExtraLabels>
              {state && <CardYear>{state}</CardYear>}
            </CardExtraLabels>
          </CardContainerImage>
        </a>
      </Link>
      {title && <CardTitle>{title}</CardTitle>}
      {children && <CardHover>{children}</CardHover>}
    </CardContainer>
  )
}
