import styled from 'styled-components'
import { addOpacityToColor } from 'styles/utils'
import IMG from 'react-cool-img'

export const ImageBackgroundContainer = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: 0;
  z-index: -1;
  overflow: hidden;

  &::before {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    border-radius: 0;
    content: '';
    background-color: ${({ theme }) =>
      addOpacityToColor(theme.colors.black, 0.2)};
    z-index: 2;
  }
  &::after {
    background: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0) 0%,
      ${({ theme }) => theme.colors.bg} 100%
    );
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    border-radius: 0;
    z-index: 3;
    content: '';
  }
`

export const ImageFigure = styled.figure`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: 0;
  z-index: 1;
  padding-top: 0;
  overflow: hidden;
`

export const Image = styled(IMG)`
  opacity: 1;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  border-radius: 0;
  overflow: hidden;
  transition: opacity 400ms ease;
  object-fit: cover;
  object-position: top;
  max-width: 100%;
  transform-style: preserve-3d;
  vertical-align: top;
  pointer-events: none;
`
