import { SeenEpisode } from 'components/ActionsInfo/SeenEpisode'
import { CardEpisode } from 'components/Card/Episode'
import Link from 'next/link'
import { FaPlay } from 'react-icons/fa'
import { URL_IMAGE_EPISODE_1X } from 'utils/urls'
import {
  ListEpisodes,
  ListItem,
  ItemNumber,
  ItemInfo,
  ItemName,
  ItemDate,
  ImageItem,
  PlayItem,
  LeftItem,
  RightItem
} from './episodes.sty'

export function ListEpisodesDorama ({ list, listSeen }) {
  return (
    <ListEpisodes>
      {list.map((ep, i) => (
        <ListItem key={i}>
          <LeftItem>
            <ItemNumber>{i + 1}</ItemNumber>
            <ImageItem>
              <CardEpisode
                href={'/capitulos/[slug]'}
                as={`/capitulos/${ep.slug}`}
                image={URL_IMAGE_EPISODE_1X + ep.still_path}
              />
            </ImageItem>
            <ItemInfo>
              <Link href={'/capitulos/[slug]'} as={`/capitulos/${ep.slug}`}>
                <ItemName>{`${
                  ep.season_number > 1 ? `Temporada ${ep.season_number}` : ''
                } Episodio ${ep.episode_number}`}</ItemName>
              </Link>
              <ItemDate>{ep.air_date && ep.air_date.split('T')[0]}</ItemDate>
            </ItemInfo>
          </LeftItem>
          <RightItem>
            <SeenEpisode _id={ep._id} listSeen={listSeen} />
            <PlayItem>
              <Link href={'/capitulos/[slug]'} as={`/capitulos/${ep.slug}`}>
                <FaPlay size={13} />
              </Link>
            </PlayItem>
          </RightItem>
        </ListItem>
      ))}
    </ListEpisodes>
  )
}
