import { CardEpisode } from 'components/Card/Episode'
import { URL_IMAGE_EPISODE_1X } from 'utils/urls'
import { ListContainer, ItemEpisode } from './styles'

export function ListEpisodes ({ list }) {
  return (
    <ListContainer>
      {list.map((ep, i) => (
        <ItemEpisode key={i}>
          <CardEpisode
            href={'/capitulos/[slug]'}
            as={`/capitulos/${ep.slug}`}
            title={`${ep.serie_name} ${
              ep.season_number > 1 ? `Temporada ${ep.season_number}` : ''
            } Episodio ${ep.episode_number}`}
            image={URL_IMAGE_EPISODE_1X + ep.still_path}
            label={ep.air_date && ep.air_date.split('T')[0]}
          />
        </ItemEpisode>
      ))}
    </ListContainer>
  )
}
