import styled from 'styled-components'

export const ListEpisodes = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin: 0;
  padding: 5px;
  list-style-type: none;
  margin-bottom: 1.6rem;
  max-height: 500px;
  overflow: scroll;
  background-color: ${({ theme }) => theme.colors.gray};
`

export const ListItem = styled.li`
  width: 100%;
  margin-bottom: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const ItemNumber = styled.div`
  width: 30px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  font-size: 0.9rem;
`

export const ItemInfo = styled.div`
  height: 40px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 15px;
`

export const ImageItem = styled.div`
  width: 80px;
  margin-left: 5px;
`

export const ItemName = styled.a`
  font-weight: bold;
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.white};

  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const ItemDate = styled.span`
  font-size: 0.8rem;
`

export const LeftItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const RightItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding-right: 15px;
`

export const PlayItem = styled.span`
  cursor: pointer;
  svg {
    color: ${({ theme }) => theme.colors.white};
  }
  :hover {
    svg {
      color: ${({ theme }) => theme.colors.primary};
    }
  }
`
