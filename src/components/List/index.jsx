import { Info } from '../Info'
import { Card } from '../Card'
import { Loading } from '../Loading'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { ListContainer, ListItem } from './styles'

export function List ({ items, loading }) {
  return (
    <ListContainer>
      {loading && <Loading />}
      {items.map((item, i) => {
        const type = item.__typename === 'Dorama' ? 'doramas' : 'peliculas'
        const label = type === 'doramas' ? 'Dorama' : 'Pelicula'
        const year =
          type === 'doramas'
            ? item.first_air_date && item.first_air_date.split('-')[0]
            : item.release_date && item.release_date.split('-')[0]
        return (
          <ListItem key={i}>
            <Card
              href={`/${type}/[slug]`}
              as={`/${type}/${item.slug}`}
              image={URL_IMAGE_POSTER + item.poster_path}
              title={item.name}
              langs={item.languages}
              label={label}
              year={year}
            >
              <Info {...item} card />
            </Card>
          </ListItem>
        )
      })}
    </ListContainer>
  )
}
