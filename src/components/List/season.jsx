import { Card } from '../Card'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { ListContainer, ListItem } from './styles'

export function ListSeasons ({ list }) {
  return (
    <ListContainer>
      {list.map((season, i) => (
        <ListItem key={i}>
          <Card
            height='100px'
            href={'/temporadas/[slug]'}
            as={`/temporadas/${season.slug}`}
            title={`${season.serie_name}`}
            image={URL_IMAGE_POSTER + season.poster_path}
            label={`Temporada ${season.season_number}`}
            imageCover={true}
          />
        </ListItem>
      ))}
    </ListContainer>
  )
}
