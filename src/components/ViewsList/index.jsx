import { useQuery } from '@apollo/react-hooks'

import { Card } from '../Card'
import { Widget } from '../Widget'
import { URL_IMAGE_POSTER_1X, URL_IMAGE_POSTER_2X } from 'utils/urls'
import { List, Item } from './styles'

import { Loading } from '../Loading'
import { TREND_SERIES } from 'gql/user'

export default function ViewsList () {
  const { data: { trendsDoramas = [] } = {}, loading } = useQuery(
    TREND_SERIES,
    {
      ssr: false
    }
  )

  if (loading) {
    return <Loading />
  }

  if (trendsDoramas.length === 0) {
    return null
  }

  return (
    <Widget title='Tendencias'>
      <List>
        {trendsDoramas.map((item, idx) => (
          <Item key={idx}>
            <Card
              href={'/doramas/[slug]'}
              as={`/doramas/${item.slug}`}
              image={URL_IMAGE_POSTER_1X + item.poster_path}
              srcSet={`${URL_IMAGE_POSTER_1X +
                item.poster_path} 1x, ${URL_IMAGE_POSTER_2X +
                item.poster_path} 2x`}
              title={item.name}
            />
          </Item>
        ))}
      </List>
    </Widget>
  )
}
