import styled from 'styled-components'

export const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;

  &::after {
    content: '';
    display: block;
    overflow: hidden;
    clear: both;
  }
`

export const Item = styled.li`
  padding: 0 0.3rem 0.6rem;
  padding-bottom: 0;
  display: block;
  float: left;
  width: 100%;
  flex: 0 0 33.33333333%;
  max-width: 33.33333333%;

  @media ${({ theme }) => theme.device.mobileL} {
    flex: 0 0 25%;
    max-width: 25%;
  }

  @media ${({ theme }) => theme.device.tablet} {
    flex: 0 0 20%;
    max-width: 20%;
  }

  @media ${({ theme }) => theme.device.laptop} {
    flex: 0 0 33.33333333%;
    max-width: 33.33333333%;
  }
`
