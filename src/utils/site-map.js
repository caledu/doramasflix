import fs from 'fs'
import globby from 'globby'
import * as builder from 'xmlbuilder'

import createClient from '../lib/apolloClient'
import { firstDayMonth } from 'utils/functions'
import { LIST_GENRE_SLUG } from 'gql/genre'
import { LIST_MOVIES_SLUG } from 'gql/movie'
import { LIST_SEASON_SLUG } from 'gql/season'
import { LIST_DORAMAS_SLUG } from 'gql/dorama'
import { LIST_EPISODES_SLUG } from 'gql/episode'
import { LIST_NETWORKS_SLUG } from 'gql/network'

import { formatDate } from '../utils/functions'

const rootUrl = 'https://doramasflix.co'

const buildUrlObject = (
  path,
  updatedAt = new Date(),
  freq = 'daily',
  priority = '1.0'
) => {
  return {
    loc: { '#text': `${rootUrl}${path}` },
    lastmod: { '#text': formatDate(updatedAt) },
    changefreq: { '#text': freq },
    priority: { '#text': priority }
  }
}

export const generateSiteMap = async () => {
  try {
    const client = createClient()
    const pages = await globby([
      'src/pages/**/*.jsx',
      '!src/pages/_*.jsx',
      '!src/pages/**/[slug].jsx',
      '!src/pages/**/[letter].jsx',
      '!src/pages/api',
      'posts/*.md'
    ])

    const { data: { listEpisodes = [] } = {} } = await client.query({
      query: LIST_EPISODES_SLUG
    })
    const { data: { listDoramas = [] } = {} } = await client.query({
      query: LIST_DORAMAS_SLUG
    })
    const { data: { listGenres = [] } = {} } = await client.query({
      query: LIST_GENRE_SLUG
    })
    const { data: { listMovies = [] } = {} } = await client.query({
      query: LIST_MOVIES_SLUG
    })
    const { data: { listNetworks = [] } = {} } = await client.query({
      query: LIST_NETWORKS_SLUG
    })
    const { data: { listSeasons = [] } = {} } = await client.query({
      query: LIST_SEASON_SLUG
    })

    const listCast = []
    listDoramas.map(item => {
      item.cast.map(c => {
        if (listCast.findIndex(l => l.slug === c.slug) === -1) {
          listCast.push({ ...c, updatedAt: item.updatedAt })
        }
      })
    })
    listMovies.map(item => {
      item.cast.map(c => {
        if (listCast.findIndex(l => l.slug === c.slug) === -1) {
          listCast.push({ ...c, updatedAt: item.updatedAt })
        }
      })
    })

    generateSubSiteMap(listGenres, 'generos', 'weekly', '0.6')
    generateSubSiteMap(listNetworks, 'productoras', 'weekly', '0.6')
    generateSubSiteMap(listMovies, 'peliculas', 'weekly', '1.0')
    generateSubSiteMap(listDoramas, 'doramas', 'weekly', '1.0')
    generateSubSiteMap(listSeasons, 'temporadas', 'weekly', '0.8')
    generateSubSiteMapCapitulos(listEpisodes, 'capitulos', 'weekly', '1.0')
    generateSubSiteMap(listCast, 'reparto', 'weekly', '0.6')
    generateSubSiteMapPages(pages)

    return
  } catch (error) {
    return { error: 404 }
  }
}

const generateSubSiteMap = (array, root, freq, priority, extra = '') => {
  try {
    let feedObject = {
      urlset: {
        '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
        url: []
      }
    }

    for (let index = array.length - 1; index >= 0; index--) {
      const item = array[index]
      if (item.slug) {
        feedObject.urlset.url.push(
          buildUrlObject(
            `/${root}/${item.slug}`,
            item.updatedAt || firstDayMonth(),
            freq,
            priority
          )
        )
      }
    }

    const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
    const xml = sitemap.end({ pretty: true })

    fs.writeFileSync(`public/sitemap-${root}${extra}.xml`, xml, function (
      err,
      data
    ) {
      if (err) {
        console.error(err)
      }
    })
  } catch (error) {}

  return
}

const generateSubSiteMapCapitulos = (array, root, freq, priority) => {
  try {
    let feedObject = {
      sitemapindex: {
        '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
        '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
        sitemap: []
      }
    }

    const months = []
    array.forEach(item => {
      const date = formatDate(item.updatedAt)
      const month = date
        .split('-')
        .slice(0, 2)
        .join('-')
      if (months.findIndex(m => m === month) === -1) {
        months.push(month)
      }
    })

    months.reverse().forEach(month => {
      feedObject.sitemapindex.sitemap.push({
        loc: { '#text': `${rootUrl}/sitemap-${root}${month}.xml` }
      })

      const caps = array.filter(item => {
        const date = formatDate(item.updatedAt)
        const monthCap = date
          .split('-')
          .slice(0, 2)
          .join('-')
        return monthCap === month
      })

      generateSubSiteMap(caps, root, freq, priority, month)
    })

    const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
    const xml = sitemap.end({ pretty: true })

    fs.writeFileSync(`public/sitemap-${root}.xml`, xml, function (err, data) {
      if (err) {
        console.error(err)
      }
    })
  } catch (error) {}

  return
}

const generateSubSiteMapPages = pages => {
  let feedObject = {
    urlset: {
      '@xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
      '@xmlns:image': 'http://www.google.com/schemas/sitemap-image/1.1',
      url: []
    }
  }

  for (const item of pages) {
    const path = item
      .replace('src/pages', '')
      .replace('.jsx', '')
      .replace('.md', '')
    const route = path.replace('/index', '')
    feedObject.urlset.url.push(
      buildUrlObject(
        `${route}`,
        firstDayMonth(),
        route ? 'weekly' : 'daily',
        route ? '0.6' : '1.0'
      )
    )
  }

  const sitemap = builder.create(feedObject, { encoding: 'utf-8' })
  const xml = sitemap.end({ pretty: true })

  fs.writeFileSync(`public/sitemap-pages.xml`, xml, function (err, data) {
    if (err) {
      console.error(err)
    }
  })

  return
}
