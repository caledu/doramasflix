export const countries = [
  {
    name: 'Corea del Sur',
    flag: '🇰🇷',
    slug: 'corea',
    type: 'Coreanos',
    letter: 'K'
  },
  { name: 'China', flag: '🇨🇳', slug: 'china', type: 'Chinos', letter: 'C' },
  { name: 'Japon', flag: '🇯🇵', slug: 'japon', type: 'Japoneses', letter: 'J' },
  {
    name: 'Taiwan',
    flag: '🇹🇼',
    slug: 'taiwan',
    type: 'Taiwaneses',
    letter: 'T'
  },
  {
    name: 'Tailandia',
    flag: '🇹🇭',
    slug: 'tailandia',
    type: 'Tailandeses',
    letter: 'T'
  }
]

export const languages = {
  13109: 'corea',
  13111: 'china',
  13110: 'japon',
  38: 'mexico'
}

export const languagesList = [
  { id: '38', name: 'Latino' },
  { id: '13109', name: 'Coreano Sub.' },
  { id: '13111', name: 'Chino Sub.' },
  { id: '13110', name: 'Japones Sub.' }
]

export const qualities = {
  123: 'Full HD',
  124: 'HD'
}

export const servers = {
  '34': 'Fembed',
  '395': 'Uqload',
  '3890': 'Supervideo',
  '2671': 'Mystream',
  '3889': 'Mixdrop',
  '6705': 'Jetload',
  '7286': 'Dood',
  '8309': 'Streamtape',
  '8310': 'Gounlimited',
  '14463': 'UpStream',
  '14707': 'Flixplayer',
  '1113': 'Ok',
  '1114': 'Mega',
  '1111': 'FB',
  '1230': 'Voe',
  '1231': 'Viwol',
  '1235': 'Fembed',
  '1233': 'Uqload',
  '1234': 'mp4Upload',
  '5555': 'Server'
}

export const langsName = {
  38: 'Latino',
  37: 'Castellano',
  192: 'Subtitulado',
  13109: 'Coreano Sub.',
  13110: 'Japonés Sub.',
  13111: 'Mandarín Sub.',
  13771: 'Alemán Sub.',
  13772: 'Ruso Sub.',
  13773: 'Portugués Sub.'
}

export const webs = [
  { name: 'SeriesFlix', url: 'https://seriesflix.to/' },
  { name: 'PeliculasFlix', url: 'https://peliculasflix.co/' },
  { name: 'SeriesGO', url: 'https://seriesgo.co/' },
  { name: 'DoramasMp4', url: 'https://doramasflix.co/doramas' },
  { name: 'EstrenosDoramas', url: 'https://doramasflix.co/estrenos' }
]

export const letters = 'abcdefghijklmnopqrstuvwxyz'.split('')

export const collection = ['Viendo', 'Por ver', 'Vistos', 'Por continuar']
