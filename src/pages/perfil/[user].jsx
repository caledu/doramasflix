import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { withApollo } from 'lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

import { Profile } from 'containers/Profile'
import { useQuery } from '@apollo/react-hooks'
import { GET_USER_COLLECTION } from 'gql/user'
import { Loading } from 'components/Loading'

function ShareProfile ({ user }) {
  const { loading, data: { detailUser = {} } = {} } = useQuery(
    GET_USER_COLLECTION,
    { variables: { username: user } }
  )

  const { username } = detailUser

  return (
    <Page>
      <LayoutPage header={<Space height='7rem' />} widgets={[]}>
        {loading ? (
          <Loading />
        ) : (
          <>
            <Head
              title={`Perfil @${username} ► Doramasflix`}
              extra={<meta name='robots' content='noindex, follow' />}
            />
            <Profile user={detailUser} />
          </>
        )}
      </LayoutPage>
    </Page>
  )
}

ShareProfile.getInitialProps = ({ req, query }) => {
  return {
    ...query
  }
}

export default withApollo({ ssr: true })(ShareProfile)
