import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'
import { DoramasSubList } from 'containers/DoramasSubList'
import { URL } from 'utils/urls'

function DoramaSub ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters type='doramas' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Doramas en Sub Español Gratis HD 🥇 Doramasflix`}
          description={`Ver doramas en el idioma Sub Español Latino Online ✅ en calidad HD. Ver estrenos de Dorma Subtitulado. Doramasflix el mejor sitio de Doramas.`}
          url={`${URL}/doramas-subtitulados`}
        />
        <DoramasSubList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

DoramaSub.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(DoramaSub)
