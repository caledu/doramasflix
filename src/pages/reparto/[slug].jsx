import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { CastDetail } from 'containers/CastDetail'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'

function Cast (props) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <CastDetail {...props} />
      </LayoutPage>
    </Page>
  )
}

Cast.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Cast)
