import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'
import { PeliculasSubList } from 'containers/PeliculasSubList'

function DoramaSub ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters type='doramas' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Peliculas en Sub Español Gratis HD 🥇 Doramasflix`}
          description={`Ver peliculas en el idioma Sub Español Latino Online ✅ en calidad HD. Ver estrenos de peliculas en el idioma original. Doramasflix el mejor sitio de Doramas.`}
        />
        <PeliculasSubList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

DoramaSub.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(DoramaSub)
