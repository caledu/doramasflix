import { Page } from 'layouts/Page'
import { SeasonDetail } from 'containers/SeasonDetail'
import { withApollo } from '../../lib/apollo'
import { getDeviceType } from 'utils/functions'

function Season (props) {
  return (
    <Page>
      <SeasonDetail {...props} />
    </Page>
  )
}

Season.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Season)
