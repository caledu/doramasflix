import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { withApollo } from 'lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

import { Collection } from 'containers/Collection'
import { useQuery } from '@apollo/react-hooks'
import { GET_USER_COLLECTION } from 'gql/user'
import { Loading } from 'components/Loading'
import { getDeviceType } from 'utils/functions'

function ShareCollection ({ user, status, deviceType }) {
  const { loading, data: { detailUser = {} } = {} } = useQuery(
    GET_USER_COLLECTION,
    { variables: { username: user } }
  )

  const { names } = detailUser

  return (
    <Page>
      <LayoutPage
        header={<Space height='7rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        {loading ? (
          <Loading />
        ) : (
          <>
            <Head
              title={`Colección ${names} - ${status} ► Doramasflix`}
              extra={<meta name='robots' content='noindex, follow' />}
            />
            <Collection
              user={detailUser}
              status={status}
              deviceType={deviceType}
            />
          </>
        )}
      </LayoutPage>
    </Page>
  )
}

ShareCollection.getInitialProps = ({ req, query }) => {
  const deviceType = getDeviceType(req)
  return {
    deviceType,
    ...query
  }
}

export default withApollo({ ssr: true })(ShareCollection)
