import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { withApollo } from 'lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

import { Account } from 'containers/Account'

function AccountPage () {
  return (
    <Page>
      <LayoutPage header={<Space height='7rem' />} widgets={['views', 'share']}>
        <Head
          title={`Editar Cuenta ► Doramasflix`}
          extra={<meta name='robots' content='noindex, follow' />}
        />
        <Account />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: true })(AccountPage)
