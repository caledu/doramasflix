import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { Terms } from 'components/Terms'
import { Head } from 'components/Head'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

function TermPage () {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`Términos de Cookies ► Doramasflix`} />
        <Terms />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: false })(TermPage)
