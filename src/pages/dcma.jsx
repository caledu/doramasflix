import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { DCMA } from 'components/Terms/DCMA'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { Head } from 'components/Head'

function DCMAPage () {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`DCMA ► Doramasflix`} />
        <DCMA />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: false })(DCMAPage)
