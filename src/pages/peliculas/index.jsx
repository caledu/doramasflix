import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Letters } from 'components/Letters'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { MoviesList } from 'containers/MoviesList'
import { getDeviceType } from 'utils/functions'
import { MoviesCarousel } from 'containers/MoviesCarousel'
import { URL } from 'utils/urls'

function Movies ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<MoviesCarousel deviceType={deviceType} />}
        extraHeader={<Letters type='peliculas' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Peliculas Online Gratis Completas HD 🥇 Doramasflix`}
          description={`En ✅ Doramasflix  ✅ puedes ver peliculas coreanas, chinas, tailandesas y japonesas online gratis HD en Sub Español, Latino en HD Gratis.`}
          url={`${URL}/peliculas`}
        />
        <MoviesList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

Movies.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(Movies)
