import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { PeliculasMXList } from 'containers/PeliculasMXList'
import { getDeviceType } from 'utils/functions'

function PeliculasMX ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Peliculas en Español Latino Gratis HD 🥇 Doramasflix`}
          description={`Ver peliculas en el idioma Español Latino Online ✅ en calidad HD. Ver estrenos de Pelicula en Español Latino. Doramasflix el mejor sitio de Doramas.`}
        />
        <PeliculasMXList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

PeliculasMX.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(PeliculasMX)
