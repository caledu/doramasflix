import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { PremiereList } from 'containers/PremiereList'
import { getDeviceType } from 'utils/functions'
import { URL } from 'utils/urls'

function Premiere ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters type='doramas' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Estrenos Doramas Online Sub Español Gratis HD Completas 🥇 Doramasflix`}
          description={`Ver Los mejores ✅ Estrenos Doramas ✅ Online gratis en español subtitulado, latino, doramas coreanos, chinos, tailandia y japones completas en HD. En Doramasflix, nunca fue tan fácil ver estrenosdoramas.`}
          url={`${URL}/estrenos`}
        />
        <PremiereList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

Premiere.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(Premiere)
