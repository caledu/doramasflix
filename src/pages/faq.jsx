import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { FAQs } from 'components/Terms/FAQs'
import { Space } from 'components/Space'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

function FAQPage () {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`Preguntas frecuentes ► Doramasflix`} />
        <FAQs />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: false })(FAQPage)
