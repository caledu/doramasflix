import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Notes } from 'components/Notes'
import { Letters } from 'components/Letters'
import { withApollo } from 'lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { HomeMovies } from 'containers/HomeMovies'
import { HomeDoramas } from 'containers/HomeDoramas'
import { HomePopulars } from 'containers/HomePopulars'
import { HomeCarousel } from 'containers/HomeCarrousel'
import { LastSeen } from 'containers/LastSeen'
import { getDeviceType } from 'utils/functions'
import { PremiereEpisodes } from 'containers/PremiereEpisodes'

function Home ({ deviceType }) {
  return (
    <Page>
      <Head />
      <LayoutPage
        header={<HomeCarousel deviceType={deviceType} />}
        extraHeader={
          <>
            <Letters />
            <HomePopulars deviceType={deviceType} />
            <LastSeen />
          </>
        }
      >
        <Notes />
        <PremiereEpisodes deviceType={deviceType} />
        <HomeDoramas deviceType={deviceType} />
        <HomeMovies deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

Home.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: true })(Home)
