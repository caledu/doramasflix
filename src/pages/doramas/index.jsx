import { Head } from 'components/Head'
import { Page } from 'layouts/Page'
import { Letters } from 'components/Letters'
import { LayoutPage } from 'layouts/LayoutPage'
import { withApollo } from '../../lib/apollo'
import { DoramasList } from 'containers/DoramasList'
import { getDeviceType } from 'utils/functions'
import { DoramasCarousel } from 'containers/DoramasCarrousel'
import { URL } from 'utils/urls'

function Doramas ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<DoramasCarousel deviceType={deviceType} />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`DoramasMP4 ⋆ Ver Doramas Online - Mira Doramas Sub Español Online 🥇 Doramasflix`}
          description={`Ver los mejores ✅ DoramasMP4 ✅ Online Gratis en Sub Español, Latino en HD Gratis. Ver Doramas MP4 nunca fue tan fácil en Doramasflix.`}
          url={`${URL}/doramas`}
        />
        <DoramasList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

Doramas.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(Doramas)
