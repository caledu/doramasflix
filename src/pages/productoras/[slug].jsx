import { Letters } from 'components/Letters'

import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { NetworkList } from 'containers/NetworkList'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { getDeviceType } from 'utils/functions'

function Genre (props) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters />}
        widgets={['views', 'friends', 'follow']}
      >
        <NetworkList {...props} />
      </LayoutPage>
    </Page>
  )
}

Genre.getInitialProps = ({ req, query }) => {
  const device = getDeviceType(req)
  return {
    ...device,
    ...query
  }
}

export default withApollo({ ssr: true })(Genre)
