import { Page } from 'layouts/Page'
import { Space } from 'components/Space'
import { Networks } from 'containers/Networks'
import { withApollo } from '../../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { Head } from 'components/Head'

function AllNetworks () {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head title={`Productoras de Doramas ► Doramasflix`} />
        <Networks />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: true })(AllNetworks)
