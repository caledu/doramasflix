import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Letters } from 'components/Letters'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'
import { DoramasMXList } from 'containers/DoramaMXList'
import { getDeviceType } from 'utils/functions'
import { URL } from 'utils/urls'

function DoramaMX ({ deviceType }) {
  return (
    <Page>
      <LayoutPage
        header={<Space height='8rem' />}
        extraHeader={<Letters type='doramas' />}
        widgets={['views', 'friends', 'follow']}
      >
        <Head
          title={`Ver Doramas en Español Latino Gratis HD 🥇 Doramasflix`}
          description={`Ver doramas en el idioma Español Latino Online ✅ en calidad HD. Ver estrenos de Dorma en Español Latino. Doramasflix el mejor sitio de Doramas.`}
          url={`${URL}/doramas-latinos`}
        />
        <DoramasMXList deviceType={deviceType} />
      </LayoutPage>
    </Page>
  )
}

DoramaMX.getInitialProps = ({ req }) => getDeviceType(req)

export default withApollo({ ssr: false })(DoramaMX)
