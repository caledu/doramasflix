import { ThemeProvider } from 'styled-components'
import Router from 'next/router'

import theme from 'styles/theme'
import { Body, GlobalStyles } from 'styles/global'
import { Provider as AlertProvider, positions } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

import * as gtag from 'lib/gtag'
import 'react-multi-carousel/lib/styles.css'

const options = {
  timeout: 5000,
  position: positions.BOTTOM_CENTER,
  containerStyle: {
    zIndex: 100,
    maxWidth: '90%'
  }
}

// Notice how we track pageview when route is changed
Router.events.on('routeChangeComplete', url => gtag.pageview(url))

export default function MyApp ({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <AlertProvider template={AlertTemplate} {...options}>
        <GlobalStyles />
        <Body>
          <Component {...pageProps} />
        </Body>
      </AlertProvider>
    </ThemeProvider>
  )
}
