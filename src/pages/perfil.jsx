import { Page } from 'layouts/Page'
import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { withApollo } from '../lib/apollo'
import { LayoutPage } from 'layouts/LayoutPage'

import { Profile } from 'containers/Profile'

function Prefil () {
  return (
    <Page>
      <LayoutPage header={<Space height='7rem' />} widgets={[]}>
        <Head
          title={`Perfil ► Doramasflix`}
          extra={<meta name='robots' content='noindex, follow' />}
        />

        <Profile />
      </LayoutPage>
    </Page>
  )
}

export default withApollo({ ssr: true })(Prefil)
