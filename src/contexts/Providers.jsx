import React from 'react'

import UserContext from './UserContext'
import FavsContext from './FavsContext'
import ListContext from './ListContext'

export function ContextsProviders ({ children }) {
  return (
    <UserContext.Provider>
      <FavsContext.Provider>
        <ListContext.Provider>{children}</ListContext.Provider>
      </FavsContext.Provider>
    </UserContext.Provider>
  )
}
