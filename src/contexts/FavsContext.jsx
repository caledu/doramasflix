import React from 'react'
import { useFavs } from '../hooks/useFavs'

export const FavsContext = React.createContext()

const Provider = ({ children }) => {
  const data = useFavs()

  return <FavsContext.Provider value={data}>{children}</FavsContext.Provider>
}

const Consumer = FavsContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
