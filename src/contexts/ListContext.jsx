import React from 'react'
import { useUserList } from '../hooks/useUserList'

export const ListContext = React.createContext()

const Provider = ({ children }) => {
  const { ...dataUserList } = useUserList()

  return (
    <ListContext.Provider value={dataUserList}>{children}</ListContext.Provider>
  )
}

const Consumer = ListContext.Consumer

export { Provider, Consumer }
export default { Provider, Consumer }
