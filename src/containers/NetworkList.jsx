import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { List } from 'components/List'
import { Head } from 'components/Head'
import { mixArray } from 'utils/functions'
import { Pagination } from 'components/Pagination'
import { LIST_MOVIES } from 'gql/movie'
import { MetaNetwork } from 'components/Meta/Network'
import { LIST_DORAMAS } from 'gql/dorama'
import { TitleSection } from 'components/TitleSection'
import { URL } from 'utils/urls'

export function NetworkList ({ deviceType, slug }) {
  const perPage =
    deviceType === 'mobile' ? 16 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)

  const variables = {
    filter: { networks: [{ slug }] }
  }

  const { data: { listDoramas = [] } = {}, loading: ld1 } = useQuery(
    LIST_DORAMAS,
    {
      variables
    }
  )
  const { data: { listMovies = [] } = {}, loading: ld2 } = useQuery(
    LIST_MOVIES,
    {
      variables
    }
  )

  let name = ''
  const length = listDoramas.length + listMovies.length
  const list = mixArray(listDoramas, listMovies, length)
  const item = list[0]
  if (item) {
    const network = item.networks.find(g => g.slug === slug)
    name = network?.name
  }

  const pageList = list.filter(
    (i, idx) => idx >= (page - 1) * perPage && idx < page * perPage
  )

  return (
    <>
      <Head
        title={`Ver Doramas de ${name} Online Sub Español Gratis HD ► Doramasflix`}
        description={`Ver Doramas de ${name} Online ✅ Series de ${name} en Sub Español, Latino en HD Gratis ✅ Ver dramas de la productora ${name}.`}
        url={`${URL}/productoras/${slug}`}
      />
      <TitleSection title={`Doramas y peliculas de ${name}`} Icon={MdMovie} />
      <MetaNetwork name={name} />
      <List items={pageList} loading={ld1 || ld2} />
      <Pagination
        deviceType={deviceType}
        count={list.length}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
