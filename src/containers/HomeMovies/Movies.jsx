import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { LIST_MOVIES } from 'gql/movie'
import { HomeList } from 'components/HomeList'

export default function Movies ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 15 : deviceType === 'tablet' ? 20 : 24
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const { data: { listMovies = [] } = {}, loading } = useQuery(LIST_MOVIES, {
    variables: {
      sort,
      limit,
      skip: 0
    },
    ssr: false
  })

  return (
    <HomeList
      list={listMovies}
      loading={loading}
      title='Peliculas'
      sort={sort}
      changeSort={changeSort}
      url='/peliculas'
    />
  )
}
