import React from 'react'
import dynamic from 'next/dynamic'
import { TitleSection } from 'components/TitleSection'
import { MdMovieFilter } from 'react-icons/md'
import { useNearScreen } from 'hooks/useNearScreen'

const SeasonsList = dynamic(() => import('./SeasonsList'))

export function Seasons ({ name, serie_id }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '50px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      <TitleSection
        name={`Temporadas dispoinibles de ${name}`}
        Icon={MdMovieFilter}
      />
      {isNearScreen && <SeasonsList serie_id={serie_id} />}
    </div>
  )
}
