import { useQuery } from '@apollo/react-hooks'

import { Loading } from 'components/Loading'
import { ListEpisodesDorama } from 'components/List/episodes'
import { LIST_EPISODES, SEEN_EPISODES } from 'gql/episode'
import { useContext, useEffect } from 'react'
import { UserContext } from 'contexts/UserContext'

export default function EpisodeList ({ serie_id, season_number = 1 }) {
  const { loggedUser } = useContext(UserContext)

  const { data: { listEpisodes = [] } = {}, loading } = useQuery(
    LIST_EPISODES,
    {
      variables: { serie_id, season_number },
      ssr: false
    }
  )

  const { data: dataSeen, refetch } = useQuery(SEEN_EPISODES, {
    variables: { serie_id, season_number, user_id: loggedUser?._id },
    fetchPolicy: 'cache-and-network'
  })

  useEffect(() => {
    if (loggedUser) {
      refetch && refetch()
    }
  }, [loggedUser])

  const { listSeen = [] } = dataSeen || {}

  if (loading) {
    return <Loading />
  }

  return <ListEpisodesDorama list={listEpisodes} listSeen={listSeen} />
}
