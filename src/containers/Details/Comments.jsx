import React from 'react'
import dynamic from 'next/dynamic'
import { useNearScreen } from 'hooks/useNearScreen'

const Disqus = dynamic(() => import('components/Disqus'))

export function Comments (props) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '80px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      {isNearScreen && <Disqus {...props} />}
    </div>
  )
}
