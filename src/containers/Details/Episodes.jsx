import dynamic from 'next/dynamic'
import { TitleSection } from 'components/TitleSection'
import { MdMovieFilter } from 'react-icons/md'
import { useNearScreen } from 'hooks/useNearScreen'

const EpisodesList = dynamic(() => import('./EpisodesList'))

export function Episodes ({ name, serie_id, season_number }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '50px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      <TitleSection name={`${name} - Temporada 1`} Icon={MdMovieFilter} />
      {isNearScreen && (
        <EpisodesList serie_id={serie_id} season_number={season_number} />
      )}
    </div>
  )
}
