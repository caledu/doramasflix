import { useQuery } from '@apollo/react-hooks'

import { Carousel } from 'components/Carousel'
import { LIST_DORAMAS } from 'gql/dorama'
import { CarrouselLoader } from 'components/Loader/Carousel'

export function DoramasCarousel ({ deviceType }) {
  const { data: { listDoramas = [] } = {}, loading } = useQuery(LIST_DORAMAS, {
    variables: { limit: 8, sort: 'POPULARITY_DESC', skip: 0 }
  })

  if (loading) {
    return <CarrouselLoader />
  }

  return <Carousel list={listDoramas} deviceType={deviceType} />
}
