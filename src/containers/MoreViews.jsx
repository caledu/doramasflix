import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const Views = dynamic(() => import('components/ViewsList'))

export function MoreViews ({ deviceType }) {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '100px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      {isNearScreen && <Views deviceType={deviceType} />}
    </div>
  )
}
