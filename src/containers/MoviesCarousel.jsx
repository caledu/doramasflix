import { useQuery } from '@apollo/react-hooks'

import { Space } from 'components/Space'
import { Loading } from 'components/Loading'
import { Carousel } from 'components/Carousel'
import { LIST_MOVIES } from 'gql/movie'

export function MoviesCarousel ({ deviceType }) {
  const { data: { listMovies = [] } = {}, loading } = useQuery(LIST_MOVIES, {
    variables: { limit: 8, sort: 'POPULARITY_DESC', skip: 0 }
  })

  if (listMovies.length === 0) {
    return (
      <>
        <Space height='8rem' />
        {loading && <Loading />}
      </>
    )
  }

  return <Carousel list={listMovies} deviceType={deviceType} />
}
