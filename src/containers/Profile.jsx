import React, { useContext } from 'react'
import { ProfileInfo } from 'components/Profile/info'
import { UserContext } from 'contexts/UserContext'
import { FavsContext } from 'contexts/FavsContext'
import { ListContext } from 'contexts/ListContext'
import { NotFound } from 'components/NotFound'

export function Profile ({ user }) {
  const { favs } = useContext(FavsContext)
  const { loggedUser } = useContext(UserContext)
  const { listDoramas } = useContext(ListContext)

  const own = loggedUser && loggedUser._id == user._id

  if (own) {
    return (
      <ProfileInfo
        user={{ ...loggedUser, favs_doramas: favs, list_doramas: listDoramas }}
        own
      />
    )
  } else if (user) {
    return <ProfileInfo user={user} own={own} />
  }
  return (
    <NotFound title={`No se encontro el perfil con el username @${user}`} />
  )
}
