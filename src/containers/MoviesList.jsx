import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { copy } from 'utils/functions'
import { List } from 'components/List'
import { Filters } from 'components/Filters'
import { Pagination } from 'components/Pagination'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_MOVIES } from 'gql/movie'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function MoviesList ({ deviceType }) {
  const perPage =
    deviceType === 'mobile' ? 12 : deviceType === 'tablet' ? 20 : 32
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const [filter, changeFilter] = useState({})
  const [page, changePage] = useState(1)

  const variables = {
    perPage,
    sort,
    filter: copy(filter),
    page
  }

  const { data: { paginationMovie = {} } = {}, loading } = useQuery(
    PAGINATION_MOVIES,
    {
      variables: copy(variables)
    }
  )
  const { items = [], count } = paginationMovie

  return (
    <>
      <TitleSection title='Peliculas Online' Icon={MdMovie}>
        <Filters
          sort={sort}
          changeSort={changeSort}
          filter={filter}
          changeFilter={changeFilter}
          changePage={changePage}
        />
      </TitleSection>
      <MetaContainer>
        Ver <MetaStrong>peliculas online</MetaStrong> online gratis en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>peliculas online</MetaStrong> en Sub Español, Latino en HD
        Gratis y Completas.
      </MetaContainer>
      <List items={items} loading={loading} />
      <Pagination
        deviceType={deviceType}
        count={count}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
