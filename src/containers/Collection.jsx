import { MdMovie } from 'react-icons/md'
import { useContext, useState } from 'react'

import { List } from 'components/List'
import { UserInfo } from 'components/Profile/user'
import { mixArray } from 'utils/functions'
import { collection } from 'utils/constans'
import { Pagination } from 'components/Pagination'
import { MenuCollection } from 'components/MenuCollection'
import { TitleSection } from 'components/TitleSection'
import { LIST_MOVIES_BY_IDS } from 'gql/movie'
import { LIST_DORAMAS_BY_IDS } from 'gql/dorama'

import slug from 'slug'
import { useQuery } from '@apollo/react-hooks'
import { UserContext } from 'contexts/UserContext'

export function Collection ({ deviceType, status, user }) {
  const { loggedUser } = useContext(UserContext)
  const perPage =
    deviceType === 'mobile' ? 16 : deviceType === 'tablet' ? 24 : 32
  const [page, changePage] = useState(1)

  let listDormasIds = []
  let listMoviesIds = []
  let name = ''

  if (status === 'favoritos') {
    name = 'Favoritos'
    listDormasIds = user.favs_doramas
      .filter(it => it.dorama_id)
      .map(it => it.dorama_id)
    listMoviesIds = user.favs_doramas
      .filter(it => it.movie_id)
      .map(it => it.movie_id)
  } else {
    name = collection.find(it => slug(it, { lower: true }) === status)
    listDormasIds = user.list_doramas
      .filter(it => it.status === name && it.dorama_id)
      .map(it => it.dorama_id)
    listMoviesIds = user.list_doramas
      .filter(it => it.status === name && it.movie_id)
      .map(it => it.movie_id)
  }

  const { loading: ld1, data: { listDoramasByIds = [] } = {} } = useQuery(
    LIST_DORAMAS_BY_IDS,
    { variables: { _ids: listDormasIds } }
  )

  const { loading: ld2, data: { listMoviesByIds = [] } = {} } = useQuery(
    LIST_MOVIES_BY_IDS,
    { variables: { _ids: listMoviesIds } }
  )

  const length = listDoramasByIds.length + listMoviesByIds.length
  let list = mixArray(listDoramasByIds, listMoviesByIds, length)

  const pageList = list.filter(
    (i, idx) => idx >= (page - 1) * perPage && idx < page * perPage
  )

  return (
    <>
      <UserInfo user={user} own={loggedUser && loggedUser._id === user._id} />
      <MenuCollection username={user.username} status={status} />
      <TitleSection
        title={`Doramas y peliculas ${name} de @${user.username}`}
        Icon={MdMovie}
      />
      <List items={pageList} loading={ld1 || ld2} />
      <Pagination
        deviceType={deviceType}
        count={list.length}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
