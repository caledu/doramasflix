import { useQuery } from '@apollo/react-hooks'
import { MdMovieFilter } from 'react-icons/md'

import { TitleSection } from 'components/TitleSection'
import { LastEpisodes } from 'components/LastEpisodes'
import { LAST_EPISODES } from 'gql/episode'

export default function Episodes ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 6 : deviceType === 'tablet' ? 12 : 20
  const { data: { premiereEpisodes = [] } = {}, loading } = useQuery(
    LAST_EPISODES,
    {
      variables: { limit },
      ssr: false
    }
  )

  return (
    <>
      <TitleSection name='Últimos episodios' Icon={MdMovieFilter} />
      <LastEpisodes episodes={premiereEpisodes} loading={loading} />
    </>
  )
}
