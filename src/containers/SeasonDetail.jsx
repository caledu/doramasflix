import { useEffect } from 'react'
import { MdMovieFilter } from 'react-icons/md'
import { useQuery, useLazyQuery } from '@apollo/react-hooks'

import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { Loading } from 'components/Loading'
import { NotFound } from 'components/NotFound'
import { URL_IMAGE_POSTER } from 'utils/urls'
import { LayoutPage } from 'layouts/LayoutPage'
import { ListSeasons } from 'components/List/season'
import { MetaSeason } from 'components/Meta/Season'
import { TitleSection } from 'components/TitleSection'
import { ListEpisodes } from 'components/List/epidisodes'
import { SEASON_DETAIL, SEASON_DETAIL_EXTRA } from 'gql/season'

export function SeasonDetail ({ slug }) {
  const { data: { detailSeason } = {}, loading } = useQuery(SEASON_DETAIL, {
    variables: { slug }
  })

  const [detailSeasonExtra, { data }] = useLazyQuery(SEASON_DETAIL_EXTRA)

  useEffect(() => {
    if (detailSeason) {
      detailSeasonExtra({
        variables: {
          slug: detailSeason.serie_slug,
          season_number: detailSeason.season_number
        }
      })
    }
  }, [detailSeason])

  if (loading) {
    return (
      <>
        <Space height='6rem' />
        <Loading />
      </>
    )
  }

  if (!detailSeason) {
    return (
      <>
        <Space height='8rem' />
        <NotFound title='No se encontro el dorama deseado.' />
      </>
    )
  }

  const { listSeasons = [], listEpisodes = [] } = data || {}

  const { serie_name, serie_poster, season_number } = detailSeason

  return (
    <LayoutPage
      header={<Space height='8rem' />}
      widgets={['views', 'friends', 'follow']}
    >
      <Head
        title={`Ver ${serie_name} Temporada ${season_number} Online Sub Español HD ► Doramasflix`}
        description={`Ver ${serie_name} Teporada ${season_number} Online ✅ Dorama ${serie_name} temporada ${season_number} Sub Español, Latino en HD Gratis  ✅ ${serie_name} Gratis.`}
        image={URL_IMAGE_POSTER + serie_poster}
      />
      <MetaSeason name={serie_name} season={season_number} />
      {listEpisodes.length > 0 && (
        <>
          <TitleSection
            name={`Temporada ${season_number}`}
            Icon={MdMovieFilter}
          />
          <ListEpisodes list={listEpisodes} />
        </>
      )}
      {listSeasons.length > 0 && (
        <>
          <TitleSection
            name={`Temporadas disponibles de ${serie_name}`}
            Icon={MdMovieFilter}
          />
          <ListSeasons list={listSeasons} />
        </>
      )}
    </LayoutPage>
  )
}
