import { MdMovie } from 'react-icons/md'
import { useQuery } from '@apollo/react-hooks'

import { Head } from 'components/Head'
import { Space } from 'components/Space'
import { NotFound } from 'components/NotFound'
import { ListCast } from 'components/List/cast'
import { CastInfo } from 'components/Cast/Info'
import { filterCast } from 'utils/functions'
import { CAST_DETAIL } from 'gql/person'
import { TitleSection } from 'components/TitleSection'
import { MetaCast } from 'components/Meta/Cast'
import { Loading } from 'components/Loading'
import { URL } from 'utils/urls'

export function CastDetail ({ slug }) {
  const { data: { detailPeople } = {}, loading } = useQuery(CAST_DETAIL, {
    variables: { slug }
  })

  if (loading) {
    return (
      <>
        <Space height='6rem' />
        <Loading />
      </>
    )
  }

  if (!detailPeople) {
    return (
      <>
        <Space height='8rem' />
        <NotFound title='No se encontro la busqueda deseada.' />
      </>
    )
  }

  detailPeople.doramas = filterCast(detailPeople.doramas)
  detailPeople.movies = filterCast(detailPeople.movies)

  const { name, gender, doramas, movies } = detailPeople

  return (
    <>
      <Head
        title={`Ver Doramas de ${name} Online Sub Español Gratis HD ► Doramasflix`}
        description={`Ver doramas de ${name} online ✅ Gratis en español subtitulado, Sub Español en HD. Ver los mejores doramas ${
          gender === 1 ? ' de la actriz' : 'del actor'
        } ${name} online.`}
        url={`${URL}/reparto/${slug}`}
      />
      <CastInfo {...detailPeople} />
      {doramas.length > 0 && (
        <>
          <TitleSection name={`Doramas de ${name}`} Icon={MdMovie} />
          <MetaCast name={name} gender={gender} type='doramas' />
          <ListCast items={doramas} loading={loading} />
        </>
      )}
      {movies.length > 0 && (
        <>
          <TitleSection name={`Peliculas de ${name}`} Icon={MdMovie} />
          <MetaCast name={name} gender={gender} type='peliculas' />
          <ListCast items={movies} loading={loading} />
        </>
      )}
    </>
  )
}
