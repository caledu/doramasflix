import { useEffect } from 'react'
import { Head } from 'components/Head'

import { useQuery, useLazyQuery, useMutation } from '@apollo/react-hooks'

import { Space } from 'components/Space'
import { Player } from 'components/Player'
import { Loading } from 'components/Loading'
import { ShareEpisode } from 'components/ShareEpisode'

import { EPISODE_DETAIL, EPISODE_DETAIL_EXTRA } from 'gql/episode'
import { HeaderEpisode } from './Header'
import { Comments } from 'containers/Details/Comments'
import { URL_IMAGE_POSTER, URL } from 'utils/urls'
import { ADD_SEEN_MUTATION } from 'gql/user'
import { NotFound } from 'components/NotFound'

export function EpisodeDetail ({ slug }) {
  const { data: { detailEpisode } = {}, loading } = useQuery(EPISODE_DETAIL, {
    variables: { slug }
  })

  const [detailEpisodeExtra, { data }] = useLazyQuery(EPISODE_DETAIL_EXTRA)

  const [seenEpisode] = useMutation(ADD_SEEN_MUTATION)

  useEffect(() => {
    if (detailEpisode) {
      detailEpisodeExtra({
        variables: {
          episode_id: detailEpisode._id
        }
      })
      seenEpisode({
        variables: { episode_id: detailEpisode._id }
      })
    }
  }, [detailEpisode])

  if (loading) {
    return (
      <>
        <Space height='6rem' />
        <Loading />
      </>
    )
  }

  if (!detailEpisode) {
    return (
      <>
        <Space height='8rem' />
        <NotFound title='No se encontro el episodio deseado deseado.' />
      </>
    )
  }

  const { nextEpisode, prevEpisode } = data || {}

  const {
    _id,
    languages,
    serie_name,
    serie_slug,
    links_online,
    serie_poster,
    still_path,
    season_poster,
    season_number,
    episode_number,
    serie_backdrop_path,
    air_date
  } = detailEpisode

  return (
    <>
      <Head
        title={`Ver ${serie_name} ${
          season_number > 1 ? `${season_number}` : ''
        }Capitulo ${episode_number} Online Sub Español HD ► Doramasflix`}
        description={`Ver ${serie_name}${
          season_number > 1 ? ` ${season_number}` : ''
        } Capitulo ${episode_number} Online ✅ Dorama ${serie_name} ${season_number}X${episode_number}  en Sub Español, Latino en HD Gratis  ✅ ${serie_name} Cap ${episode_number} Gratis.`}
        image={URL_IMAGE_POSTER + (season_poster || serie_poster || still_path)}
        url={`${URL}/capitulos/${slug}`}
      />
      <Player
        langs={languages}
        links={links_online}
        backdrop_path={serie_backdrop_path}
        title={`${serie_name} Capitulo ${episode_number}`}
        season_number={season_number}
        slug={serie_slug}
        next={nextEpisode}
        prev={prevEpisode}
        episode={detailEpisode}
        air_date={air_date}
      />
      <ShareEpisode
        url={`${URL}/capitulos/${slug}`}
        name={`${serie_name} ${
          season_number > 1 ? `${season_number}` : ''
        } Capitulo ${episode_number}`}
      />
      <HeaderEpisode detailEpisode={detailEpisode}>
        <Comments
          name={`${serie_name} ${
            season_number > 1 ? `${season_number}` : ''
          } Capitulo ${episode_number}`}
          _id={_id}
        />
        <Space height='3rem' />
      </HeaderEpisode>
    </>
  )
}
