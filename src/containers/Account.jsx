import React, { useContext } from 'react'
import { UserContext } from 'contexts/UserContext'
import { EditUserForm } from 'components/ModalAuth/EditUserForm'
import { Loading } from 'components/Loading'
import { useAlert } from 'react-alert'
import { useRouter } from 'next/router'

export function Account () {
  const { loggedUser, ldUser, updateUser, ldUpdate, errUpdate } = useContext(
    UserContext
  )

  const alert = useAlert()
  const router = useRouter()

  const handleSubmit = data => {
    updateUser({
      variables: {
        record: {
          _id: loggedUser._id,
          ...data
        }
      }
    }).then(
      ({
        data: {
          updateUser: {
            record: { username }
          }
        }
      }) => {
        alert.success('Se edito correctamente')
        router.push('/perfil/[user]', `/perfil/${username}`)
      }
    )
  }

  if (ldUser) {
    return <Loading />
  }

  return (
    <EditUserForm
      user={loggedUser}
      loading={ldUpdate}
      error={errUpdate}
      onSubmit={handleSubmit}
    />
  )
}
