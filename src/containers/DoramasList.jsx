import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { copy } from 'utils/functions'
import { List } from 'components/List'
import { Filters } from 'components/Filters'
import { Pagination } from 'components/Pagination'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_DORAMAS } from 'gql/dorama'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function DoramasList ({ deviceType }) {
  const perPage =
    deviceType === 'mobile' ? 12 : deviceType === 'tablet' ? 20 : 32
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const [filter, changeFilter] = useState({})
  const [page, changePage] = useState(1)

  const variables = {
    page,
    sort,
    perPage,
    filter: copy(filter)
  }

  const { data: { paginationDorama = {} } = {}, loading } = useQuery(
    PAGINATION_DORAMAS,
    {
      variables: copy(variables),
      ssr: false
    }
  )

  const { items = [], count } = paginationDorama

  return (
    <>
      <TitleSection title='Doramas Online' Icon={MdMovie}>
        <Filters
          sort={sort}
          changeSort={changeSort}
          filter={filter}
          changeFilter={changeFilter}
          changePage={changePage}
        />
      </TitleSection>
      <MetaContainer>
        Ver <MetaStrong>doramas MP4</MetaStrong> online gratis en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>doramasmp4</MetaStrong> en Sub Español, Latino en HD Gratis
        y Completas.
      </MetaContainer>
      <List items={items} loading={loading} />
      <Pagination
        deviceType={deviceType}
        count={count}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
