import { useQuery } from '@apollo/react-hooks'

import { Cast } from 'components/Cast'
import { Space } from 'components/Space'
import { Seasons } from 'containers/Details/Seasons'
import { NotFound } from 'components/NotFound'
import { Episodes } from 'containers/Details/Episodes'
import { Comments } from 'containers/Details/Comments'
import { HeaderDorama } from './Header'
import { DETAIL_DORAMA } from 'gql/dorama'
import { CarrouselLoader } from 'components/Loader/Carousel'

import { Head } from 'components/Head'
import { URL_IMAGE_POSTER, URL } from 'utils/urls'

export function DoramaDetail ({ slug }) {
  const { data: { detailDorama } = {}, loading } = useQuery(DETAIL_DORAMA, {
    variables: { slug }
  })

  if (loading) {
    return <CarrouselLoader />
  }

  if (!detailDorama) {
    return (
      <>
        <Space height='8rem' />
        <NotFound title='No se encontro el dorama deseado.' />
      </>
    )
  }

  const { _id, name, cast, first_air_date, poster_path, name_es } = detailDorama
  return (
    <>
      <Head
        title={`Ver Dorama ${name} (${first_air_date &&
          first_air_date.split('-')[0]}) Online Sub Español HD ► Doramasflix`}
        description={`Ver ${name} Online ✅ Dorama ${name} en Sub español, Latino en HD Gratis ✅ Capitulos Completos del dorama ${name_es ||
          name}`}
        image={URL_IMAGE_POSTER + poster_path}
        url={`${URL}/doramas/${slug}`}
      />
      <HeaderDorama detailDorama={detailDorama}>
        <Episodes serie_id={_id} name={name} />
        <Seasons serie_id={_id} name={name} />
        <Cast cast={cast} name={name} />
        <Comments _id={_id} name={name} />
        <Space height='3rem' />
      </HeaderDorama>
    </>
  )
}
