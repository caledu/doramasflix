import { Meta } from 'components/Meta'
import { Item } from 'components/Carousel/Item'

import { LayoutPage } from 'layouts/LayoutPage'

export function HeaderDorama ({ detailDorama, children }) {
  const { name } = detailDorama

  return (
    <LayoutPage
      header={<Item {...detailDorama} full title />}
      widgets={['views', 'friends', 'follow']}
    >
      <Meta name={name} type='dorama' />
      {children}
    </LayoutPage>
  )
}
