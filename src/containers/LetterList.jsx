import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { FaSpellCheck } from 'react-icons/fa'

import { List } from 'components/List'
import { mixArray } from 'utils/functions'
import { NotFound } from 'components/NotFound'
import { Pagination } from 'components/Pagination'
import { LIST_MOVIES } from 'gql/movie'
import { LIST_DORAMAS } from 'gql/dorama'
import { TitleSection } from 'components/TitleSection'

export function LetterList ({ deviceType, letter = '' }) {
  const perPage =
    deviceType === 'mobile' ? 16 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)

  const variables = {
    filter: { letter }
  }

  const { data: { listDoramas = [] } = {}, loading: ldgDoramas } = useQuery(
    LIST_DORAMAS,
    { variables }
  )
  const { data: { listMovies = [] } = {}, loading: ldgMovies } = useQuery(
    LIST_MOVIES,
    { variables }
  )

  const length = listDoramas.length + listMovies.length
  const list = mixArray(listDoramas, listMovies, length)
  const name = `Doramas y peliculas con inicial "${letter.toUpperCase()}"`

  const pageList = list.filter(
    (item, idx) => idx >= (page - 1) * perPage && idx < page * perPage
  )

  return (
    <>
      <TitleSection title={name} Icon={FaSpellCheck} />
      <List items={pageList} loading={ldgDoramas || ldgMovies} />
      {list.length === 0 && !(ldgDoramas || ldgMovies) ? (
        <NotFound
          title={'¡Uy! no se encontraron resultados para esta busqueda.'}
        />
      ) : (
        <Pagination
          deviceType={deviceType}
          count={list.length}
          perPage={perPage}
          onChange={changePage}
          page={page}
        />
      )}
    </>
  )
}
