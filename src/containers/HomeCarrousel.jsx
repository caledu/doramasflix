import { useQuery } from '@apollo/react-hooks'

import { LIST } from 'gql/both'
import { Space } from 'components/Space'
import { Carousel } from 'components/Carousel'
import { mixArray } from 'utils/functions'
import { CarrouselLoader } from 'components/Loader/Carousel'

export function HomeCarousel ({ deviceType }) {
  const {
    data: { listDoramas = [], listMovies = [] } = {},
    loading
  } = useQuery(LIST, {
    variables: { limit: 4 }
  })

  if (loading) {
    return <CarrouselLoader />
  }

  const list = mixArray(listDoramas, listMovies, 8)

  if (list.length === 0) {
    return <Space height='8rem' />
  }

  return <Carousel list={list} deviceType={deviceType} />
}
