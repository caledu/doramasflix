import { useQuery } from '@apollo/react-hooks'
import { MdStarBorder } from 'react-icons/md'

import { mixArray } from 'utils/functions'
import { PopularList } from 'components/PopularList'
import { CardsLoader } from 'components/Loader/Cards'
import { TitleSection } from 'components/TitleSection'
import { LIST_POPULARS } from 'gql/both'

export function HomePopulars ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 5 : deviceType === 'tablet' ? 8 : 12
  const {
    data: { listDoramas = [], listMovies = [] } = {},
    loading
  } = useQuery(LIST_POPULARS, {
    variables: { limit, skip: 4 },
    ssr: false
  })

  const list = mixArray(listDoramas, listMovies, limit * 2)

  return (
    <>
      <TitleSection name='Populares' Icon={MdStarBorder} />
      {loading ? (
        <CardsLoader cols={limit} />
      ) : (
        <PopularList list={list} deviceType={deviceType} />
      )}
    </>
  )
}
