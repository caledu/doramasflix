import { useQuery } from '@apollo/react-hooks'
import { MdMovieFilter } from 'react-icons/md'

import { TitleSection } from 'components/TitleSection'
import { LastSeenEpisodes } from 'components/LastEpisodes/seen'
import { SEEN_SERIES } from 'gql/user'
import { useEffect } from 'react'

export function LastSeen ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 6 : deviceType === 'tablet' ? 12 : 20
  const { data: { listDoramasSeen = [] } = {}, loading, refetch } = useQuery(
    SEEN_SERIES,
    {
      variables: { limit },
      ssr: false
    }
  )

  useEffect(() => {
    refetch && refetch()
  }, [deviceType])

  if (listDoramasSeen.length === 0) {
    return null
  }

  return (
    <>
      <TitleSection name='Continuar viendo' Icon={MdMovieFilter} />
      <LastSeenEpisodes episodes={listDoramasSeen} loading={loading} />
    </>
  )
}
