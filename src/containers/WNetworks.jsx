import React from 'react'
import dynamic from 'next/dynamic'

import { useNearScreen } from 'hooks/useNearScreen'

const NetworkList = dynamic(() => import('components/WidgetList/NetworkList'))

export function WNetworks () {
  const { isNearScreen, fromRef } = useNearScreen({
    distance: '100px'
  })

  return (
    <div style={{ minHeight: 150 }} ref={fromRef}>
      {isNearScreen && <NetworkList />}
    </div>
  )
}
