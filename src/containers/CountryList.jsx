import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { List } from 'components/List'
import { Head } from 'components/Head'
import { mixArray } from 'utils/functions'
import { countries } from 'utils/constans'
import { Pagination } from 'components/Pagination'
import { LIST_MOVIES } from 'gql/movie'
import { LIST_DORAMAS } from 'gql/dorama'
import { TitleSection } from 'components/TitleSection'
import { MetaCountry } from 'components/Meta/Country'
import { URL } from 'utils/urls'

export function CountryList ({ deviceType, slug = '' }) {
  const perPage =
    deviceType === 'mobile' ? 16 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)

  const variables = {
    filter: { country: slug }
  }

  const { data: { listDoramas = [] } = {}, loading: ld1 } = useQuery(
    LIST_DORAMAS,
    {
      variables
    }
  )
  const { data: { listMovies = [] } = {}, loading: ld2 } = useQuery(
    LIST_MOVIES,
    {
      variables
    }
  )

  const length = listDoramas.length + listMovies.length
  let list = mixArray(listDoramas, listMovies, length)
  const country = countries.find(g => g.slug === slug)
  const name = country && country.name
  const type = country && country.type
  const letter = country && country.letter

  const pageList = list.filter(
    (i, idx) => idx >= (page - 1) * perPage && idx < page * perPage
  )

  return (
    <>
      <Head
        title={`Ver Doramas ${type} Online Sub Español Gratis HD ► Doramasflix`}
        description={`Ver doramas ${type} online ✅ Gratis en español subtitulado, Sub Español en HD. Ver los mejores ${letter}-Dramas Completos producidos en ${name}.`}
        url={`${URL}/paises/${slug}`}
      />
      <TitleSection title={`Doramas y peliculas de ${name}`} Icon={MdMovie} />
      <MetaCountry name={name} type={type} />
      <List items={pageList} loading={ld1 || ld2} />
      <Pagination
        deviceType={deviceType}
        count={list.length}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
