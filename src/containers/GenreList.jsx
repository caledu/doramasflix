import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { List } from 'components/List'
import { Head } from 'components/Head'
import { mixArray } from 'utils/functions'
import { MetaGenre } from 'components/Meta/Genre'
import { Pagination } from 'components/Pagination'
import { LIST_MOVIES } from 'gql/movie'
import { LIST_DORAMAS } from 'gql/dorama'
import { TitleSection } from 'components/TitleSection'
import { URL } from 'utils/urls'

export function GenreList ({ deviceType, slug }) {
  const perPage =
    deviceType === 'mobile' ? 16 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)

  const variables = {
    filter: { genres: [{ slug }] }
  }

  const { data: { listDoramas = [] } = {}, loading: ld1 } = useQuery(
    LIST_DORAMAS,
    {
      variables
    }
  )
  const { data: { listMovies = [] } = {}, loading: ld2 } = useQuery(
    LIST_MOVIES,
    {
      variables
    }
  )

  let name = ''
  const length = listDoramas.length + listMovies.length
  const list = mixArray(listDoramas, listMovies, length)
  const item = list[0]
  if (item) {
    const genre = item.genres.find(g => g.slug === slug)
    name = genre?.name
  }

  const pageList = list.filter(
    (item, idx) => idx >= (page - 1) * perPage && idx < page * perPage
  )

  return (
    <>
      <Head
        title={`Ver Doramas de ${name} Online Sub Español Gratis ► Doramasflix`}
        description={`Ver Doramas de ${name} Online ✅ Series de ${name} en Sub Español, Latino en HD Gratis ✅ Ver dramas del género ${name}.`}
        url={`${URL}/generos/${slug}`}
      />
      <TitleSection title={`Doramas y peliculas de ${name}`} Icon={MdMovie} />
      <MetaGenre name={name} />
      <List items={pageList} loading={ld1 || ld2} />
      <Pagination
        deviceType={deviceType}
        count={list.length}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
