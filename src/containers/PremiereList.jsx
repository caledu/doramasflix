import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { copy } from 'utils/functions'
import { List } from 'components/List'
import { Filters } from 'components/Filters'
import { Pagination } from 'components/Pagination'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_DORAMAS } from 'gql/dorama'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function PremiereList ({ deviceType }) {
  const perPage =
    deviceType === 'mobile' ? 12 : deviceType === 'tablet' ? 20 : 32
  const [page, changePage] = useState(1)
  const [filter, changeFilter] = useState({})
  const [sort, changeSort] = useState('CREATEDAT_DESC')

  const variables = {
    sort,
    page,
    perPage,
    filter: copy({ ...filter, premiere: true })
  }

  const { data: { paginationDorama = {} } = {}, loading } = useQuery(
    PAGINATION_DORAMAS,
    {
      variables: copy(variables)
    }
  )

  const { items = [], count } = paginationDorama

  return (
    <>
      <TitleSection title='Doramas en Emisión' Icon={MdMovie}>
        <Filters
          sort={sort}
          changeSort={changeSort}
          filter={filter}
          changeFilter={changeFilter}
          changePage={changePage}
        />
      </TitleSection>
      <MetaContainer>
        Ver <MetaStrong>estrenos doramas</MetaStrong> online gratis en 720p HD y
        1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de los{' '}
        <MetaStrong>estrenosdoramas</MetaStrong> en Sub Español, Latino en HD
        Gratis y Completas.
      </MetaContainer>
      <List items={items} loading={loading} type='doramas' />
      <Pagination
        deviceType={deviceType}
        count={count}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
