import { MdMovie } from 'react-icons/md'
import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { List } from 'components/List'
import { Pagination } from 'components/Pagination'
import { TitleSection } from 'components/TitleSection'
import { PAGINATION_MOVIES } from 'gql/movie'
import { MetaContainer, MetaStrong } from 'components/Meta/styles'

export function PeliculasSubList ({ deviceType }) {
  const [page, changePage] = useState(1)
  const perPage =
    deviceType === 'mobile' ? 12 : deviceType === 'tablet' ? 20 : 32

  const variables = {
    sort: 'CREATEDAT_DESC',
    page,
    perPage,
    filter: { bylanguages: ['13109', '13110', '13111'] }
  }

  const { data: { paginationMovie = {} } = {}, loading } = useQuery(
    PAGINATION_MOVIES,
    {
      variables
    }
  )

  const { items = [], count } = paginationMovie

  return (
    <>
      <TitleSection
        title='Peliculas subtitulados al español'
        Icon={MdMovie}
      ></TitleSection>
      <MetaContainer>
        Ver <MetaStrong>peliculas sub español</MetaStrong> online gratis en 720p
        HD y 1080p Full HD. Recuerda que en Doramasflix, puedes disfrutar de las{' '}
        <MetaStrong>
          peliculas en el idioma original subtitulados al español
        </MetaStrong>{' '}
        HD Gratis y Completas.
      </MetaContainer>
      <List items={items} loading={loading} type='peliculas' />
      <Pagination
        deviceType={deviceType}
        count={count}
        perPage={perPage}
        onChange={changePage}
        page={page}
      />
    </>
  )
}
