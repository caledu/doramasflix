import { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'

import { HomeList } from 'components/HomeList'
import { LIST_DORAMAS } from 'gql/dorama'

export default function Doramas ({ deviceType }) {
  const limit = deviceType === 'mobile' ? 15 : deviceType === 'tablet' ? 20 : 24
  const [sort, changeSort] = useState('CREATEDAT_DESC')
  const { data: { listDoramas = [] } = {}, loading } = useQuery(LIST_DORAMAS, {
    variables: {
      sort,
      limit,
      skip: 0
    },
    ssr: false
  })

  return (
    <HomeList
      list={listDoramas}
      loading={loading}
      title='Doramas'
      sort={sort}
      changeSort={changeSort}
      url='/doramas'
      type='doramas'
    />
  )
}
