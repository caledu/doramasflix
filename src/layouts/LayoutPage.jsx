import { Follow } from 'components/Follow'
import { Container, Row, Col } from 'styles/base'
import { WebsFriend } from 'components/WidgetList'
import { MoreViews } from 'containers/MoreViews'
import { Genres } from 'containers/Genres'
import { WNetworks } from 'containers/WNetworks'
import { Countries } from 'containers/Countries'

export function LayoutPage ({
  header,
  children,
  extraHeader,
  widgets = ['follow', 'views', 'friends', 'genres', 'networks', 'countries']
}) {
  return (
    <>
      {header}
      <Container>
        {extraHeader}
        <Row>
          <Col direction='right'>{children}</Col>
          {widgets.length > 0 && (
            <Col direction='left'>
              {widgets.map(widget => {
                const item = listWidgets.find(item => item.id === widget)
                if (item) {
                  return item.component(item.id)
                } else {
                  return null
                }
              })}
            </Col>
          )}
        </Row>
      </Container>
    </>
  )
}

const listWidgets = [
  { id: 'follow', component: key => <Follow key={key} /> },
  { id: 'views', component: key => <MoreViews key={key} /> },
  { id: 'friends', component: key => <WebsFriend key={key} /> },
  { id: 'genres', component: key => <Genres key={key} /> },
  { id: 'networks', component: key => <WNetworks key={key} /> },
  { id: 'countries', component: key => <Countries key={key} /> }
]
