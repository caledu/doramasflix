import { Main } from 'styles/global'
import { Header } from 'components/Header'
import { Footer } from '../components/Footer'
import { ContextsProviders } from 'contexts/Providers'

export function Page ({ children }) {
  return (
    <ContextsProviders>
      <Header />
      <Main>{children}</Main>
      <Footer />
    </ContextsProviders>
  )
}
