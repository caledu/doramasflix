import { useEffect } from 'react'
import { usePersistStorage } from 'hooks'
import { useMutation, useLazyQuery } from '@apollo/react-hooks'

import { UPDATE_USER, GET_LOGGED_USER } from 'gql/user'

export function useUser ({ jwt } = {}) {
  const [updateUser, { loading: ldUpdate, error: errUpdate }] = useMutation(
    UPDATE_USER,
    {
      update (
        cache,
        {
          data: {
            updateUser: { record }
          }
        }
      ) {
        changeUser(record)
      }
    }
  )
  const [user, changeUser, restored] = usePersistStorage('user', null)

  const [fetchUser, { loading, error }] = useLazyQuery(GET_LOGGED_USER, {
    onCompleted: ({ loggedUser }) => {
      changeUser(loggedUser)
    }
  })

  useEffect(() => {
    if (restored && jwt) {
      fetchUser()
    }
  }, [restored, jwt])

  const refetch = () => {
    fetchUser()
  }

  return {
    loading,
    error,
    refetch,
    loggedUser: user,
    updateUser,
    changeUser,
    ldUpdate,
    errUpdate,
    ldUser: !restored || loading
  }
}
