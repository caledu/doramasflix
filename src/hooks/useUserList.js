import { useState, useContext, useEffect } from 'react'
import { useMutation, useLazyQuery } from '@apollo/react-hooks'
import { UserContext } from 'contexts/UserContext'

import { UPDATE_USER, GET_USER_LIST } from 'gql/user'

export function useUserList () {
  const { loggedUser } = useContext(UserContext)
  const [updateUser] = useMutation(UPDATE_USER)
  const [listDoramas, changeListDoramas] = useState([])

  const [fetch, { loading }] = useLazyQuery(GET_USER_LIST, {
    onCompleted: ({ detailUserById }) => {
      if (detailUserById) {
        changeListDoramas(detailUserById.list_doramas || [])
      }
    }
  })

  useEffect(() => {
    if (loggedUser) {
      fetch({ variables: { _id: loggedUser._id } })
    }
  }, [loggedUser])

  const updateList = async ({
    status,
    slug,
    dorama_id,
    movie_id,
    name,
    poster_path,
    __typename
  }) => {
    const index = listDoramas.findIndex(l =>
      __typename === 'Dorama'
        ? l.dorama_id === dorama_id
        : l.movie_id === movie_id
    )
    if (index !== -1) {
      listDoramas[index].status = status
    } else {
      listDoramas.push({ status, dorama_id, name, poster_path, movie_id, slug })
    }

    updateUser({
      variables: {
        record: {
          _id: loggedUser._id,
          list_doramas: listDoramas.map(it => ({
            ...it,
            __typename: undefined
          }))
        }
      }
    }).catch(console.error)

    changeListDoramas(listDoramas.slice())
  }

  const deleteToList = async _id => {
    const index = listDoramas.findIndex(
      l => l.dorama_id === _id || l.movie_id === _id
    )
    if (index !== -1) {
      listDoramas.splice(index, 1)
    }

    updateUser({
      variables: {
        record: {
          _id: loggedUser._id,
          list_doramas: listDoramas.map(it => ({
            ...it,
            __typename: undefined
          }))
        }
      }
    })
    changeListDoramas(listDoramas.slice())
  }

  return {
    loading,
    listDoramas,
    updateList,
    deleteToList
  }
}
