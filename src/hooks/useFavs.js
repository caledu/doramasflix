import { useState, useContext, useEffect } from 'react'
import { useMutation, useLazyQuery } from '@apollo/react-hooks'
import { UserContext } from 'contexts/UserContext'

import { UPDATE_USER, GET_USER_FAVS } from 'gql/user'

export function useFavs () {
  const { loggedUser, jwt } = useContext(UserContext)
  const [updateUser] = useMutation(UPDATE_USER)
  const [favs, changeFavs] = useState([])

  const [fetch, { loading }] = useLazyQuery(GET_USER_FAVS, {
    onCompleted: ({ detailUserById }) => {
      if (detailUserById) {
        changeFavs(detailUserById.favs_doramas || [])
      }
    }
  })

  useEffect(() => {
    if (loggedUser && jwt) {
      fetch({ variables: { _id: loggedUser._id } })
    }
  }, [loggedUser, jwt])

  const updatefavs = async ({ _id, name, poster_path, __typename, slug }) => {
    const index = favs.findIndex(l =>
      __typename === 'Dorama' ? l.dorama_id === _id : l.movie_id === _id
    )
    if (index !== -1) {
      favs.splice(index, 1)
    } else {
      favs.push({
        name,
        slug,
        poster_path,
        dorama_id: __typename === 'Dorama' ? _id : null,
        movie_id: __typename === 'Movie' ? _id : null
      })
    }

    updateUser({
      variables: {
        record: {
          _id: loggedUser._id,
          favs_doramas: favs.map(it => ({
            ...it,
            __typename: undefined
          }))
        }
      }
    })
    changeFavs(favs.slice())
  }

  return {
    loading,
    favs,
    updatefavs
  }
}
