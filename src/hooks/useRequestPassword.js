import { useMutation } from '@apollo/react-hooks'

import { REQUEST_PASS } from 'gql/auth'

export const useRequestPassword = ({ onSuccess }) => {
  const [requestPass, { loading, error }] = useMutation(REQUEST_PASS)

  const handleRequest = async data => {
    requestPass({
      variables: {
        ...data
      }
    }).then(({ errors }) => {
      if (errors) {
        alert('Se presento un error al requerir una nueva contraseña.')
      } else {
        alert('Se envió un email con una contraseña temporal.')
        onSuccess && onSuccess()
      }
    })
  }

  return {
    handleRequest,
    ldRequest: loading,
    errRequest: error
  }
}
