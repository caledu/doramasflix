import { useContext } from 'react'
import { useMutation } from '@apollo/react-hooks'

import { REGISTER } from 'gql/auth'
import { UserContext } from 'contexts/UserContext'

export const useRegister = ({ onSuccess } = {}) => {
  const { setAuth } = useContext(UserContext)
  const [createUser, { loading, error }] = useMutation(REGISTER)

  const registerUser = async record => {
    createUser({
      variables: { record }
    }).then(({ data }) => {
      const { signup = {} } = data || {}
      const { token } = signup
      if (token) {
        setAuth(token)
        onSuccess && onSuccess()
      } else {
        alert('Ocurrio un error al registrar al usuario')
      }
    })
  }

  return {
    registerUser,
    ldRegister: loading,
    errRegister: error
  }
}
